import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';
import 'package:score_immo/screens/list_immobilier/list_immobilier_screen.dart';
import 'package:score_immo/screens/list_locataire/list_locataire_screen.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';
import 'package:score_immo/screens/profile/profile_screen.dart';

class NavigationBarBottom extends StatefulWidget {
  int? index;
  NavigationBarBottom({this.index});

  @override
  _NavigationBarBottomState createState() => _NavigationBarBottomState();
}

class _NavigationBarBottomState extends State<NavigationBarBottom> {
  int bottomSelectedIndex = 0;
  PageController pageController =
      PageController(initialPage: 0, keepPage: true);

  List<BottomNavigationBarItem> buildBottomNavBarItems() {
    return [
      BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined), title: Text("Accueil")),
      BottomNavigationBarItem(
        icon: Icon(Icons.location_city_outlined),
        title: Text('Acquisitions'),
      ),
      BottomNavigationBarItem(
          icon: Icon(Icons.home_work_outlined), title: Text('Biens')),
      BottomNavigationBarItem(
          icon: Icon(Icons.local_hotel_outlined), title: Text('Locations')),
      BottomNavigationBarItem(
          icon: Icon(Icons.transfer_within_a_station_outlined),
          title: Text('Locataires')),
      BottomNavigationBarItem(
          icon: Icon(Icons.person_outline_outlined), title: Text('Profil'))
    ];
  }

  Widget buildPageView() {
    setState(() {
      pageController =
          PageController(initialPage: bottomSelectedIndex, keepPage: true);
    });
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        pageChanged(index);
      },
      children: <Widget>[
        HomeScreen(),
        ListImmobilierScreen(),
        ListBienImmobilierScreen(),
        LocationScreen(),
        ListLocataireScreen(),
        ProfileScreen(),
      ],
    );
  }

  @override
  void initState() {
    if (widget.index != null) {
      setState(() {
        bottomSelectedIndex = widget.index!;
        //widget.index = null;
      });
    }
    super.initState();
  }

  void pageChanged(int index) {
    setState(() {
      bottomSelectedIndex = index;
    });
  }

  void bottomTapped(int index) {
    setState(() {
      bottomSelectedIndex = index;
      pageController.animateToPage(index,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildPageView(),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: bottomSelectedIndex,
        onTap: (index) {
          bottomTapped(index);
        },
        items: buildBottomNavBarItems(),
      ),
    );
  }
}
