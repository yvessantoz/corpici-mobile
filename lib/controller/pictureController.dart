import 'dart:convert';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:file_picker/file_picker.dart';

class PictureController extends GetxController with StateMixin {
  @override
  void onInit() {
    retrieveLostData();
    super.onInit();
  }

  @override
  void onReady() {
    initAll();
    retrieveLostData();
    super.onReady();
  }

  bool retrieved = false;
  var selectedImagePath = ''.obs;
  var selectedPDFPath = ''.obs;
  var retrieveDataError = ''.obs;
  var pickImageError = ''.obs;
  RxList<XFile> fileList = <XFile>[].obs;

  final _picker = ImagePicker();
  //

  initAll() {
    change(null, status: RxStatus.empty());
  }

  void takePicture(ImageSource imageSource) async {
    try {
      final pickedFile = await _picker.getImage(source: imageSource);
      if (pickedFile != null) {
        selectedImagePath.value = pickedFile.path;
        change(selectedImagePath, status: RxStatus.success());
        print(selectedImagePath);
        retrieved = false;
      } else {
        change(null, status: RxStatus.empty());
        Get.back();
      }
    } catch (e) {
      change(null, status: RxStatus.error());
      retrieved = false;
      pickImageError.value = "Error de capture: " + e.toString();
    }
  }

  // Ouvre la boite de dialogue pour le choix des images
  void takeMultiplePictures() async {
    try {
      change(fileList, status: RxStatus.loading());
      final List<XFile>? pickedFile = await _picker.pickMultiImage();
      if (pickedFile != null) {
        fileList = pickedFile.obs;
        change(fileList, status: RxStatus.success());
        retrieved = false;
      } else {
        change(null, status: RxStatus.empty());
        Get.back();
      }
    } catch (e) {
      change(null, status: RxStatus.error());
      retrieved = false;
      pickImageError.value = "Error de capture: " + e.toString();
    }
  }

  //
  downloadPDF() async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: ['pdf']);
    if (result != null) {
      selectedPDFPath = result.files.single.path!.obs;
      change(selectedPDFPath, status: RxStatus.success());
    } else {
      change(null, status: RxStatus.empty());
      // User canceled the picker
    }
  }

  /// Retourne les données perdues lors de la capture
  Future<void> retrieveLostData() async {
    final LostDataResponse response = await _picker.retrieveLostData();
    if (response.isEmpty) {
      retrieved = false;
      return;
    } else if (response.file != null) {
      if (response.type == RetrieveType.image) {
        selectedImagePath.value = "${response.file!.path.toString()}";
        retrieved = true;
      }
    } else if (response.files != null) {
      if (response.type == RetrieveType.image) {
        fileList = response.files!.obs;
        retrieved = true;
      }
    } else {
      retrieved = false;
      retrieveDataError.value =
          "Erreur de récupération: " + response.exception!.code.toString();
    }
  }

  String retrieveDataErrorValue() {
    return retrieveDataError.value;
  }

  String pictureSelected() {
    return selectedImagePath.value;
  }

  List<XFile> filesSelected() {
    return fileList;
  }

  String getBase64File(String path) {
    File file = File(path);
    List<int> fileInByte = file.readAsBytesSync();
    String fileInBase64 = base64Encode(fileInByte);
    return fileInBase64;
  }
}
