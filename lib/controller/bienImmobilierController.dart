import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/models/Galery.dart';
import 'package:score_immo/models/network/Http.dart';
import 'package:score_immo/models/network/ResponseBody.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';

class BienImmobilierController extends GetxController
    with StateMixin<List<BienImmobilier>> {
  // @override
  // void onReady() {
  //   initAll();
  //   super.onReady();
  // }

  @override
  void onInit() {
    initAll();
    super.onInit();
  }

  RxList<BienImmobilier> bienImmobiliersList = <BienImmobilier>[].obs;
  RxList<Galerie> galeryList = <Galerie>[].obs;
  Rx<StatistiqueBienImmobilier> statsData = StatistiqueBienImmobilier().obs;

  initAll() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await fetchBienImmobiliers();
      await statistiquesBiensImmobiliers();
    } else {
      change(null, status: RxStatus.error());
      _alertDanger("Désolé, vous n'avez pas accès à internet");
    }
  }

  // Recherche
  search(String value) {
    List<BienImmobilier> newList = <BienImmobilier>[].obs;
    var list = bienImmobiliersList.where((item) =>
        item.name!.contains("$value") ||
        item.bienType!.contains("$value") ||
        item.adresse!.contains("$value"));
    newList.assignAll(list.map((data) => data));

    if (value.isNotEmpty && newList.isEmpty) {
      change(null, status: RxStatus.empty());
    } else if (value.isEmpty) {
      change(bienImmobiliersList, status: RxStatus.success());
    } else {
      change(newList, status: RxStatus.success());
    }
  }

  //
  Future<Map<dynamic, dynamic>> fetchBienImmobiliers() async {
    try {
      change(null, status: RxStatus.loading());

      HttpResponse response = await Request().send("biens", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          BienImmobilierFromData bienImmobilierData =
              BienImmobilierFromData.fromJson(data);

          List<dynamic> bienImmobilierFetch = bienImmobilierData.biens ?? [];

          bienImmobiliersList.assignAll(bienImmobilierFetch
              .map((data) => BienImmobilier.fromJson(data))
              .toList());

          if (bienImmobiliersList.isNotEmpty) {
            change(bienImmobiliersList, status: RxStatus.success());
          } else {
            change(null, status: RxStatus.empty());
          }
          //log(jsonEncode(bienImmobiliersList));
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          change(null, status: RxStatus.error(message));
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        change(null, status: RxStatus.error(body));
        _alertDanger("$body bien");
      }
    } catch (e) {
      log("bien: ${e.toString()}");
      _alertDanger("Désolé une erreur s'est produite.");
      change(null, status: RxStatus.error(e.toString()));
    }
    return {'status': false};
  }

  //
  Future<Map<dynamic, dynamic>> statistiquesBiensImmobiliers() async {
    try {
      HttpResponse response =
          await Request().send("statistiques/biens_immobiliers", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          statsData = StatistiqueBienImmobilier.fromJson(data).obs;
          //log(jsonEncode(data));
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          // _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        //_alertDanger(body);
      }
    } catch (e) {}
    return {'status': false};
  }

  //
  Future<Map<dynamic, dynamic>> ajouterBien(
      BienImmobilier bienImmobilier, PickedFile file, List<XFile> files) async {
    try {
      var bienImmobilierJson = bienImmobilier.toJson();
      bienImmobilierJson.removeWhere((key, value) => value == null);
      HttpResponse response = await Request().sendMultipartData(
          "biens", bienImmobilierJson, File(file.path), null, files, "POST");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;
        if (!isError && response.statuscode == 200) {
          await initAll();
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // edition d'un bienImmobilier
  Future<Map<dynamic, dynamic>> editerBien(BienImmobilier bienImmobilier,
      PickedFile? file, List<XFile>? files) async {
    try {
      var bienImmobilierJson = bienImmobilier.toJson();

      // Suppression des champs vides
      bienImmobilierJson.removeWhere((key, value) => value == null);

      HttpResponse response = await Request().sendMultipartData(
          "biens/${bienImmobilier.id}?_method=PUT",
          bienImmobilierJson,
          file == null ? null : File(file.path),
          null,
          files,
          "POST");
      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          await initAll();
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Suppression d'un bienImmobilier
  Future<Map<dynamic, dynamic>>? delete(BienImmobilier bienImmobilier) async {
    try {
      HttpResponse response = await Request().send(
          "biens/${bienImmobilier.id}", bienImmobilier.toJson(), "DELETE");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError as bool;

        if (!isError && response.statuscode == 200) {
          await initAll();
          SoftDialog()
              .successDialog(Get.context!, responseBody.message!, "Succès", () {
            Get.off(() => ListBienImmobilierScreen());
          }, () {
            Get.off(() => ListBienImmobilierScreen());
          });
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  //
  List<Galerie> fetchGalery(List<dynamic> galerie) {
    galeryList
        .assignAll(galerie.map((data) => Galerie.fromJson(data)).toList());
    return galeryList;
  }

  int fetchIDByName(String name) {
    var bien = bienImmobiliersList.firstWhere((item) => item.name == name);
    return bien.id!;
  }

  String fetchNameByID(int id) {
    var bien = bienImmobiliersList.firstWhere((item) => item.id == id);
    return bien.name!;
  }

  String fetchAssetByID(int id) {
    var bien = bienImmobiliersList.firstWhere((item) => item.id == id);
    return bien.photo!;
  }

  List<Galerie> fetchAllAssetsByID(int id) {
    var bien = bienImmobiliersList.firstWhere((item) => item.id == id);
    return fetchGalery(bien.galerie!);
  }

  _alertDanger(String message) {
    SoftDialog(btnOkOnPress: "Réessayer")
        .dangerDialog(Get.context!, message, "Echec", () {}, () {});
  }
}
