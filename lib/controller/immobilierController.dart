import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Galery.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/models/network/Http.dart';
import 'package:score_immo/models/network/ResponseBody.dart';
import 'package:score_immo/screens/list_immobilier/list_immobilier_screen.dart';

class ImmobilierController extends GetxController
    with StateMixin<List<Immobilier>> {
  @override
  void onInit() {
    initAll();
    super.onInit();
  }

  @override
  void onReady() {
    initAll();
    super.onReady();
  }

  initAll() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await fetchImmobiliers();
      await fetchEquipements();
      await statistiquesImmobiliers();
    } else {
      change(null, status: RxStatus.error());
      _alertDanger("Désolé, vous n'avez pas accès à internet");
    }
  }

  RxList<Immobilier> immobiliersList = <Immobilier>[].obs;
  List<dynamic> equipementList = <dynamic>[];
  RxList<Galerie> galeryList = <Galerie>[].obs;
  Rx<StatistiqueImmobilier> statsData = StatistiqueImmobilier().obs;

  // Recherche
  search(String value) {
    List<Immobilier> newList = <Immobilier>[].obs;
    var list = immobiliersList.where((item) =>
        item.name!.contains("$value") ||
        item.immobilierType!.contains("$value"));
    newList.assignAll(list.map((data) => data));

    if (value.isNotEmpty && newList.isEmpty) {
      change(null, status: RxStatus.empty());
    } else if (value.isEmpty) {
      change(immobiliersList, status: RxStatus.success());
    } else {
      change(newList, status: RxStatus.success());
    }
  }

  // Recuperation de toutes les acquisitions enregistrées.
  Future<Map<dynamic, dynamic>> fetchImmobiliers() async {
    try {
      change(null, status: RxStatus.loading());
      HttpResponse response = await Request().send("immobiliers", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          AcquisitionFromData acquisitionData =
              AcquisitionFromData.fromJson(data);
          List<dynamic> acquisitionFetch = acquisitionData.acquisitions ?? [];

          immobiliersList.assignAll(acquisitionFetch
              .map((data) => Immobilier.fromJson(data))
              .toList());

          if (immobiliersList.isNotEmpty) {
            //log("immos: $immobiliersList");
            change(immobiliersList, status: RxStatus.success());
          } else {
            change(null, status: RxStatus.empty());
          }
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          change(null, status: RxStatus.error(message));
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        change(null, status: RxStatus.error(body));
        _alertDanger(body);
      }
    } catch (e) {
      log(e.toString());
      change(null, status: RxStatus.error(e.toString()));
      _alertDanger("Désolé, une erreur quelconque est survenue !");
    } finally {
      //
    }
    return {'status': false};
  }

  // Recuperation de tous équipements enregistrés.
  Future<Map<dynamic, dynamic>> fetchEquipements() async {
    try {
      HttpResponse response =
          await Request().send("immobiliers/equipements", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          Equipement equipementData = Equipement.fromJson(data);
          equipementList = equipementData.listeEquipements ?? [];
          // log("equipements: $equipementList");
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      log(e.toString());
      _alertDanger("Désolé, une erreur quelconque est survenue !");
    } finally {
      //
    }
    return {'status': false};
  }

  // Récuperation des statistiques concernant les acquisitions
  Future<Map<dynamic, dynamic>> statistiquesImmobiliers() async {
    try {
      HttpResponse response =
          await Request().send("statistiques/immobiliers", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          statsData = StatistiqueImmobilier.fromJson(data).obs;
          //log(jsonEncode(data));
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      log(e.toString());
      _alertDanger("Désolé, une erreur quelconque est survenue !");
    } finally {}
    return {'status': false};
  }

  // Creation d'une nouvelle acquisition
  Future<Map<dynamic, dynamic>> ajouterAcquision(
      Immobilier immobilier, PickedFile file, List<XFile> files) async {
    try {
      var immobilierJson = immobilier.toJson();
      immobilierJson.removeWhere((key, value) => value == null);
      HttpResponse response = await Request().sendMultipartData(
          "immobiliers", immobilierJson, File(file.path), null, files, "POST");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;
        if (!isError && response.statuscode == 200) {
          await initAll();
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      log(e.toString());
      _alertDanger("Désolé, une erreur quelconque est survenue ! immo");
    } finally {}
    return {"status": false};
  }

  // edition d'une acquisition
  Future<Map<dynamic, dynamic>> editerAcquision(
      Immobilier immobilier, PickedFile? file, List<XFile>? files) async {
    try {
      var immobilierJson = immobilier.toJson();
      // Suppression des champs vides
      immobilierJson.removeWhere(
          (key, value) => value == null || key == "photo" || key == "galerie");
      HttpResponse response = await Request().sendMultipartData(
          "immobiliers/${immobilier.id}?_method=PUT",
          immobilierJson,
          file == null ? null : File(file.path),
          null,
          files,
          "POST");
      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          await initAll();
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger("Désolé, une erreur quelconque est survenue !");
      log(e.toString());
      //_alertDanger(e.toString());
      //return {"status": false};
    } finally {}
    return {"status": false};
  }

  // Suppression d'une acquisition
  Future<Map<dynamic, dynamic>>? delete(Immobilier immobilier) async {
    try {
      HttpResponse response = await Request()
          .send("immobiliers/${immobilier.id}", immobilier.toJson(), "DELETE");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError as bool;

        if (!isError && response.statuscode == 200) {
          await initAll();
          SoftDialog()
              .successDialog(Get.context!, responseBody.message!, "Succès", () {
            Get.off(() => ListImmobilierScreen());
          }, () {
            Get.off(() => ListImmobilierScreen());
          });
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      log(e.toString());
      _alertDanger("Désolé, une erreur quelconque est survenue !");
    } finally {}
    return {"status": false};
  }

  // traite la galerie d'images
  List<Galerie> fetchGalery(List<dynamic> galerie) {
    galeryList
        .assignAll(galerie.map((data) => Galerie.fromJson(data)).toList());
    return galeryList;
  }

  String fetchNameByID(int id) {
    var immo = immobiliersList.firstWhere((element) => element.id == id);
    return immo.name!;
  }

  int fetchIDByName(String name) {
    var immo = immobiliersList.firstWhere((element) => element.name == name);
    return immo.id!;
  }

  _alertDanger(String message) {
    SoftDialog(btnOkOnPress: "Réessayer")
        .dangerDialog(Get.context!, message, "Echec", () {}, () {});
  }
}
