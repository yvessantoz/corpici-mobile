import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Galery.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/models/network/Http.dart';
import 'package:score_immo/models/network/ResponseBody.dart';
import 'dart:isolate';
import 'dart:ui';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

class LocationController extends GetxController
    with StateMixin<List<Location>> {
  @override
  void onReady() {
    initAll();
    super.onReady();
  }

  @override
  void onInit() {
    initAll();
    initDonwload();
    super.onInit();
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  // Stocke la liste des locations
  RxList<Location> locationList = <Location>[].obs;
  RxList<Galerie> galeryList = <Galerie>[].obs;
  Rx<StatistiqueLocation> statsData = StatistiqueLocation().obs;
  ReceivePort _port = ReceivePort();

  initAll() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await fetchLocationWhere();
      await statistiquesLocations();
    } else {
      change(null, status: RxStatus.error());
      _alertDanger("Désolé, vous n'avez pas accès à internet");
    }
  }

  // Recherche
  search(String value) {
    List<Location> newList = <Location>[].obs;
    var list = locationList.where((item) =>
        item.name!.contains("$value") ||
        item.montant!.contains("$value") ||
        item.modesPaiement!.contains("$value") ||
        item.locationType!.contains("$value") ||
        item.recurencePaiement!.contains("$value"));
    newList.assignAll(list.map((data) => data));

    if (value.isNotEmpty && newList.isEmpty) {
      change(null, status: RxStatus.empty());
    } else if (value.isEmpty) {
      change(locationList, status: RxStatus.success());
    } else {
      change(newList, status: RxStatus.success());
    }
  }

  // Creation d'une location
  Future<Map<dynamic, dynamic>> creationlocation(
      Location location, File? file) async {
    try {
      // HttpResponse response = await Request()
      //     .send("locations", cleanLocationData(location.toJson()), "POST");

      HttpResponse response = await Request().sendMultipartData("locations",
          cleanLocationData(location.toJson()), file, null, null, "POST");

      Map<dynamic, dynamic> result = await requestResult(response, null);
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Enregistrement d'un location simple
  Future<Map<dynamic, dynamic>> simpleSave(
      Location location, File? file) async {
    try {
      HttpResponse response = await Request()
          .send("locations", cleanLocationData(location.toJson()), "POST");

      Map<dynamic, dynamic> result = await requestResult(response, "location");
      return result;
    } catch (e) {}
    return {"status": false};
  }

  // Mise à jour d'un location simple
  Future<Map<dynamic, dynamic>> simpleUpdate(
      Location location, File? file) async {
    try {
      HttpResponse response = await Request()
          .send("locations", cleanLocationData(location.toJson()), "PUT");

      Map<dynamic, dynamic> result = await requestResult(response, "location");
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Mise à jour de compte
  Future<Map<dynamic, dynamic>> updateLocation(
      Location location, File? file) async {
    try {
      // HttpResponse response = await Request().send(
      //     "locations/${location.id}?_method=PUT",
      //     cleanLocationData(location.toJson()),
      //     "POST");

      HttpResponse response = await Request().sendMultipartData(
          "locations/${location.id}?_method=PUT",
          cleanLocationData(location.toJson()),
          file,
          null,
          null,
          "POST");
      Map<dynamic, dynamic> result = await requestResult(response, null);
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Retourne le résultat de la requête
  Future<Map<dynamic, dynamic>> requestResult(
      HttpResponse response, String? type) async {
    try {
      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body!));
        bool isError = responseBody.isError!;
        //log("is_error: ${responseBody.data}");

        if (!isError) {
          //Map<String, dynamic> data = responseBody.data!;
          //Location locationData = Location.fromJson(data);
          //log(jsonEncode(locationData));
          await initAll();
          return {"status": true};
        } else {
          String message = responseBody.message!;
          _alertDanger(message);
        }
      } else {
        String body = response.body!;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Suppression de compte
  Future<Map<dynamic, dynamic>>? delete(Location location) async {
    try {
      HttpResponse response = await Request()
          .send("locations/${location.id}", location.toJson(), "DELETE");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError as bool;

        if (!isError && response.statuscode == 200) {
          await initAll();
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Générer un contrat de bail et télécharger
  genererContrat(Location location) async {
    try {
      change(null, status: RxStatus.loading());
      HttpResponse response = await Request().send(
          "locations/contrat_location_existante/${location.id}", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError ?? false;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          ContratLocation contratLocation = ContratLocation.fromJson(data);
          location.contratBail = contratLocation.lien;
          if (contratLocation.lien != null) {
            download(contratLocation.lien!);
          }

          SoftDialog(btnOkOnPress: "OK", btnCancelText: "Fermer").successDialog(
              Get.context!,
              "${responseBody.message}. \nVotre contrat a bien été téléchargé.",
              "Succès",
              () {},
              () {});

          return {"status": true};
        } else {
          String message = responseBody.message as String;
          change(null, status: RxStatus.error(message));
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        change(null, status: RxStatus.error(body));
        _alertDanger(body);
      }
    } catch (e) {
      log(e.toString());
      _alertDanger(e.toString());
      change(null, status: RxStatus.error(e.toString()));
    }
    return {"status": false};
  }

  // Recupère la liste des locations un proprietaire
  fetchLocationWhere() async {
    try {
      change(null, status: RxStatus.loading());
      HttpResponse response = await Request().send("locations", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError ?? false;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;

          LocationFromData locationData = LocationFromData.fromJson(data);

          List<dynamic> locationFetch = locationData.locations ?? [];

          locationList.assignAll(
              locationFetch.map((data) => Location.fromJson(data)).toList());

          if (locationList.isNotEmpty) {
            change(locationList, status: RxStatus.success());
          } else {
            change(null, status: RxStatus.empty());
          }
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          change(null, status: RxStatus.error(message));
          //_alertDanger(message);
        }
      } else {
        String body = response.body as String;
        change(null, status: RxStatus.error(body));
        _alertDanger(body);
      }
    } catch (e) {
      log("location: ${e.toString()}");
      _alertDanger(e.toString());
      change(null, status: RxStatus.error(e.toString()));
    }
    return {"status": false};
  }

  //
  Future<Map<dynamic, dynamic>> statistiquesLocations() async {
    try {
      HttpResponse response =
          await Request().send("statistiques/locations", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          statsData = StatistiqueLocation.fromJson(data).obs;
          //log(jsonEncode(data));
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          //_alertDanger(message);
        }
      } else {
        String body = response.body as String;
        //_alertDanger(body);
      }
    } catch (e) {}
    return {'status': false};
  }

  //
  initDonwload() async {
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');

    _port.listen((dynamic data) {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
    });

    FlutterDownloader.registerCallback(downloadCallback);
  }

  //
  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  // Téléchargement d'un contrat
  void download(String url) async {
    final status = await Permission.storage.request();

    if (status.isGranted) {
      if (url.isNotEmpty && url != 'null') {
        final externalDir = await getExternalStorageDirectory();

        final id = await FlutterDownloader.enqueue(
          url: url,
          savedDir: externalDir!.path,
          showNotification: true,
          openFileFromNotification: true,
        );
      } else {
        _alertDanger('Aucune ressource identifiée !');
      }
    } else {
      _alertDanger('Permission non accordée !');
    }
  }

  // Retire les champs inutiles
  Map<dynamic, dynamic> cleanLocationData(Map<dynamic, dynamic> location) {
    location.removeWhere((key, value) => value == null);
    return location;
  }

  //
  List<Galerie> fetchGalery(List<dynamic> galerie) {
    galeryList
        .assignAll(galerie.map((data) => Galerie.fromJson(data)).toList());
    return galeryList;
  }

  // Affiche les messages d'erreur
  _alertDanger(String message) {
    SoftDialog(btnOkOnPress: "Réessayer")
        .dangerDialog(Get.context!, message, "Echec", () {}, () {});
  }
}
