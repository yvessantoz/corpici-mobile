import 'dart:convert';
import 'dart:developer';

import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/controller/appController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/models/network/Http.dart';
import 'package:score_immo/models/network/ResponseBody.dart';
import 'package:score_immo/screens/sign_in/sign_in_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserController extends GetxController with StateMixin<List<User>> {
  @override
  void onReady() {
    authCheck();
    super.onReady();
  }

  @override
  void onInit() {
    authCheck();
    super.onInit();
  }

  authCheck() async {
    var tok = await token();
    bool connected = await Request().isConnected();
    if (tok.isNotEmpty && connected) {
      await initAll();
    } else if (!connected) {
      change(null, status: RxStatus.error());
      _alertDanger("Désolé, vous n'avez pas accès à internet");
    }
  }

  // Stocke la liste des utilisateurs
  RxList<User> userList = <User>[].obs;
  RxList<User> usersList = <User>[].obs;
  Rx<StatistiqueLocataire> statsData = StatistiqueLocataire().obs;

  initAll() async {
    await fetchUserWhere(false);
    await fetchUserWhere(true);
    await statistiquesLocataires();
  }

  // Recherche
  search(String value) {
    List<User> newList = <User>[].obs;
    var list = userList.where((item) =>
            item.firstName!.contains("$value") ||
            item.lastName!.contains("$value") ||
            //item.agenceName!.contains("$value") ||
            //item.agencePhone!.contains("$value") ||
            item.dateNaissance!.contains("$value") ||
            item.email!.contains("$value") ||
            item.mobile!.contains("$value") ||
            item.userType!.contains("$value") ||
            //item.domaineActivite!.contains("$value") ||
            item.numeroPiece!.contains("$value")
        //item.pseudo!.contains("$value") ||
        //item.qualification!.contains("$value") ||
        //item.revenuMensuel!.contains("$value") ||
        //item.numeroCompteContribuable!.contains("$value") ||
        //item.agenceRegistreCommerce!.contains("$value") ||
        //item.numeroRegistreCommerce!.contains("$value") ||
        //item.qualification!.contains("$value")
        );
    newList.assignAll(list.map((data) => data));

    if (value.isNotEmpty && newList.isEmpty) {
      change(null, status: RxStatus.empty());
    } else if (value.isEmpty) {
      change(userList, status: RxStatus.success());
    } else {
      change(newList, status: RxStatus.success());
    }
  }

  // Connexion
  Future<Map<dynamic, dynamic>> login(String login, String password) async {
    // Contient le corps de la requête
    var querydata;

    bool isEmail = login.isEmail;
    bool isMobile = login.isNumericOnly;

    if (isEmail) {
      querydata = {"email": login, "password": password};
    } else if (isMobile) {
      querydata = {"mobile": login, "password": password};
    } else {
      querydata = {"pseudo": login, "password": password};
    }

    try {
      HttpResponse response = await Request().send("login", querydata, "POST");
      Map<dynamic, dynamic> result = await requestResult(response, null, null);
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Inscription
  Future<Map<dynamic, dynamic>> register(User user) async {
    try {
      HttpResponse response = await Request()
          .send("register", cleanUserData(user.toJson()), "POST");

      Map<dynamic, dynamic> result = await requestResult(response, null, null);
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Enregistrement d'un utilisateur simple (locataire)
  Future<Map<dynamic, dynamic>> simpleSave(User user) async {
    try {
      HttpResponse response = await Request()
          .send("locataire", cleanUserData(user.toJson()), "POST");

      Map<dynamic, dynamic> result =
          await requestResult(response, "locataire", null);
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Mise à jour d'un utilisateur simple (locataire)
  Future<Map<dynamic, dynamic>> simpleUpdate(User user) async {
    try {
      HttpResponse response = await Request().send(
          "locataire/${user.id}?_method=PUT",
          cleanUserData(user.toJson()),
          "POST");

      Map<dynamic, dynamic> result =
          await requestResult(response, "locataire", null);
      return result;
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Mise à jour de compte
  Future<Map<dynamic, dynamic>> updateUser(User user) async {
    try {
      HttpResponse response = await Request().send(
          "users/${user.id}?_method=PUT", cleanUserData(user.toJson()), "POST");
      Map<dynamic, dynamic> result =
          await requestResult(response, null, "update");

      return result;
    } catch (e) {
      print(e.toString());
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  // Retourne le résultat de la requête
  Future<Map<dynamic, dynamic>> requestResult(
      HttpResponse response, String? type, String? requestType) async {
    bool result;

    try {
      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body!));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          UserFromData userData = UserFromData.fromJson(data);
          User user = User.fromJson(userData.user ?? userData.profil!);
          if (type != "locataire") {
            if (requestType == "update") {
              result = await updatePreferences(true, user, "user");
            } else if (user.userType == "PROPRIETAIRE" ||
                user.userType == "AGENCE") {
              user.token = userData.token;
              result = await setPreferences(true, user, "user");
            } else {
              result = false;
            }
          } else {
            await initAll();
            result = true;
          }
          return {"status": result};
        } else {
          String message = responseBody.message!;
          _alertDanger(message);
        }
      } else {
        String body = response.body!;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
      print(e.toString());
    }
    return {"status": false};
  }

  // Suppression de compte
  Future<Map<dynamic, dynamic>>? delete(User user) async {
    try {
      HttpResponse response =
          await Request().send("users/${user.id}", user.toJson(), "DELETE");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError as bool;

        if (!isError && response.statuscode == 200) {
          User sessionUser = await getUser("user");
          if (user.id == sessionUser.id) {
            logOut();
          } else {
            await initAll();
            return {"status": true};
          }
        } else {
          String message = responseBody.message as String;
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        _alertDanger(body);
      }
    } catch (e) {
      _alertDanger(e.toString());
    }
    return {"status": false};
  }

  /// Enregistre les données en session
  Future<bool> setPreferences(bool isLogged, User user, String key) async {
    SharedPreferences prefs = await AppController().prefs();

    if (isLogged) {
      await prefs.setString(key, jsonEncode(user));
      await prefs.setBool("isLogin", isLogged);
    } else if (key == "register") {
      await prefs.setString(key, jsonEncode(user));
    }
    return true;
  }

  /// Mise a jour de profil
  Future<bool> updatePreferences(bool isLogged, User user, String key) async {
    SharedPreferences prefs = await AppController().prefs();
    if (isLogged) {
      var sessionUser = await getUser(key);
      String sessionToken = sessionUser.token!;
      user.token = sessionToken;
      await prefs.setString(key, jsonEncode(user));
    }
    return true;
  }

  // Determine l'etat actuel du compte
  Future<bool> isLogin() async {
    SharedPreferences prefs = await AppController().prefs();
    bool isLogin = prefs.getBool('isLogin') ?? false;
    return isLogin;
  }

  /// Retourne les données utilisateur en session
  Future<User> getUser(String key) async {
    SharedPreferences prefs = await AppController().prefs();
    var user = prefs.get(key); //"user"
    if (user != null) {
      user = user as String;
      User userMap = User.fromJson(jsonDecode(user));
      return userMap;
    }
    return User();
  }

  // Recupère la liste des utilisateurs
  fetchUserWhere(bool all) async {
    try {
      change(null, status: RxStatus.loading());

      HttpResponse response = await Request()
          .send(all == true ? "locataire/?wrap=true" : "users", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError ?? false;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          UserFromData userData = UserFromData.fromJson(data);
          List<dynamic> userFetch = userData.users ?? [];

          if (all == true) {
            usersList.assignAll(
                userFetch.map((data) => User.fromJson(data)).toList());
          } else {
            userList.assignAll(
                userFetch.map((data) => User.fromJson(data)).toList());
          }

          if (all == true && usersList.isNotEmpty) {
            change(usersList, status: RxStatus.success());
          } else if (all == false && userList.isNotEmpty) {
            change(userList, status: RxStatus.success());
          } else {
            change(null, status: RxStatus.empty());
          }
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          change(null, status: RxStatus.error(message));
          _alertDanger(message);
        }
      } else {
        String body = response.body as String;
        change(null, status: RxStatus.error(body));
        _alertDanger(body);
      }
    } catch (e) {
      log("${e.toString()}");
      _alertDanger(e.toString());
      change(null, status: RxStatus.error(e.toString()));
    }
    return {"status": false};
  }

  //
  Future<Map<dynamic, dynamic>> statistiquesLocataires() async {
    try {
      HttpResponse response =
          await Request().send("statistiques/locations", null, "GET");

      if (response.statuscode != -1 && response.statuscode != 12029) {
        ResponseBody responseBody =
            ResponseBody.fromJson(jsonDecode(response.body.toString()));
        bool isError = responseBody.isError!;

        if (!isError && response.statuscode == 200) {
          Map<String, dynamic> data = responseBody.data!;
          statsData = StatistiqueLocataire.fromJson(data).obs;
          //log(jsonEncode(data));
          return {"status": true};
        } else {
          String message = responseBody.message as String;
          //_alertDanger(message);
        }
      } else {
        String body = response.body as String;
        // _alertDanger(body);
      }
    } catch (e) {}
    return {'status': false};
  }

  // Retire les champs inutiles
  Map<dynamic, dynamic> cleanUserData(Map<dynamic, dynamic> user) {
    user.removeWhere((key, value) => value == null || key == "id");
    return user;
  }

  // Retourne le jeton après authentification
  Future<String> token() async {
    User user = await getUser("user");
    return user.token ?? '';
  }

  // Déconnecte le compte actuel
  logOut() async {
    SharedPreferences prefs = await AppController().prefs();
    bool stateUser = await prefs.remove("user");
    bool stateLogin = await prefs.remove("isLogin");
    if (stateUser && stateLogin) {
      await AppController().deleteAllController();
    }
  }

  int fetchIDByName(String name) {
    var user = userList
        .firstWhere((item) => item.lastName! + ' ' + item.firstName! == name);
    return user.id!;
  }

  String fetchNameByID(int id) {
    var user = userList.firstWhere((item) => item.id! == id);
    return user.lastName! + ' ' + user.firstName!;
  }

  // Affiche les messages d'erreur
  _alertDanger(String message) {
    SoftDialog(btnOkOnPress: "Réessayer")
        .dangerDialog(Get.context!, message, "Echec", () {}, () {});
  }
}
