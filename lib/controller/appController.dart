import 'package:get/get.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/screens/sign_in/sign_in_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppController extends GetxController {
  // Retourne une instance de session
  Future<SharedPreferences> prefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  //
  Future<bool> isInstalled() async {
    SharedPreferences preferences = await prefs();
    bool isIntalleds = preferences.getBool("isInstalled") ?? false;
    return isIntalleds;
  }

  //
  install() async {
    SharedPreferences preferences = await prefs();
    bool result = await preferences.setBool("isInstalled", true);
    if (result) {
      Get.offAll(SignInScreen());
    }
  }

  deleteAllController() async {
    await Get.delete<BienImmobilierController>();
    await Get.delete<ImmobilierController>();
    await Get.delete<LocationController>();
    await Get.delete<UserController>();
    Get.offAll(() => SignInScreen());
  }
}
