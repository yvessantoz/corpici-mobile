import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:path/path.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/network/Http.dart';
import 'package:connectivity/connectivity.dart';

class Request extends GetxController {
  // Instancie le client http
  var client = http.Client();

  // ignore: constant_identifier_names
  static const String PORT = ":8000";
  // ignore: constant_identifier_names
  static const String SERVER = "192.168.1.2";
  // ignore: constant_identifier_names
  static const String BASE_URL_LOCAL = "http://" + SERVER + PORT + "/api/";
  // ignore: constant_identifier_names
  static const String BASE_URL_PRODUCTION =
      "www.corpici-server.straton-system.com";
  static const int _noInternet = 12029;
  static const IMAGE_LOAD_BASE = "http://" + BASE_URL_PRODUCTION + "/photos/";
  var responseBody = "";

  // Les types de connectivité que l'on pourrait avoir
  static const Set<ConnectivityResult> connectivityMethod = {
    ConnectivityResult.mobile,
    ConnectivityResult.wifi
  };

  /// SEND REQUEST METHOD
  Future<HttpResponse> send(
      String endpoint, Map<dynamic, dynamic>? body, String method) async {
    // Stocke l'etat de la connexion de l'utilisateur
    bool connected = await isConnected();

    // Récupération de l'entête de la requete;
    Map<String, String> headers = await _headers(method);

    // Stocke le résultat de la requête
    var response;

    try {
      if (connected) {
        // url:: Stocke l'url complète de la requête
        Uri url = Uri.https(BASE_URL_PRODUCTION, '/api/' + endpoint);
        //log("url: ${url.toString()}");

        // Recherche la bonne requête en fonction de la methode spécifiée
        switch (method) {
          case "POST":
            response = await client.post(url, body: body, headers: headers);
            break;
          case "PUT":
            response = await client.put(url, body: body, headers: headers);
            break;
          case "GET":
            response = await client.get(url, headers: headers);
            //log("response: ${response.body}");
            break;
          case "DELETE":
            response = await client.delete(url, headers: headers);
            break;
          default:
            response = await client.get(url, headers: headers);
            break;
        }
        if (response != null) {
          //log(response.body);
          return HttpResponse(
              statuscode: response.statusCode, body: response.body);
        }
      } else {
        //_alertDanger();
        return HttpResponse(
            statuscode: _noInternet,
            body: "Vous n'avez pas accès à internet !");
      }
    } catch (e) {
      // ignore: avoid_print
      print("HTTP Request catch print: ${e.toString()}");
    } finally {
      client.close();
    }
    return HttpResponse(
        statuscode: -1,
        body: "Oops! Nous n'avons pas pu effectuer cette requête.");
  }

  /// SEND MULTIPARTDATA REQUEST METHOD
  Future<HttpResponse> sendMultipartData(
      String endpoint,
      Map<dynamic, dynamic>? body,
      File? file,
      String? fileName,
      List<XFile>? files,
      String method) async {
    // Stocke l'etat de la connexion de l'utilisateur
    bool connected = await isConnected();

    // Récupération de l'entête de la requete;
    String _token = await UserController().token();
    Map<String, String> headers = {
      "Authorization": "Bearer $_token",
      "Accept": "application/json",
      "Content-type": "multipart/form-data"
    };

    // Stocke le résultat de la requête
    var response;

    try {
      if (connected) {
        // url:: Stocke l'url complète de la requête
        //log("$BASE_URL_PRODUCTION/api/$endpoint");
        Uri url = Uri.https(BASE_URL_PRODUCTION, '/api/' + endpoint);
        var request = http.MultipartRequest(method, url);

        // Traitement des fichiers avant envoi
        var singleFile;
        var allFiles;

        if (file != null) {
          singleFile =
              await singleMultipart(File(file.path), fileName ?? "photo");
        }

        //
        if (files != null && file != null) {
          allFiles = await parseMultipleImage(files, singleFile);
        } else if (file == null && files != null) {
          allFiles = await parseMultipleImage(files, null);
        }

        //
        if (file != null && files != null) {
          request.files.addAll(allFiles);
        } else if (file != null) {
          request.files.add(singleFile);
        } else if (files != null) {
          request.files.addAll(allFiles);
        }

        Map<String, String> realBody = {};

        body!.forEach((key, value) {
          if (key == "equipements") {
            realBody["$key[]"] = "$value";
          } else {
            realBody["$key"] = "$value";
          }
        });

        request.headers.addAll(headers);
        request.fields.addAll(realBody);
        response = await request.send();
        print(jsonEncode(realBody));
        await response.stream.transform(utf8.decoder).listen((value) {
          log('value');
          //log(jsonEncode(value));
          this.responseBody = value;
        });

        if (response != null) {
          return HttpResponse(
              statuscode: response.statusCode, body: responseBody);
        }
      } else {
        //_alertDanger();
        return HttpResponse(
            statuscode: _noInternet,
            body: "Vous n'avez pas accès à internet !");
      }
    } catch (e) {
      // ignore: avoid_print
      print("HTTP Request catch print: ${e.toString()}");
    } finally {
      client.close();
    }
    return HttpResponse(
        statuscode: -1, body: "Oops! Quelque chose s'est mal passé.");
  }

  // Stockes tous les fichiers dans un tableau
  Future<List<http.MultipartFile>> parseMultipleImage(
      List<XFile> imagesList, http.MultipartFile? file) async {
    List<http.MultipartFile> newList = <http.MultipartFile>[].obs;

    for (int i = 0; i < imagesList.length; i++) {
      File imageFile = File(imagesList[i].path);
      var multipartFile = await singleMultipart(imageFile, "galerie[]");
      newList.add(multipartFile);
    }
    if (file != null) {
      newList.add(file);
    }
    return newList;
  }

  // traite un seul fichier
  Future<http.MultipartFile> singleMultipart(
      File file, String groupName) async {
    var stream = new http.ByteStream(file.openRead());
    var length = await file.length();
    return http.MultipartFile(groupName, stream, length,
        filename: basename(file.path));
  }

  // Retourne l'entête de la requête
  Future<Map<String, String>> _headers(String method) async {
    // Récupération du jeton mis en session
    String _token = await UserController().token();

    var headers;

    if (_token.isNotEmpty) {
      headers = {
        "Authorization": "Bearer $_token",
        "Accept": "application/json",
        "Access-Control-Allow-Origin": BASE_URL_PRODUCTION,
        "Vary": "Origin"
      };
    } else {
      headers = {
        "Accept": "application/json",
        "Access-Control-Allow-Origin": SERVER,
        "Vary": "Origin"
      };
    }
    return headers;
  }

  // Vérifie si l'utilisateur est connecté à internet
  Future<bool> isConnected() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityMethod.contains(connectivityResult)) {
      return true;
    } else {
      return false;
    }
  }

  // Affiche les erreurs rencontrées sur le réseau
  void _alertDanger() {
    SoftDialog().dangerDialog(
        Get.context as BuildContext,
        "Désolé vous n'avez pas accès à internet !",
        "Erreur internet",
        () {},
        () {});
  }
}
