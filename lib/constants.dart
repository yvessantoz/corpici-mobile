import 'package:flutter/material.dart';
import 'package:score_immo/size_config.dart';

const APP_NAME = "CORPICI";
const CGU =
    "En poursuivant vous acceptez nos termes \net conditions générales d'utilisation.";
const APP_LOGO = "assets/images/logo_corpici_1.png";
const kPrimaryColor = Color(0xFF043eb5); //Color(0xFF249b92)
//Color(0xFFFF7643);
const kSecoundaryColor = Color(0xFF9a6c24); //Color(0xFF11435a)
const kPrimaryLightColor = Color(0xFFFFECDF); //Color(0xFFFFECDF)
const kBlueColor = Color(0xFF247ba9);

const kPrimaryGradientColor = LinearGradient(
    begin: Alignment.topLeft,
    end: Alignment.bottomRight,
    colors: [
      Color(0xFF04047c),
      Color(0xFF043eb5)
    ]); //[Color(0xFF11435a), Color(0xFF247ba9)]
//[Color(0xFFFFA53E), Color(0xFFFF7643)],
const kSecondaryColor = Color(0xFF979797); // Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
final RegExp pseudoValidatorRegExp = RegExp(r'^[a-zA-Z0-9]+$');
const String kLoginNullError =
    "Veuillez saisir l'identifiant (email, pseudo \nou mobile)";

const String kEmailNullError = "Veuillez saisir votre adresse email";
const String kInvalidEmailError = "Entrez une adresse email correcte";
const String kInvalidIdentifyError = "Entrez un identifiant correct";
const String kInvalidPseudoError = "Le pseudo ne doit pas contenir d'espace";
const String kPassNullError = "Veuillez saisir un mot de passe";
const String kShortPassError = "Mot de passe trop court";
const String kMatchPassError = "Les mots de passe ne correspondent pas";
const String kNamelNullError = "Entrez votre nom de famille";
const String kFirstNamelNullError = "Entrez votre prénom";
const String kPhoneNumberNullError = "Entrez votre numéro de téléphone";
const String kPhoneNumericError = "Veuillez entrer que les chiffres";
const String kPhoneMinChar = "Veuillez saisir au moins 10 chiffres";
const String kBirthPlaceNullError = "Entrez votre lieu de naissance";
const String kTypeIDNullError = "Choisissez le type de pièce";
const String kIDNullError = "Entrez votre numéro de pièce";
const String kExpirationIDNullError =
    "Entrez la date d'expiration de votre pièce";
const String kIncomeNullError = "Spécifiez votre revenu mensuel";
const String kFonctionNullError = "Entrez votre fonction";
const String kProfessionalStatusNullError = "Entrez votre status professionnel";
const String kDomainActivityNullError = "Entrez votre domaine d'activité";
const String kQualificationNullError = "Entrez votre qualification";
const String kBirthDateNullError = "Entrer votre date de naissance";
const String kTaxpayerNullError = "Numéro de compte contribuable obligatoire";
const String kTaxCommercialRegisterNullError =
    "Numéro registre de commerce obligatoire";
const String kAgencyNullError = "Veuillez renseigner le nom de l'agence";

const String kNameNullError = "Veuillez siasir un libellé";
const String kAdresseNullError = "Veuillez entrer une adresse";
const String kPaysNullError = "Veuillez entrer le pays";
const String kRegionNullError = "Veuillez entrer la région";
const String kVilleNullError = "Veuillez entrer la ville";
const String kDateConstructionNullError =
    "Veuillez entrer la date de constrcution";
const String kDescriptionNullError = "Veuillez entrer une description";
const String equipements = "Veuillez saisir les équipements";
const String kNbPieceNullError = "Veuillez definir le nombre de pièce";
const String kNbChambreNullError = "Veuillez definir le nombre de chambre";
const String kNbSalleBainNullError =
    "Veuillez definir le nombre de salle de bain";

const String kDureeBailNullError = "Entrer la durée du bail";
const String kMontantNullError = "Entrer le montant de la location";
const String kTauxTvaNullError = "Entrer le taux tva";
const String kDateDebutBailNullError = "Entrer la date de début";
const String kDateFinBailNullError = "Entrer la date de fin";

const String verificationText = "Vérification ...";
const String repeatText = "Réessayer";

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}
