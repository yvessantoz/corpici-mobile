class StatistiqueLocataire {
  StatistiqueLocataire({this.totalLocataire, this.totalLocation});

  int? totalLocataire;
  int? totalLocation;

  StatistiqueLocataire.fromJson(Map<String, dynamic> json) {
    totalLocataire = json['total_locataire'];
    totalLocation = json['total_location'];
  }
}

class UserFromData {
  String? token;
  Map<String, dynamic>? user;
  Map<String, dynamic>? profil;
  List<dynamic>? users;

  UserFromData({this.token, this.user, this.users});

  UserFromData.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    user = json['user'];
    profil = json['profil'];
    users = json['users'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = this.token;
    data['user'] = this.user;
    data['users'] = this.users;
    return data;
  }
}

class User {
  int? id;
  String? userType;
  String? firstName;
  String? lastName;
  String? dateNaissance;
  String? lieuNaissance;
  String? typePiece;
  String? numeroPiece;
  String? expirationPiece;
  String? statutProfessionnel;
  String? fonction;
  String? domaineActivite;
  String? qualification;
  String? revenuMensuel;
  String? numeroRegistreCommerce;
  String? numeroCompteContribuable;
  String? agenceName;
  String? agencePhone;
  String? agenceRegistreCommerce;
  String? agenceCompteContribuable;
  String? pseudo;
  String? mobile;
  String? email;
  String? password;
  String? passwordConfirmation;
  String? token;
  String? avatar;

  // Constructeur de la classe User
  User(
      {this.id,
      this.userType,
      this.firstName,
      this.lastName,
      this.dateNaissance,
      this.lieuNaissance,
      this.typePiece,
      this.numeroPiece,
      this.expirationPiece,
      this.statutProfessionnel,
      this.fonction,
      this.domaineActivite,
      this.qualification,
      this.revenuMensuel,
      this.numeroRegistreCommerce,
      this.numeroCompteContribuable,
      this.agenceName,
      this.agencePhone,
      this.agenceRegistreCommerce,
      this.agenceCompteContribuable,
      this.pseudo,
      this.mobile,
      this.email,
      this.password,
      this.passwordConfirmation,
      this.token,
      this.avatar = "assets/images/avatar.png"});

  // Converti les données json en des données de type user
  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userType = json['user_type'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    dateNaissance = dateFormat("${json['date_naissance']}");
    lieuNaissance = json['lieu_naissance'];
    typePiece = json['type_piece'];
    numeroPiece = json['numero_piece'];
    expirationPiece = dateFormat("${json['expiration_piece']}");
    statutProfessionnel = json['statut_professionnel'];
    fonction = json['fonction'];
    domaineActivite = json['domaine_activite'];
    qualification = json['qualification'];
    revenuMensuel = json['revenu_mensuel'];
    numeroRegistreCommerce = json['numero_registre_commerce'];
    numeroCompteContribuable = json['numero_compte_contribuable'];
    agenceName = json['agence_name'];
    agencePhone = json['agence_phone'];
    agenceRegistreCommerce = json['agence_registre_commerce'];
    agenceCompteContribuable = json['agence_compte_contribuable'];
    pseudo = json['pseudo'];
    email = json['email'];
    mobile = json['mobile'];
    password = json['password'];
    passwordConfirmation = json['password_confirmation'];
    token = json['token'];
    avatar = json['avatar'] ?? "assets/images/avatar.png";
  }

  // Converti les données de type user en json
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this.id;
    data['user_type'] = this.userType;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['date_naissance'] = this.dateNaissance;
    data['lieu_naissance'] = this.lieuNaissance;
    data['type_piece'] = this.typePiece;
    data['numero_piece'] = this.numeroPiece;
    data['expiration_piece'] = this.expirationPiece;
    data['statut_professionnel'] = this.statutProfessionnel;
    data['fonction'] = this.fonction;
    data['domaine_activite'] = this.domaineActivite;
    data['qualification'] = this.qualification;
    data['revenu_mensuel'] = this.revenuMensuel;
    data['numero_registre_commerce'] = this.numeroRegistreCommerce;
    data['numero_compte_contribuable'] = this.numeroCompteContribuable;
    data['agence_name'] = this.agenceName;
    data['agence_phone'] = this.agencePhone;
    data['agence_registre_commerce'] = this.agenceRegistreCommerce;
    data['agence_compte_contribuable'] = this.agenceCompteContribuable;
    data['pseudo'] = this.pseudo;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['password'] = this.password;
    data['password_confirmation'] = this.passwordConfirmation;
    data['token'] = this.token;
    data['avatar'] = this.avatar;
    return data;
  }

  String dateFormat(String date) {
    return date.split(" ")[0];
  }
}

/* Variables initiales destinées aux champs select */
List<String> professionnalStatusItems = ["Statut professionnel", "SALARIE"];
List<String> typeIDItems = [
  "Type de pièce",
  "CNI",
  "PASSPORT",
  "CARTE CONSULAIRE",
  "ATTESTATION"
];
List<String> typeUserItems = [
  "Type d'utilisateur",
  "AGENCE",
  /*"LOCATAIRE",*/
  "PROPRIETAIRE",
];
/* Variables initiales destinées aux champs select */
