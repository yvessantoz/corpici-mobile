import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/models/User.dart';

class StatistiqueLocation {
  StatistiqueLocation({this.totalLocation});

  int? totalLocation;

  StatistiqueLocation.fromJson(Map<String, dynamic> json) {
    totalLocation = json['total_location'];
  }
}

class ContratLocation {
  ContratLocation({this.lien});

  String? lien;

  ContratLocation.fromJson(Map<String, dynamic> json) {
    lien = json['lien'];
  }
}

class LocationFromData {
  Map<dynamic, dynamic>? location;
  List<dynamic>? locations;

  LocationFromData({this.location, this.locations});

  LocationFromData.fromJson(Map<String, dynamic> json) {
    location = json['location'];
    locations = json['locations'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['location'] = this.location;
    data['locations'] = this.locations;
    return data;
  }
}

class Location {
  int? id;
  String? locationType;
  String? name;
  String? slugName;
  String? dureeBail;
  String? debutBail;
  String? finBail;
  String? isRenouvellable;
  String? recurencePaiement;
  String? modesPaiement;
  String? montant;
  String? isTva;
  String? tauxTva;
  List<String>? images;
  int? locataire;
  String? contratBail;
  int? bienId;
  String? etat;
  User? userLocataire;
  BienImmobilier? bien;

  Location(
      {this.id,
      this.locationType,
      this.name,
      this.slugName,
      this.dureeBail,
      this.debutBail,
      this.finBail,
      this.isRenouvellable,
      this.recurencePaiement,
      this.modesPaiement,
      this.montant,
      this.isTva,
      this.tauxTva,
      this.images,
      this.locataire,
      this.contratBail,
      this.bienId,
      this.etat = "ACTIF",
      this.userLocataire,
      this.bien});

  Location.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.locationType = json['location_type'];
    this.name = json['name'];
    this.slugName = json['slug_name'];
    this.dureeBail = "${json['duree_bail']}";
    this.debutBail = json['debut_bail'];
    this.finBail = json['fin_bail'];
    this.isRenouvellable = "${json['is_renouvellable']}";
    this.recurencePaiement = json['recurence_paiement'];
    this.modesPaiement = json['modes_paiement'];
    this.montant = "${json['montant']}";
    this.isTva = "${json['is_tva']}";
    this.tauxTva = "${json['taux_tva']}";
    this.images = json['images'];
    this.locataire = json['user_id'];
    this.contratBail = json['contrat_bail'];
    this.bienId = json['bien_immobilier_id'];
    this.userLocataire = User.fromJson(json['locataire']);
    this.bien = BienImmobilier.fromJson(json['bien']);
    this.etat = json['etat'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this.id;
    data['location_type'] = this.locationType;
    data['name'] = this.name;
    data['slug_name'] = this.slugName;
    data['duree_bail'] = this.dureeBail;
    data['debut_bail'] = this.debutBail;
    data['fin_bail'] = this.finBail;
    data['is_renouvellable'] = "${this.isRenouvellable}";
    data['recurence_paiement'] = this.recurencePaiement;
    data['modes_paiement'] = this.modesPaiement;
    data['montant'] = this.montant;
    data['is_tva'] = "${this.isTva}";
    data['taux_tva'] = this.tauxTva;
    data['images'] = this.images;
    data['locataire_id'] = "${this.locataire}";
    data['contrat_bail'] = this.contratBail;
    data['bien_id'] = "${this.bienId}";
    data['etat'] = this.etat;
    return data;
  }

  static const List<String> recurrencePaiementList = [
    "Recurrence de paiement",
    "MENSUEL",
    "BIMESTRIEL",
    "TRIMESTRIEL",
    "QUATRIMESTRIEL",
    "SEMESTRIEL",
    "ANNUEL"
  ];
  static const List<String> modesPaiementList = [
    "Modes de paiement",
    "ESPECE",
    "CARTE DE CREDIT",
    "CHEQUE",
    "VIREMENT",
    "PRELEVEMENT"
  ];
  static const List<String> renouvellableList = [
    "Est-ce renouvelable ?",
    "OUI",
    "NON"
  ];
  static const List<String> tvaList = ["Y'a-t-il une TVA ?", "OUI", "NON"];
  static const List<String> etatList = ["Etat du contrat", "ACTIF", "ROMPU"];
  // static const List<String> locataireList = ["Huberson Kouakou", "Yves Ladde"];
  static const List<String> typeList = [
    "Type de location",
    "HABITATION VIDE",
    "HABITATION MEUBLE",
    "LOCATION SAISONNIERE",
    "COMMERCIAL",
    "PROFESSIONNEL",
    "CIVIL"
  ];
}
