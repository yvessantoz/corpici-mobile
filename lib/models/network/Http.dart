// Analyse la reponse initialse
class HttpResponse {
  String? body;
  int? statuscode;

  HttpResponse({this.body, this.statuscode});
}
