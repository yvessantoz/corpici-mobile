// Analyse le corps de la reponse
class ResponseBody {
  int? status;
  bool? isError;
  String? message;
  String? errorType;
  Map<String, dynamic>? data;

  ResponseBody(
      {this.status, this.isError, this.message, this.errorType, this.data});

  ResponseBody.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    isError = json['is_error'];
    message = json['message'];
    data = json['data'];
    errorType = json['error_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['is_error'] = isError;
    data['message'] = message;
    data['error_type'] = errorType;
    data['data'] = data;
    return data;
  }
}
