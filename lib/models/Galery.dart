// import 'package:get/get.dart';

import 'dart:convert';
import 'dart:developer';

class Galerie {
  String? photo;

  Galerie({this.photo});

  Galerie.fromJson(Map<String, dynamic> json) {
    photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['photo'] = this.photo;
    return data;
  }

  List<Galerie> toList(List<dynamic>? galerie) {
    List<Galerie> galeryList = <Galerie>[];
    if (galerie != null) {
      galeryList.addAll(galerie.map((data) => Galerie.fromJson(data)).toList());
    }
    return galeryList;
  }
}
