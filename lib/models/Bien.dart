import 'package:score_immo/models/Galery.dart';
import 'package:score_immo/models/Immobilier.dart';

class StatistiqueBienImmobilier {
  StatistiqueBienImmobilier(
      {this.totalBien, this.totalLocataire, this.totalLocation});

  int? totalLocataire;
  int? totalBien;
  int? totalLocation;

  StatistiqueBienImmobilier.fromJson(Map<String, dynamic> json) {
    totalLocataire = json['total_locataire'];
    totalBien = json['total_bien'];
    totalLocation = json['total_location'];
  }
}

class BienImmobilierFromData {
  Map<String, dynamic>? bien;
  List<dynamic>? biens;

  BienImmobilierFromData({this.bien, this.biens});

  BienImmobilierFromData.fromJson(Map<String, dynamic> json) {
    bien = json['bien'];
    biens = json['biens'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bien'] = this.bien;
    data['biens'] = this.biens;
    return data;
  }
}

class BienImmobilier {
  int? id;
  String? bienType;
  String? name;
  String? slugName;
  String? adresse;
  String? superficie;
  String? nbPiece;
  String? nbChambre;
  String? nbSalleBain;
  String? description;
  List<String>? images;
  List<dynamic>? galerie;
  List<Galerie>? galerieList;
  int? immobilier;
  String? photo;
  Immobilier? immobilierItem;

  BienImmobilier({
    this.id,
    this.bienType,
    this.name,
    this.slugName,
    this.adresse,
    this.superficie,
    this.nbPiece,
    this.nbChambre,
    this.nbSalleBain,
    this.description,
    this.images,
    this.galerie,
    this.galerieList,
    this.immobilier,
    this.photo,
    this.immobilierItem,
  });

  BienImmobilier.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.bienType = json['bien_type'];
    this.name = json['name'];
    this.slugName = json['slug_name'];
    this.adresse = json['adresse'];
    this.superficie = "${json['superficie']}";
    this.nbPiece = "${json['nb_piece']}";
    this.nbChambre = "${json['nb_chambre']}";
    this.nbSalleBain = "${json['nb_salle_bain']}";
    this.description = json['description'];
    this.images = json['images'];
    this.galerie = json['galerie'];
    this.galerieList = Galerie().toList(json['galerie']);
    this.immobilier = json['immobilier_id'];
    this.photo = json['photo'];
    this.immobilierItem = Immobilier.fromJson(json['immobilier']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this.id;
    data['bien_type'] = this.bienType;
    data['name'] = this.name;
    data['slug_name'] = this.slugName;
    data['adresse'] = this.adresse;
    data['superficie'] = this.superficie;
    data['nb_piece'] = this.nbPiece;
    data['nb_chambre'] = this.nbChambre;
    data['nb_salle_bain'] = this.nbSalleBain;
    data['description'] = this.description;
    data['images'] = this.images;
    data['immobilier_id'] = this.immobilier;
    data['galerie'] = this.galerie;
    data['photo'] = this.photo;
    return data;
  }

  static const List<String> typeList = [
    "Type de bien",
    "APPARTEMENT",
    "ATELIER",
    "BOUTIQUE",
    "BUREAU",
    "CAVE",
    "CHAMBRE",
    "MAISON",
    "STUDIO"
  ];
}
