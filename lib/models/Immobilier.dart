import 'dart:convert';
import 'dart:developer';

import 'package:score_immo/models/Galery.dart';

class StatistiqueImmobilier {
  StatistiqueImmobilier(
      {this.totalBien, this.totalImmobilier, this.totalLocation});

  int? totalImmobilier;
  int? totalBien;
  int? totalLocation;

  StatistiqueImmobilier.fromJson(Map<String, dynamic> json) {
    totalImmobilier = json['total_immobilier'];
    totalBien = json['total_bien'];
    totalLocation = json['total_location'];
  }
}

class Equipement {
  List<dynamic>? listeEquipements;

  Equipement({this.listeEquipements});

  Equipement.fromJson(Map<String, dynamic> json) {
    listeEquipements = json['liste_equipements'];
  }
}

class AcquisitionFromData {
  Map<String, dynamic>? acquisition;
  List<dynamic>? acquisitions;

  AcquisitionFromData({this.acquisition, this.acquisitions});

  AcquisitionFromData.fromJson(Map<String, dynamic> json) {
    acquisition = json['immobilier'];
    acquisitions = json['immobiliers'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['immobilier'] = this.acquisition;
    data['immobiliers'] = this.acquisitions;
    return data;
  }
}

class Immobilier {
  int? id;
  String? immobilierType;
  String? name;
  String? slugName;
  String? adresse;
  String? pays;
  String? region;
  String? ville;
  String? dateConstruction;
  List<dynamic>? equipements;
  String? description;
  List<dynamic>? images;
  List<dynamic>? galerie;
  List<Galerie>? galerieList;
  String? photo;

  Immobilier(
      {this.id,
      this.immobilierType,
      this.name,
      this.slugName,
      this.adresse,
      this.pays,
      this.region,
      this.ville,
      this.dateConstruction,
      this.equipements,
      this.description,
      this.images,
      this.galerie,
      this.galerieList,
      this.photo});

  Immobilier.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.immobilierType = json['immobilier_type'];
    this.name = json['name'];
    this.slugName = json['slug_name'];
    this.adresse = json['adresse'];
    this.pays = json['pays'];
    this.region = json['region'];
    this.ville = json['ville'];
    this.dateConstruction = json['date_construction'];
    this.equipements = json['equipements'];
    this.description = json['description'];
    this.images = json['images'];
    this.galerie = json['galerie'];
    this.galerieList = Galerie().toList(json['galerie']);
    this.photo = json['photo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = this.id;
    data['immobilier_type'] = this.immobilierType;
    data['name'] = this.name;
    data['slug_name'] = this.slugName;
    data['adresse'] = this.adresse;
    data['pays'] = this.pays;
    data['region'] = this.region;
    data['ville'] = this.ville;
    data['date_construction'] = this.dateConstruction;
    data['equipements'] = this.equipements;
    data['description'] = this.description;
    data['images'] = this.images;
    data['galerie'] = this.galerie;
    data['photo'] = this.photo;
    return data;
  }

  static const List<String> equipementsList = [
    "Climatiseur",
    "Ventilateur",
    "Chauffe-eau",
    "Fauteuils",
    "Table à manger"
  ];

  static const List<String> typeList = ["IMMEUBLE", "MAISON", "VILLA", "ETAGE"];
}
