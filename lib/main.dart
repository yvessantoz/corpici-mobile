import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:get/get.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/appController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/routes.dart';
import 'package:score_immo/screens/error_page/error_screen.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/screens/sign_in/sign_in_screen.dart';
import 'package:score_immo/screens/splash/splash_screen.dart';
import 'package:score_immo/theme.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() async {
  // Assure l'initialisation des widgets
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: false // optional: set false to disable printing logs to console
      );
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppSate createState() => _MyAppSate();
}

class _MyAppSate extends State<MyApp> {
  final UserController userController = Get.put(UserController());
  final AppController appController = Get.put(AppController());
  final locale = "fr";
  bool? isLogin; // si l'utilisateur est connecté
  bool? isInstalled; // si l'application est installée
  bool? isLoading = true; // si en cours de chargement
  bool? beConnected;

  @override
  void initState() {
    initializeDateFormatting(locale); // Initialise le timezone
    sessionCheck();
    super.initState();
  }

  //
  sessionCheck() async {
    bool connected = await Request().isConnected();
    bool sessionIsLogin = await userController.isLogin();
    bool isInstalledSave = await appController.isInstalled();
    setState(() {
      isLogin = sessionIsLogin;
      beConnected = connected;
      isInstalled = isInstalledSave;
      if (isLogin != null && isInstalled != null) {
        isLoading = false;
      }
    });
  }

  // Retourne le bon écran de départ
  nextView() {
    if (isInstalled == false) {
      return SplashScreen();
    } else if (isLogin == true) {
      if (beConnected == true) {
        return NavigationBarBottom();
      } else {
        return ErrorScreen();
      }
      // HomeScreen();
    } else if (isLogin == false) {
      return SignInScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: APP_NAME,
      theme: theme(),
      home: isLoading == true ? loaderView() : nextView(),
      //initialRoute: getView(),
      routes: routes,
    );
  }

  Scaffold loaderView() {
    return Scaffold(
      body: Container(
        child: Center(
          child: Image.asset(APP_LOGO),
        ),
      ),
    );
  }
}
