import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/size_config.dart';

showBottomSheets(BuildContext context, item, arguments, detailScreen,
    String message, String title, controller) {
  Get.bottomSheet(
    Container(
      padding: EdgeInsets.only(top: 4),
      height: SizeConfig.screenHeight * 0.28,
      width: SizeConfig.screenWidth,
      color: Colors.white,
      child: Column(children: [
        Container(
          height: 6,
          width: 120,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Get.isDarkMode ? Colors.grey[600] : Colors.grey[300]),
        ),
        Spacer(),
        _buildBottomSheetButton(
            label: "Afficher",
            onTap: () {
              Get.to(detailScreen, arguments: arguments);
            },
            clr: kPrimaryColor),
        _buildBottomSheetButton(
            label: "Supprimer",
            onTap: () {
              SoftDialog(btnCancelText: "NON", btnOkOnPress: "OUI")
                  .warningDialog(context, message, title, () {
                controller.delete(item);
                controller.initAll();
                Get.back(closeOverlays: true);
              }, () {
                Get.back(closeOverlays: true);
              });
            },
            clr: Colors.red[300]!),
        SizedBox(
          height: 10,
        ),
        _buildBottomSheetButton(
            clr: Colors.black26,
            label: "Fermer",
            onTap: () {
              Get.back();
            },
            isClose: true),
        SizedBox(
          height: 20,
        ),
      ]),
    ),
  );
}

_buildBottomSheetButton(
    {required String label,
    required VoidCallback onTap,
    required Color clr,
    bool isClose = false}) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      margin: EdgeInsets.symmetric(vertical: 4),
      height: 38,
      width: SizeConfig.screenWidth * 0.9,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: clr,
        ),
        borderRadius: BorderRadius.circular(20),
        color: isClose ? Colors.transparent : clr,
      ),
      child: Center(
          child: Text(
        label,
        style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
            color: isClose ? Colors.black : Colors.white),
      )),
    ),
  );
}
