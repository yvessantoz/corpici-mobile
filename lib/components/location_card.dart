import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/screens/details_location/details_screen.dart';

import '../constants.dart';
import '../size_config.dart';

class LocationCard extends StatelessWidget {
  LocationCard({
    Key? key,
    this.width = 140,
    this.aspectRetio = 1.02,
    required this.location,
  }) : super(key: key);

  final double width, aspectRetio;
  final Location location;
  final LocationController locationController = Get.put(LocationController());
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () => Get.to(
            DetailsScreen(),
            arguments: LocationDetailsArguments(location: location),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsets.all(
                      getProportionateScreenWidth(10)), //EdgeInsets.all(0.0)
                  decoration: BoxDecoration(
                    color: kSecondaryColor
                        .withOpacity(0.1), //Colors.white.withOpacity(0.1)
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: 'homeloc' +
                        location.id.toString() +
                        '' +
                        location.name!,
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/spinner.gif",
                      image:
                          "${Request.IMAGE_LOAD_BASE + location.bien!.photo!}",
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                location.name!,
                style: TextStyle(color: Colors.black),
                maxLines: 2,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 120,
                    child: Text(
                      "${location.montant} Fcfa",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.w600,
                          color: kPrimaryColor,
                          overflow: TextOverflow.ellipsis),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
