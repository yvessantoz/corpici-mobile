import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/size_config.dart';

class ReportingItemCard extends StatelessWidget {
  final IconData icon;
  final String title;
  final int total;
  final GestureTapCallback press;

  ReportingItemCard(
      {required this.title,
      required this.icon,
      required this.total,
      required this.press});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        flex: 1,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(10),
              height: getProportionateScreenWidth(75),
              width: getProportionateScreenWidth(73),
              decoration: BoxDecoration(
                color: kBlueColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Column(
                  children: [
                    Icon(icon, size: 25, color: Colors.white),
                    SizedBox(
                      height: 3,
                    ),
                    Text(
                      '$total',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 17),
                    )
                  ],
                ),
              ),
            ),
            Center(child: Text(title))
          ],
        ));
  }
}
