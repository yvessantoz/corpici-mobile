import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';

class SoftDialog {
  String? btnCancelText;
  String? btnOkOnPress;

  SoftDialog({this.btnOkOnPress, this.btnCancelText});

  AwesomeDialog successDialog(BuildContext context, String message,
      String title, Function confirm, Function cancel) {
    return AwesomeDialog(
        context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.RIGHSLIDE,
        title: title,
        desc: message,
        btnCancelOnPress: cancel,
        btnOkOnPress: confirm,
        btnOkText: btnOkOnPress ?? "Continuer",
        btnCancelText: btnCancelText ?? "Annuler",
        dismissOnTouchOutside: false,
        headerAnimationLoop: false)
      ..show();
  }

  AwesomeDialog dangerDialog(BuildContext context, String message, String title,
      Function confirm, Function cancel) {
    return AwesomeDialog(
        context: context,
        dialogType: DialogType.ERROR,
        animType: AnimType.BOTTOMSLIDE,
        title: title,
        desc: message,
        btnCancelOnPress: cancel,
        btnOkOnPress: confirm,
        btnOkText: btnOkOnPress ?? "Continuer",
        btnCancelText: btnCancelText ?? "Annuler",
        dismissOnTouchOutside: false,
        headerAnimationLoop: false)
      ..show();
  }

  AwesomeDialog infoDialog(BuildContext context, String message, String title,
      Function confirm, Function cancel) {
    return AwesomeDialog(
      context: context,
      dialogType: DialogType.INFO,
      animType: AnimType.LEFTSLIDE,
      title: title,
      desc: message,
      btnCancelOnPress: cancel,
      btnOkOnPress: confirm,
      btnOkText: btnOkOnPress ?? "Continuer",
      btnCancelText: btnCancelText ?? "Annuler",
      dismissOnTouchOutside: false,
    )..show();
  }

  AwesomeDialog warningDialog(BuildContext context, String message,
      String title, Function confirm, Function cancel) {
    return AwesomeDialog(
      context: context,
      dialogType: DialogType.WARNING,
      animType: AnimType.BOTTOMSLIDE,
      title: title,
      desc: message,
      btnCancelOnPress: cancel,
      btnOkOnPress: confirm,
      btnOkText: btnOkOnPress ?? "Continuer",
      btnCancelText: btnCancelText ?? "Annuler",
      dismissOnTouchOutside: false,
    )..show();
  }
}
