import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/screens/details_immobilier/details_screen.dart';

import '../constants.dart';
import '../size_config.dart';

class ImmobilierCard extends StatelessWidget {
  ImmobilierCard({
    Key? key,
    this.width = 140,
    this.aspectRetio = 1.02,
    required this.immobilier,
  }) : super(key: key);

  final double width, aspectRetio;
  final Immobilier immobilier;
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () => Get.to(
            DetailsScreen(),
            arguments: ImmobilerDetailsArguments(immobilier: immobilier),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(20)),
                  decoration: BoxDecoration(
                    color: kSecondaryColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: "homeimmo-${immobilier.id.toString()}",
                    child: FadeInImage.assetNetwork(
                        placeholder: "assets/images/spinner.gif",
                        image: Request.IMAGE_LOAD_BASE + immobilier.photo!),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                immobilier.name!,
                style: TextStyle(color: Colors.black),
                maxLines: 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
