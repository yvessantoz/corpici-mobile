import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';
import 'package:score_immo/screens/list_immobilier/list_immobilier_screen.dart';
import 'package:score_immo/screens/list_locataire/list_locataire_screen.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';
import 'package:score_immo/screens/profile/profile_screen.dart';

import '../constants.dart';
import '../enums.dart';

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key? key,
    required this.selectedMenu,
  }) : super(key: key);

  final MenuState selectedMenu;

  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = Color(0xFFB6B6B6);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 14),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40),
          topRight: Radius.circular(40),
        ),
      ),
      child: SafeArea(
          top: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                  icon: Icon(
                    Icons.home_outlined,
                    color: MenuState.home == selectedMenu
                        ? kPrimaryColor
                        : inActiveIconColor,
                    size: 30,
                  ),
                  onPressed: () => Get.to(() => HomeScreen())),
              IconButton(
                icon: Icon(
                  Icons.location_city_outlined,
                  size: 30,
                  color: MenuState.immobilier == selectedMenu
                      ? kPrimaryColor
                      : inActiveIconColor,
                ),
                onPressed: () {
                  Get.to(ListImmobilierScreen());
                },
              ),
              IconButton(
                icon: Icon(
                  Icons.home_work_outlined,
                  size: 30,
                  color: MenuState.bienImmobilier == selectedMenu
                      ? kPrimaryColor
                      : inActiveIconColor,
                ),
                onPressed: () {
                  Get.to(ListBienImmobilierScreen());
                },
              ),
              IconButton(
                icon: Icon(
                  Icons.local_hotel_outlined,
                  size: 30,
                  color: MenuState.location == selectedMenu
                      ? kPrimaryColor
                      : inActiveIconColor,
                ),
                onPressed: () => Get.to(() => LocationScreen()),
              ),
              IconButton(
                icon: Icon(
                  Icons.transfer_within_a_station_outlined,
                  size: 30,
                  color: MenuState.locataire == selectedMenu
                      ? kPrimaryColor
                      : inActiveIconColor,
                ),
                onPressed: () => Get.to(ListLocataireScreen()),
              ),
              IconButton(
                icon: Icon(
                  Icons.person_outline_outlined,
                  size: 30,
                  color: MenuState.profile == selectedMenu
                      ? kPrimaryColor
                      : inActiveIconColor,
                ),
                onPressed: () => Get.to(() => ProfileScreen()),
              ),
            ],
          )),
    );
  }
}
