import 'package:flutter/material.dart';
import 'package:score_immo/components/default_button.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ListBottomCard extends StatelessWidget {
  const ListBottomCard(
      {Key? key,
      this.linkTitle,
      required this.total,
      required this.buttonPress,
      required this.buttonText,
      this.linkPress,
      required this.icon})
      : super(key: key);

  final String? linkTitle;
  final int total;
  final String buttonText;
  final GestureTapCallback buttonPress;
  final GestureTapCallback? linkPress;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: getProportionateScreenWidth(15),
        horizontal: getProportionateScreenWidth(30),
      ),
      // height: 174,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          )
        ],
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  height: getProportionateScreenWidth(40),
                  width: getProportionateScreenWidth(40),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Icon(
                    icon,
                    color: kBlueColor,
                  ),
                ),
                Spacer(),
                linkTitle != null
                    ? GestureDetector(
                        child: Text(linkTitle ?? ''), onTap: linkPress)
                    : SizedBox(
                        height: 0,
                        width: 0,
                      ),
                const SizedBox(width: 10),
                linkPress != null
                    ? Icon(
                        Icons.arrow_forward_ios,
                        size: 12,
                        color: kTextColor,
                      )
                    : SizedBox(
                        height: 0,
                        width: 0,
                      )
              ],
            ),
            SizedBox(height: getProportionateScreenHeight(20)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text.rich(
                  TextSpan(
                    text: "Total:\n",
                    children: [
                      TextSpan(
                        text: total < 10 ? "0$total" : "$total",
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(190),
                  child: DefaultButton(
                    text: buttonText,
                    press: buttonPress,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
