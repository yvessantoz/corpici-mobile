import 'dart:io';

import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';

import '../size_config.dart';

class PDFCard extends StatelessWidget {
  const PDFCard({
    Key? key,
    this.width = 100,
    this.aspectRetio = 1.02,
    required this.image,
  }) : super(key: key);

  final double width, aspectRetio;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () => {},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(
                      10)), //getProportionateScreenWidth(20)
                  decoration: BoxDecoration(
                    color: kSecondaryColor
                        .withOpacity(0.1), //kSecondaryColor.withOpacity(0.1)
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: image,
                    child: Image.asset(image),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
