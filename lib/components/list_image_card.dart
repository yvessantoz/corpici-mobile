import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/constants.dart';

import '../size_config.dart';

class ListImageCard extends StatelessWidget {
  const ListImageCard({
    Key? key,
    this.width = 100,
    this.aspectRetio = 1.02,
    required this.image,
  }) : super(key: key);

  final double width, aspectRetio;
  final XFile image;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () => {},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(
                      20)), //getProportionateScreenWidth(20)
                  decoration: BoxDecoration(
                    color: kSecondaryColor
                        .withOpacity(0.1), //kSecondaryColor.withOpacity(0.1)
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: image.path,
                    child: Image.file(File(image.path)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
