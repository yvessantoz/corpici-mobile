import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/screens/details_bien_immobilier/details_screen.dart';

import '../constants.dart';
import '../size_config.dart';

class BienImmobilierCard extends StatelessWidget {
  BienImmobilierCard({
    Key? key,
    this.width = 140, //140
    this.aspectRetio = 1.02,
    required this.bienImmobilier,
  }) : super(key: key);

  final double width, aspectRetio;
  final BienImmobilier bienImmobilier;
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () => Get.to(DetailsScreen(),
              arguments: BienImmobilerDetailsArguments(
                  bienImmobilier: bienImmobilier)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.02,
                child: Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(
                      10)), //getProportionateScreenWidth(20) ou EdgeInsets.all(0.0)
                  decoration: BoxDecoration(
                    color: kSecondaryColor
                        .withOpacity(0.1), // Colors.white.withOpacity(0.1)
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: "homebien-${bienImmobilier.id.toString()}",
                    child: FadeInImage.assetNetwork(
                      placeholder: "assets/images/spinner.gif",
                      image: Request.IMAGE_LOAD_BASE + bienImmobilier.photo!,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                bienImmobilier.name!,
                style: TextStyle(
                  color: Colors.black,
                ),
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 120,
                    child: Text(
                      "${bienImmobilier.immobilierItem!.name}",
                      style: TextStyle(
                          fontSize: getProportionateScreenWidth(13),
                          fontWeight: FontWeight.w600,
                          color: kPrimaryColor,
                          overflow: TextOverflow.ellipsis),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
