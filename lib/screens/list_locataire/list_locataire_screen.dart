import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:score_immo/components/coustom_bottom_nav_bar.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/list_bottom_card.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/enums.dart';
import 'package:score_immo/screens/creer_locataire/locataire_screen.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';
import 'package:score_immo/screens/list_tous_locataires/list_tous_locataires_screen.dart';

import 'components/body.dart';

class ListLocataireScreen extends StatelessWidget {
  UserController userController = Get.put(UserController());

  //
  // fetchUsers() {
  //   userController.fetchUserWhere();
  // }

  static String routeName = "/list_locataire";

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  //
  initMethods() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await userController.initAll();
    } else {
      SoftDialog().dangerDialog(Get.context!,
          "Désolé vous n'avez pas accès à internet", "Erreur", () {}, () {});
    }
  }

  var myMenuItems = <String>[
    'Afficher tous les locataires',
  ];

  void onSelect(item) {
    switch (item) {
      case 'Afficher tous les locataires':
        Get.to(() => ListTousLocataireScreen());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: Body(),
      ),
      // bottomNavigationBar:
      //     CustomBottomNavBar(selectedMenu: MenuState.locataire),
      // bottomNavigationBar: ListBottomCard(
      //     linkTitle: "Afficher les locations",
      //     buttonPress: () {
      //       Get.to(CreerLocataireScreen());
      //     },
      //     buttonText: "Ajouter",
      //     linkPress: () {
      //       Get.to(LocationScreen());
      //     },
      //     total: userController.userList.length,
      //     icon: Icons.location_city_outlined),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      actions: [
        GestureDetector(
          onTap: () {
            Get.to(CreerLocataireScreen());
          },
          child: Icon(
            Icons.person_add,
            size: 30,
          ),
        ),
        PopupMenuButton<String>(
            onSelected: onSelect,
            itemBuilder: (BuildContext context) {
              return myMenuItems.map((String choice) {
                return PopupMenuItem<String>(
                  child: Text(choice),
                  value: choice,
                );
              }).toList();
            })
      ],
      title: Column(
        children: [
          Text(
            "Mes Locataires",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "${userController.userList.length} éléments",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}
