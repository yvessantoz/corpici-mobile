import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:score_immo/constants.dart';

class FilterList extends StatefulWidget {
  final Function(List<String>) onSelect;

  const FilterList({Key? key, required this.onSelect}) : super(key: key);

  @override
  _FilterListState createState() => _FilterListState();
}

class _FilterListState extends State<FilterList> {
  List<String> selected = [];
  List<dynamic> options = [
    {
      'icon': SvgPicture.asset(
          "assets/icons/Call.svg"), //SvgPicture.asset("assets/icons/tag.svg"),
      'title': 'Discount',
    },
    {
      'icon': SvgPicture.asset("assets/icons/Cart Icon.svg"),
      'title': 'Free Delivery'
    },
    {
      'icon': SvgPicture.asset("assets/icons/Cash.svg"),
      'title': 'Installment Plan'
    },
  ];

  toggle(title) {
    if (selected.contains(title))
      selected.remove(title);
    else
      selected.add(title);

    setState(() {
      widget.onSelect(selected);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: options.map((o) {
        return FilterListItem(
          icon: o['icon'],
          title: o['title'],
          selected: this.selected.contains(o['title']),
          onTap: () {
            toggle(o['title']);
          },
        );
      }).toList(),
    );
  }
}

class FilterListItem extends StatelessWidget {
  final Widget icon;
  final String title;
  final bool selected;
  final Function() onTap;

  const FilterListItem({
    Key? key,
    required this.icon,
    required this.title,
    required this.onTap,
    this.selected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.black12,
              width: 1,
            ),
          ),
        ),
        child: Row(
          children: [
            icon,
            Expanded(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  title,
                  style: TextStyle(fontSize: 16),
                ),
              ),
            ),
            if (selected)
              Icon(
                Icons.check,
                color: kPrimaryColor,
              )
          ],
        ),
      ),
    );
  }
}
