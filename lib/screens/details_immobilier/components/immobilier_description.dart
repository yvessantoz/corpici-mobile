import 'package:flutter/material.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/screens/details_immobilier/components/bottom_sheet.dart';

import '../../../size_config.dart';

class ImmobilierDescription extends StatelessWidget {
  const ImmobilierDescription({
    Key? key,
    required this.immobilier,
    this.pressOnSeeMore,
  }) : super(key: key);

  final Immobilier immobilier;
  final GestureTapCallback? pressOnSeeMore;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "${immobilier.name}",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.location_on_outlined,
                    size: 18,
                    color: Colors.black45,
                  ),
                  Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        "${immobilier.pays}, ${immobilier.ville}, ${immobilier.adresse}",
                        maxLines: 3,
                      )),
                ],
              ),
            )),
        SizedBox(
          height: 8,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.location_city_outlined,
                  size: 18,
                  color: Colors.black45,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    "${immobilier.immobilierType}",
                    maxLines: 3,
                  ),
                ),
              ],
            )),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Equipements",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(34),
          ),
          child: Text("${immobilier.equipements!.join(", ")}"),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Date de construction",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  "${immobilier.dateConstruction}",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Description",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            "${immobilier.description}",
          ),
        ),
      ],
    );
  }
}

class BottomSheets {
  // Affiche le modal des statistiques
  void settingModalBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        context: context,
        builder: (BuildContext bc) {
          return DetailsModalBottomSheet();
        });
  }
}
