import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/rounded_icon_btn.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';

import '../../../size_config.dart';

class ActionButtons extends StatelessWidget {
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  ActionButtons({
    Key? key,
    required this.immobilier,
  }) : super(key: key);

  final Immobilier immobilier;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.home_work_outlined,
              press: () {
                Get.to(ListBienImmobilierScreen(), arguments: immobilier);
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              iconColor: Colors.red,
              icon: Icons.delete_outline_outlined,
              press: () {
                SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON")
                    .warningDialog(
                        context,
                        "Voulez-vous supprimer cette acquision ?",
                        "Attention", () {
                  immobilierController.delete(immobilier);
                }, () {});
              },
            ),
          ],
        ),
      ),
    );
  }
}
