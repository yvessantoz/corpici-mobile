import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Immobilier.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ImmobilierImages extends StatefulWidget {
  const ImmobilierImages({
    Key? key,
    required this.immobilier,
  }) : super(key: key);

  final Immobilier immobilier;

  @override
  _ImmobilierImagesState createState() => _ImmobilierImagesState();
}

class _ImmobilierImagesState extends State<ImmobilierImages> {
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());
  int selectedImage = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(238),
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: 'details-immo' + widget.immobilier.id.toString(),
              child: FadeInImage.assetNetwork(
                  placeholder: "assets/images/spinner.gif",
                  image: Request.IMAGE_LOAD_BASE +
                      widget.immobilier.galerieList![selectedImage]
                          .photo!), //.widget.immobilier.galerie![selectedImage]
            ),
          ),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ...List.generate(widget.immobilier.galerieList!.length,
                    (index) => buildSmallProductPreview(index)),
              ],
            ))
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: EdgeInsets.only(right: 15),
        padding: EdgeInsets.all(8),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: FadeInImage.assetNetwork(
            placeholder: "assets/images/spinner.gif",
            image:
                "${Request.IMAGE_LOAD_BASE + widget.immobilier.galerieList![index].photo!}"),
        key: Key("gal-immo${widget.immobilier.galerieList![index].photo!}"),
      ),
    );
  }
}
