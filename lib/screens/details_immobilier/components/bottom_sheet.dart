import 'package:flutter/material.dart';
import 'package:score_immo/components/reporting_item.dart';

class DetailsModalBottomSheet extends StatelessWidget {
  void close(context) {
    if (Navigator.canPop(context)) {
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(28.0),
        child: Wrap(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: 100,
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    child: Icon(Icons.close),
                    onTap: () {
                      close(context);
                    },
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 20),
              child: Text(
                'Statistiques',
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      ReportingItemCard(
                          title: "Immobilier",
                          icon: Icons.location_city_outlined,
                          total: 123,
                          press: () {}),
                      SizedBox(
                        width: 20,
                      ),
                      ReportingItemCard(
                        title: "Bien Immobilier",
                        icon: Icons.home_work_outlined,
                        total: 123,
                        press: () {},
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      ReportingItemCard(
                          title: "Location",
                          icon: Icons.local_hotel_outlined,
                          total: 123,
                          press: () {}),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
