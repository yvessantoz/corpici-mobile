import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/screens/details_immobilier/components/immobilier_description.dart';
import 'package:score_immo/screens/editer_immobilier/editer_immobilier_screen.dart';
import 'package:score_immo/size_config.dart';

import 'action_buttons.dart';
import 'top_rounded_container.dart';
import 'immobilier_images.dart';

class Body extends StatelessWidget {
  final Immobilier immobiler;

  const Body({Key? key, required this.immobiler}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ImmobilierImages(immobilier: this.immobiler),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ImmobilierDescription(
                immobilier: this.immobiler,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ActionButtons(immobilier: this.immobiler),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.screenWidth * 0.15,
                          right: SizeConfig.screenWidth * 0.15,
                          bottom: getProportionateScreenWidth(40),
                          top: getProportionateScreenWidth(15),
                        ),
                        child: DefaultButton(
                          text: "Editer",
                          press: () {
                            Get.to(EditerImmobilierScreen(),
                                arguments: this.immobiler);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
