import 'package:flutter/material.dart';
import 'package:score_immo/models/Immobilier.dart';

import 'components/body.dart';
import 'components/custom_app_bar.dart';

class DetailsScreen extends StatelessWidget {
  static String routeName = "/details";

  @override
  Widget build(BuildContext context) {
    final ImmobilerDetailsArguments agrs =
        ModalRoute.of(context)!.settings.arguments as ImmobilerDetailsArguments;
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: CustomAppBar(),
      ),
      body: Body(immobiler: agrs.immobilier),
    );
  }
}

class ImmobilerDetailsArguments {
  final Immobilier immobilier;

  ImmobilerDetailsArguments({required this.immobilier});
}
