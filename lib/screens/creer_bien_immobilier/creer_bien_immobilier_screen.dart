import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/controller/pictureController.dart';

import 'components/body.dart';

class CreerBienImmobilierScreen extends StatefulWidget {
  @override
  _CreerBienImmobilierScreenState createState() =>
      _CreerBienImmobilierScreenState();
}

class _CreerBienImmobilierScreenState extends State<CreerBienImmobilierScreen> {
  static String routeName = "/enregistrer_bien_immobilier";
  final PictureController pictureController = Get.put(PictureController());
  var isDialOpen = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        }
        return true;
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('Enregistrer un bien immobilier'),
          ),
          body: Body(pictureController: pictureController),
          floatingActionButton: SpeedDial(
            icon: Icons.attach_file_outlined,
            openCloseDial: isDialOpen,
            backgroundColor: Colors.green,
            overlayColor: Colors.transparent,
            overlayOpacity: 0.1,
            spacing: 15,
            spaceBetweenChildren: 15,
            closeManually: true,
            children: [
              SpeedDialChild(
                  child: Icon(
                    Icons.image_outlined,
                    color: Colors.white,
                  ),
                  label: 'Galerie',
                  backgroundColor: Colors.blue,
                  onTap: () {
                    pictureController.takeMultiplePictures();
                    print('Galerie');
                  }),
              SpeedDialChild(
                  child: Icon(
                    Icons.preview_outlined,
                    color: Colors.white,
                  ),
                  label: 'Photo',
                  backgroundColor: Colors.pinkAccent,
                  onTap: () {
                    pictureController.takePicture(ImageSource.gallery);
                    print('Photo');
                  }),
            ],
          )),
    );
  }
}
