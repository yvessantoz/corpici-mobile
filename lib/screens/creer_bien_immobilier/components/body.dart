import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/size_config.dart';

import 'creer_bien_immobilier_form.dart';

class Body extends StatelessWidget {
  Body({this.pictureController});

  final PictureController? pictureController;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text("Ajouter un bien", style: headingStyle),
                Text(
                  "Veuillez complétez les champs ci-dessous afin d'enregistrer votre bien immobilier.",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                CreerBienImmobilierForm(pictureController: pictureController),
                SizedBox(height: getProportionateScreenHeight(30)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
