import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/complete_profile/complete_profile_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormState>();
  String? email;
  String? pseudo;
  String? password;
  String? phoneNumber;
  String? conformPassword;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildPseudoFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPhoneNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildConformPassFormField(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: "Suivant",
            press: () {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                nextForm();
              }
            },
          ),
        ],
      ),
    );
  }

  // Confirmation de mot de passe
  buildConformPassFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        obscureText: true,
        onSaved: (newValue) => conformPassword = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPassNullError);
          } else if (value.isNotEmpty && password == conformPassword) {
            removeError(error: kMatchPassError);
          }
          conformPassword = value;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPassNullError);
            return "";
          } else if ((password != value)) {
            addError(error: kMatchPassError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Confirmer mot de passe",
            hintText: "Répéter mot de passe",
            // If  you are using latest version of flutter then lable text and hint text shown like this
            // if you r using flutter less then 1.20.* then maybe this is not working properly
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .lock_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
            ),
      ),
    );
  }

  // Mot de passe
  buildPasswordFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        obscureText: true,
        onSaved: (newValue) => password = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPassNullError);
          } else if (value.length >= 8) {
            removeError(error: kShortPassError);
          }
          password = value;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPassNullError);
            return "";
          } else if (value.length < 8) {
            addError(error: kShortPassError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Mot de passe",
            hintText: "Entrez mot de passe",
            // If  you are using latest version of flutter then lable text and hint text shown like this
            // if you r using flutter less then 1.20.* then maybe this is not working properly
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .lock_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
            ),
      ),
    );
  }

  // Email
  buildEmailFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        onSaved: (newValue) => email = newValue,
        onChanged: (value) {
          // if (value.isNotEmpty) {
          //   removeError(error: kEmailNullError);
          // } else
          if (emailValidatorRegExp.hasMatch(value)) {
            removeError(error: kInvalidEmailError);
          }
          return null;
        },
        validator: (value) {
          // if (value!.isEmpty) {
          //   addError(error: kEmailNullError);
          //   return "";
          // } else

          if (value!.isNotEmpty && !emailValidatorRegExp.hasMatch(value)) {
            addError(error: kInvalidEmailError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Email",
            hintText: "Adresse email",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .email_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
            ),
      ),
    );
  }

  // Nom d'utilisateur
  buildPseudoFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.text,
        onSaved: (newValue) => pseudo = newValue,
        onChanged: (value) {
          if (value.isNotEmpty && pseudoValidatorRegExp.hasMatch(value)) {
            removeError(error: kInvalidPseudoError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isNotEmpty && !pseudoValidatorRegExp.hasMatch(value)) {
            addError(error: kInvalidPseudoError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Nom d'utilisateur",
          hintText: "Nom d'utilisateur",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_outlined),
        ),
      ),
    );
  }

  // Numero de téléphone
  buildPhoneNumberFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.phone,
        onSaved: (newValue) => phoneNumber = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPhoneNumberNullError);
          } else if (value.isNumericOnly) {
            removeError(error: kPhoneNumericError);
          } else if (value.isNumericOnly && value.length >= 10) {
            removeError(error: kPhoneMinChar);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPhoneNumberNullError);
            return "";
          } else if (!value.isNumericOnly) {
            addError(error: kPhoneNumericError);
            return "";
          } else if (value.isNumericOnly && value.length < 10) {
            addError(error: kPhoneMinChar);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Numéro de téléphone",
          hintText: "Numéro de téléphone",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.phone_android_outlined),
        ),
      ),
    );
  }

  // Enregistre les données en session et passe à la page suivante
  nextForm() {
    // Instancie un nouvel object
    User user = User();
    user.email = email!.isEmpty ? null : email;
    user.password = password;
    user.passwordConfirmation = conformPassword;
    user.pseudo = pseudo!.isEmpty ? null : pseudo;
    user.mobile = "$phoneNumber";

    // Enregistrement en session
    userController.setPreferences(false, user, "register");

    // Redirection à la page suivante
    Get.to(() => CompleteProfileScreen());
  }
}
