import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/screens/reset_password/reset_password_screen.dart';
import 'package:score_immo/screens/show_profile/show_profile_screen.dart';

import 'profile_menu.dart';
import 'profile_pic.dart';

class Body extends StatelessWidget {
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePic(),
          SizedBox(height: 20),
          ProfileMenu(
            text: "Mon compte",
            icon: "assets/icons/User Icon.svg",
            press: () {
              Get.to(ShowProfileScreen());
            },
          ),
          ProfileMenu(
            text: "Changer de mot de passe",
            icon: "assets/icons/Lock.svg",
            press: () {
              Get.to(ResetPasswordScreen());
            },
          ),
          ProfileMenu(
            text: "Paramètres",
            icon: "assets/icons/Settings.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Centre d'aide",
            icon: "assets/icons/Question mark.svg",
            press: () {},
          ),
          ProfileMenu(
            text: "Déconnexion",
            icon: "assets/icons/Log out.svg",
            press: () {
              userController.logOut();
            },
          ),
        ],
      ),
    );
  }
}
