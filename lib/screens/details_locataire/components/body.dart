import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/details_locataire/components/locataire_images.dart';
import 'package:score_immo/screens/editer_locataire/editer_locataire_screen.dart';
import 'package:score_immo/size_config.dart';

import 'action_buttons.dart';
import 'locataire_description.dart';
import 'top_rounded_container.dart';

class Body extends StatelessWidget {
  final User locataire;
  final bool? all;

  const Body({Key? key, required this.locataire, this.all}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        LocataireImages(locataire: this.locataire),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              LocataireDescription(
                locataire: this.locataire,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    all == null
                        ? ActionButtons(locataire: this.locataire)
                        : Container(),
                    all == null
                        ? TopRoundedContainer(
                            color: Colors.white,
                            child: Padding(
                              padding: EdgeInsets.only(
                                left: SizeConfig.screenWidth * 0.15,
                                right: SizeConfig.screenWidth * 0.15,
                                bottom: getProportionateScreenWidth(40),
                                top: getProportionateScreenWidth(15),
                              ),
                              child: DefaultButton(
                                text: "Editer",
                                press: () {
                                  Get.to(EditerLocataireScreen(),
                                      arguments: locataire);
                                },
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
