import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/rounded_icon_btn.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/User.dart';

import '../../../size_config.dart';

class ActionButtons extends StatelessWidget {
  ActionButtons({Key? key, required this.locataire}) : super(key: key);

  final User locataire;
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedIconBtn(
              showShadow: true,
              iconColor: Colors.red,
              icon: Icons.delete_outline_outlined,
              press: () {
                SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON")
                    .warningDialog(
                        context,
                        "Voulez-vous supprimer ce locataire ?",
                        "Attention", () {
                  userController.delete(locataire);
                }, () {});
              },
            ),
            // SizedBox(width: getProportionateScreenWidth(20)),
            // RoundedIconBtn(
            //   showShadow: true,
            //   icon: Icons.remove,
            //   press: () {
            //     SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON")
            //         .warningDialog(
            //             context,
            //             "Voulez-vous rompre le contrat de \nce locataire ?",
            //             "Attention",
            //             () {},
            //             () {});
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
