import 'package:flutter/material.dart';
import 'package:score_immo/models/User.dart';

import '../../../size_config.dart';

class LocataireDescription extends StatelessWidget {
  const LocataireDescription({
    Key? key,
    required this.locataire,
    this.pressOnSeeMore,
  }) : super(key: key);

  final User locataire;
  final GestureTapCallback? pressOnSeeMore;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "${locataire.lastName!} ${locataire.firstName!}",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.person_outline_outlined,
                  size: 18,
                  color: Colors.black45,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    "${locataire.userType}",
                    maxLines: 3,
                  ),
                ),
              ],
            )),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Téléphone",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.phone_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  locataire.mobile!,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Email",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.email_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  locataire.email!,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Date et lieu de naissance",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  "${locataire.dateNaissance} à ${locataire.lieuNaissance}",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Type de pièce",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            locataire.typePiece!,
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Date d'expiration de la pièce",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  "${locataire.expirationPiece}",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Numéro ${locataire.typePiece}",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            locataire.numeroPiece!,
          ),
        ),
      ],
    );
  }
}
