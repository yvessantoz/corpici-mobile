import 'package:flutter/material.dart';
import 'package:score_immo/models/User.dart';

import 'components/body.dart';
import 'components/custom_app_bar.dart';

class DetailsScreen extends StatelessWidget {
  static String routeName = "/details";

  @override
  Widget build(BuildContext context) {
    final LocataireDetailsArguments agrs =
        ModalRoute.of(context)!.settings.arguments as LocataireDetailsArguments;
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: CustomAppBar(),
      ),
      body: Body(locataire: agrs.locataire, all: agrs.all),
    );
  }
}

class LocataireDetailsArguments {
  final User locataire;
  final bool? all;

  LocataireDetailsArguments({required this.locataire, this.all});
}
