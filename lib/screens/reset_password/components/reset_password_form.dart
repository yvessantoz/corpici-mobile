import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/helper/keyboard.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/show_profile/show_profile_screen.dart';

import '../../../components/default_button.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class ResetPasswordForm extends StatefulWidget {
  @override
  _ResetPasswordFormState createState() => _ResetPasswordFormState();
}

class _ResetPasswordFormState extends State<ResetPasswordForm> {
  UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormState>();
  String? oldPassword;
  String? newPassword;
  String? repeatNewPassword;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildOldPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildNewPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildRepeatNewPasswordFormField(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(20)),
          DefaultButton(
            text: "Editer",
            press: () {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                KeyboardUtil.hideKeyboard(context);
                sendDataToServer();
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildOldPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => oldPassword = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Mot de passe actuel",
          hintText: "Mot de passe actuel",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .lock_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
          ),
    );
  }

  TextFormField buildNewPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => newPassword = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Nouveau mot de passe",
          hintText: "Nouveau mot de passe",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .lock_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
          ),
    );
  }

  TextFormField buildRepeatNewPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => repeatNewPassword = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Répéter mot de passe",
          hintText: "Répéter mot de passe",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .lock_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
          ),
    );
  }

  // Envoi des données au serveur
  sendDataToServer() async {
    User sessionUser = await userController.getUser("user");
    sessionUser.password = newPassword;
    Map<dynamic, dynamic> result = await userController.updateUser(sessionUser);
    if (result['status']) {
      // Stoppe le loader
      Get.off(() => ShowProfileScreen());
    } else {
      // Stoppe le loader
    }
  }
}
