import 'package:flutter/material.dart';
import '../../../size_config.dart';
import 'reset_password_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                // Center(
                //     child: Image.asset(
                //   APP_LOGO,
                //   width: 230,
                //   height: 230,
                // )),
                Text(
                  "Editer mot de passe",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: getProportionateScreenWidth(28),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  "Renseignez les champs ci-dessous afin d'éditer \nvotre mot de passe.",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                ResetPasswordForm(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
