import 'package:flutter/material.dart';
import 'package:score_immo/models/Location.dart';

import 'components/body.dart';
import 'components/custom_app_bar.dart';

class DetailsScreen extends StatelessWidget {
  static String routeName = "/details_location";

  @override
  Widget build(BuildContext context) {
    final LocationDetailsArguments agrs =
        ModalRoute.of(context)!.settings.arguments as LocationDetailsArguments;
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: CustomAppBar(),
      ),
      body: Body(location: agrs.location),
    );
  }
}

class LocationDetailsArguments {
  final Location location;

  LocationDetailsArguments({required this.location});
}
