import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/Location.dart';

import '../../../size_config.dart';

class LocationDescription extends StatelessWidget {
  LocationDescription({
    Key? key,
    required this.location,
    this.pressOnSeeMore,
  }) : super(key: key);

  final Location location;
  final GestureTapCallback? pressOnSeeMore;
  final UserController userController = Get.put(UserController());
  final LocationController locationController = Get.put(LocationController());

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            location.name!,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.location_city_outlined,
                  size: 18,
                  color: Colors.black45,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    "${location.locationType}",
                    maxLines: 3,
                  ),
                ),
              ],
            )),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Locataire",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.person_outline_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                    "${location.userLocataire!.lastName} ${location.userLocataire!.firstName}"),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Durée du bail",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  "Du ${location.debutBail!.split(" ")[0]} au ${location.finBail!.split(" ")[0]}",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Montant",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.money_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  "${location.montant} Fcfa (${location.recurencePaiement})",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Etat du contrat",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.check_circle_outline,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  location.etat == null ? "Actif" : "${location.etat}",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Bail Renouvellable",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            location.isRenouvellable == "1" ? "Oui" : "Non",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Mode de paiement",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            location.modesPaiement!,
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Taux TVA",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            location.isTva == "1" && location.tauxTva != "0"
                ? "${location.tauxTva}"
                : "Aucun",
          ),
        ),
        SizedBox(height: location.contratBail != null ? 20 : 0),
        location.contratBail != null
            ? Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Text(
                  "Contrat de bail PDF",
                  style: TextStyle(fontSize: 18, color: Colors.black),
                ),
              )
            : Container(),
        SizedBox(height: location.contratBail != null ? 7 : 0),
        location.contratBail != null
            ? Padding(
                padding: EdgeInsets.only(
                  left: getProportionateScreenWidth(20),
                  right: getProportionateScreenWidth(30),
                ),
                child: GestureDetector(
                  onTap: () {
                    locationController.download("${location.contratBail}");
                  },
                  child: Image.asset(
                    "assets/images/pdf.png",
                    width: MediaQuery.of(context).size.width * 0.2,
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
