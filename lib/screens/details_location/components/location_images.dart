import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Location.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class LocationImages extends StatefulWidget {
  LocationImages({
    Key? key,
    required this.location,
  }) : super(key: key);

  final Location location;

  @override
  _LocationImagesState createState() => _LocationImagesState();
}

class _LocationImagesState extends State<LocationImages> {
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  int selectedImage = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(238),
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: 'details-loc' + widget.location.id.toString(),
              child: FadeInImage.assetNetwork(
                  placeholder: "assets/images/spinner.gif",
                  image:
                      "${Request.IMAGE_LOAD_BASE + widget.location.bien!.galerieList![selectedImage].photo!}"),
            ),
          ),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ...List.generate(widget.location.bien!.galerieList!.length,
                (index) => buildSmallProductPreview(index)),
          ],
        )
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: EdgeInsets.only(right: 15),
        padding: EdgeInsets.all(8),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: FadeInImage.assetNetwork(
            placeholder: "assets/images/spinner.gif",
            image:
                "${Request.IMAGE_LOAD_BASE + widget.location.bien!.galerieList![index].photo!}"),
      ),
    );
  }
}
