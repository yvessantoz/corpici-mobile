import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/screens/editer_location/editer_location_screen.dart';
import 'package:score_immo/size_config.dart';

import 'action_buttons.dart';
import 'location_description.dart';
import 'top_rounded_container.dart';
import 'location_images.dart';

class Body extends StatelessWidget {
  final Location location;

  const Body({Key? key, required this.location}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        LocationImages(location: this.location),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              LocationDescription(
                location: this.location,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ActionButtons(location: this.location),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.screenWidth * 0.15,
                          right: SizeConfig.screenWidth * 0.15,
                          bottom: getProportionateScreenWidth(40),
                          top: getProportionateScreenWidth(15),
                        ),
                        child: DefaultButton(
                          text: "Editer",
                          press: () {
                            Get.to(EditerLocationScreen(), arguments: location);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
