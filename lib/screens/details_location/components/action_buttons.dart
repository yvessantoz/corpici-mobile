import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/rounded_icon_btn.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/screens/creer_locataire/locataire_screen.dart';
import 'package:score_immo/screens/list_locataire/list_locataire_screen.dart';

import '../../../size_config.dart';

class ActionButtons extends StatelessWidget {
  ActionButtons({
    Key? key,
    required this.location,
  }) : super(key: key);

  final Location location;
  final LocationController locationController = Get.put(LocationController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.file_download,
              press: () {
                SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON").warningDialog(
                    context,
                    "Vous allez générer un contrat de bail. \nÊtes-vous sûr de vouloir effectuer \ncette action ?",
                    "Alert", () {
                  locationController.genererContrat(location);
                }, () {});
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.location_history_outlined,
              press: () {
                Get.to(ListLocataireScreen());
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              icon: Icons.person_add_alt,
              showShadow: true,
              press: () {
                Get.to(CreerLocataireScreen());
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.delete_outline,
              iconColor: Colors.red,
              press: () {
                SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON")
                    .warningDialog(
                        context,
                        "Voulez-vous supprimer cette location ?",
                        "Attention", () {
                  locationController.delete(location);
                }, () {});
              },
            ),
          ],
        ),
      ),
    );
  }
}
