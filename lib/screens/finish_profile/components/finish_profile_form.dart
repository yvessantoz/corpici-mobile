import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/login_success/login_success_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class FinishProfileForm extends StatefulWidget {
  @override
  _FinishProfileFormState createState() => _FinishProfileFormState();
}

class _FinishProfileFormState extends State<FinishProfileForm> {
  final UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? phoneNumber;
  String? agency;
  String? taxpayer;
  String? commercialRegistry;
  String buttonText = "S'inscrire";

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildAgencyNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildCommercialRegistryAgencyFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPhoneNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTaxpayerAccountNumberFormField(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  finishFunction();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Nom de l'agence
  buildAgencyNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.text,
        onSaved: (newValue) => agency = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kAgencyNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kAgencyNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Nom de l'agence",
            hintText: "Nom de l'agence",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons.business_center_outlined)
            //CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
            ),
      ),
    );
  }

  // Numero de téléphone de l'agence
  buildPhoneNumberFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.phone,
        onSaved: (newValue) => phoneNumber = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPhoneNumberNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPhoneNumberNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Contact Entreprise",
            hintText: "Contact Entreprise",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .phone_android_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
            ),
      ),
    );
  }

  // Numéro de registre de commerce
  buildCommercialRegistryAgencyFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => commercialRegistry = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kTaxCommercialRegisterNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kTaxCommercialRegisterNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Registre de commerce N°",
            hintText: "Registre de commerce N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .person_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Numéro de compte contribuable
  buildTaxpayerAccountNumberFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => taxpayer = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kTaxpayerNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kTaxpayerNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Compte contribuable N°",
            hintText: "Compte contribuable N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .person_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  //
  finishFunction() async {
    setState(() {
      buttonText = verificationText;
    });
    // Récupération des informations en session
    User user = await userController.getUser("register");
    user.agenceName = agency;
    user.agencePhone = phoneNumber;
    user.agenceCompteContribuable = taxpayer;
    user.agenceRegistreCommerce = commercialRegistry;
    Map<dynamic, dynamic> response = await userController.register(user);
    if (response['status']) {
      Get.offAll(() => LoginSuccessScreen());
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
