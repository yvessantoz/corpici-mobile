import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/size_config.dart';

import 'finish_profile_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text("Terminez votre profil", style: headingStyle),
                Text(
                  "Veuillez complétez les champs ci-dessous afin de finaliser votre inscription.",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                FinishProfileForm(),
                SizedBox(height: getProportionateScreenHeight(30)),
                Text(
                  CGU,
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
