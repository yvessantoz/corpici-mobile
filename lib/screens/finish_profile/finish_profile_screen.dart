import 'package:flutter/material.dart';

import 'components/body.dart';

class FinishProfileScreen extends StatelessWidget {
  static String routeName = "/finish_profile";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Inscription'),
      ),
      body: Body(),
    );
  }
}
