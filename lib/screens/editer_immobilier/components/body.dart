import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/size_config.dart';

import 'editer_immobilier_form.dart';

class Body extends StatelessWidget {
  Body({this.pictureController, this.immobilier});

  final PictureController? pictureController;
  final Immobilier? immobilier;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text("Editer l'acquisition:", style: headingStyle),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.location_city_outlined,
                      color: Colors.black38,
                    ),
                    Text(
                      "${immobilier!.name}",
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                EditerImmobilierForm(
                    pictureController: pictureController,
                    immobilier: immobilier),
                SizedBox(height: getProportionateScreenHeight(30)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
