import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/helper/keyboard.dart';
import 'package:score_immo/screens/forgot_password/forgot_password_screen.dart';
import 'package:score_immo/screens/login_success/login_success_screen.dart';

import '../../../components/default_button.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class SignForm extends StatefulWidget {
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<SignForm> {
  final _formKey = GlobalKey<FormState>();
  String? login;
  String? password;
  String buttonText = "Se connecter";

  final List<String?> errors = [];

  final UserController userController = Get.put(UserController());

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPasswordFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          Row(
            children: [
              Spacer(),
              GestureDetector(
                onTap: () => Navigator.pushNamed(
                    context, ForgotPasswordScreen.routeName),
                child: Text(
                  "Mot de passe oublié ?",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(29)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  KeyboardUtil.hideKeyboard(context);
                  submitForm();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Password
  buildPasswordFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        obscureText: true,
        onSaved: (newValue) => {password = newValue},
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPassNullError);
          } else if (value.length >= 8) {
            removeError(error: kShortPassError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPassNullError);
            return "";
          } else if (value.length < 8) {
            addError(error: kShortPassError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Mot de passe",
            hintText: "Mot de passe",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .lock_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/Lock.svg"),
            ),
      ),
    );
  }

  // Email
  buildEmailFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.text,
        onSaved: (newValue) => login = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kLoginNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kLoginNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Identifiant",
            hintText: "Identifiant",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .person_outline_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
            ),
      ),
    );
  }

  // Envoi des données au serveur
  submitForm() async {
    // Activer le loader
    setState(() {
      buttonText = verificationText;
    });

    Map<dynamic, dynamic> result =
        await userController.login(login!, password!);
    if (result['status']) {
      Get.offAll(LoginSuccessScreen());
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
