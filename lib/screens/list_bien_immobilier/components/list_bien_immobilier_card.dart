import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/custom_bottom_sheets.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/screens/details_bien_immobilier/details_screen.dart';

import '../../../constants.dart';

class ListBienImmobilierCard extends StatefulWidget {
  final BienImmobilier bienImmobilier;
  ListBienImmobilierCard({
    Key? key,
    required this.bienImmobilier,
  }) : super(key: key);

  _ListBienImmobilierCardState createState() => _ListBienImmobilierCardState();
}

class _ListBienImmobilierCardState extends State<ListBienImmobilierCard> {
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          showBottomSheets(
              context,
              widget.bienImmobilier,
              BienImmobilerDetailsArguments(
                  bienImmobilier: widget.bienImmobilier),
              DetailsScreen(),
              "Voulez-vous supprimer ce bien ?",
              "Suppression de bien",
              bienImmobilierController);
        },
        child: Row(
          children: [
            SizedBox(
              width: 88,
              child: AspectRatio(
                aspectRatio: 0.88,
                child: Container(
                  padding:
                      EdgeInsets.all(8.8), //getProportionateScreenWidth(10)
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(3), //15
                  ),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/spinner.gif",
                    image:
                        Request.IMAGE_LOAD_BASE + widget.bienImmobilier.photo!,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 140,
                  child: Text(
                    "${widget.bienImmobilier.name}",
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
                SizedBox(height: 10),
                SizedBox(
                  width: 210,
                  child: Text.rich(
                    TextSpan(
                      text:
                          "${widget.bienImmobilier.immobilierItem!.name}", //"${bienImmobilier.description}"
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                    ),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    maxLines: 1,
                  ),
                )
              ],
            )
          ],
        ));
  }
}
