import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:score_immo/components/coustom_bottom_nav_bar.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/list_bottom_card.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/enums.dart';
import 'package:score_immo/screens/creer_bien_immobilier/creer_bien_immobilier_screen.dart';
import 'package:score_immo/screens/list_bien_immobilier/components/bottom_sheet.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';

import 'components/body.dart';

class ListBienImmobilierScreen extends StatelessWidget {
  BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  //
  initMethods() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await bienImmobilierController.initAll();
    } else {
      SoftDialog().dangerDialog(Get.context!,
          "Désolé vous n'avez pas accès à internet", "Erreur", () {}, () {});
    }
  }

  static String routeName = "/list_bien_immobilier";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: Body(),
      ),
      // bottomNavigationBar:
      //     CustomBottomNavBar(selectedMenu: MenuState.bienImmobilier),
      // bottomNavigationBar: ListBottomCard(
      //     linkTitle: "Afficher les locations",
      //     buttonPress: () {
      //       Get.to(CreerBienImmobilierScreen());
      //     },
      //     buttonText: "Ajouter",
      //     linkPress: () {
      //       // _settingModalBottomSheet(context);
      //       Get.to(LocationScreen());
      //     },
      //     total: bienImmobilierController.bienImmobiliersList.length,
      //     icon: Icons.home_work_outlined),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      actions: [
        GestureDetector(
          onTap: () {
            Get.to(CreerBienImmobilierScreen());
          },
          child: Padding(
            padding: EdgeInsets.only(right: 20),
            child: Icon(
              Icons.add_business_outlined,
              size: 30,
            ),
          ),
        )
      ],
      title: Column(
        children: [
          Text(
            "Mes Biens Immobiliers",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "${bienImmobilierController.bienImmobiliersList.length} élément(s)",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      context: context,
      builder: (BuildContext bc) {
        return DetailsModalBottomSheet();
      });
}
