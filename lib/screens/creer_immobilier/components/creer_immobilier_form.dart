import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/components/list_image_card.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:score_immo/screens/list_immobilier/list_immobilier_screen.dart';
import 'package:score_immo/utils/utils.dart';
import 'package:multi_select_flutter/multi_select_flutter.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CreerImmobilierForm extends StatefulWidget {
  CreerImmobilierForm({this.pictureController});
  final PictureController? pictureController;

  @override
  _CreerImmobilierFormState createState() => _CreerImmobilierFormState();
}

class _CreerImmobilierFormState extends State<CreerImmobilierForm> {
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  final _formKey = GlobalKey<FormState>();
  final _multiSelectKey = GlobalKey<FormFieldState>();

  final List<String?> errors = [];
  String? name;
  String? adresse;
  String? pays;
  String? region;
  String? ville;
  String? dateConstruction;
  String? description;
  // String? equipements;
  List<dynamic>? equipements;
  DateTime _selectedDate = new DateTime.now();
  String selectedType = "Type d'immobilier";
  List<String> immobilierType = Immobilier.typeList;
  String buttonText = "Ajouter";

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  void initState() {
    super.initState();
    //initializeDateFormatting();
  }

  @override
  void dispose() {
    //immobilierController.dispose();
    //print("immobilierController was delete in memory");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildAdresseFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPaysFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildRegionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildVilleFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDateConstructionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          //buildEquipementFormField(),
          buildEquipementSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDescriptionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeImmobilierSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          previewImages(),
          SizedBox(height: getProportionateScreenHeight(15)),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  sendDataToServer();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Name
  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNameNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNameNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Libellé",
          hintText: "Libellé",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .location_city_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Adresse
  TextFormField buildAdresseFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => adresse = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kAdresseNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kAdresseNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Adresse",
          hintText: "Adresse",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .location_on_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Pays
  TextFormField buildPaysFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => pays = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPaysNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPaysNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Pays",
          hintText: "Pays",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .map_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Région
  TextFormField buildRegionFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => region = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kRegionNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kRegionNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Région",
          hintText: "Région",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .map_sharp) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Ville
  TextFormField buildVilleFormField() {
    return TextFormField(
      onSaved: (newValue) => ville = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kVilleNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kVilleNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Ville",
          hintText: "Ville",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .streetview_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Date de construction
  TextFormField buildDateConstructionFormField() {
    return TextFormField(
      maxLines: 1,
      readOnly: true,
      onSaved: (newValue) => dateConstruction = newValue,
      // onChanged: (value) {
      //   if (value.isNotEmpty) {
      //     removeError(error: kDateConstructionNullError);
      //   }
      //   return null;
      // },
      // validator: (value) {
      //   if (value!.isEmpty) {
      //     addError(error: kDateConstructionNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
          labelText: "Date de construction",
          hintText: DateFormat.yMd('fr').format(_selectedDate),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
            icon: Icon(Icons.calendar_today_outlined),
            onPressed: () {
              getDateFromUser();
            },
          )),
    );
  }

  // Description
  TextFormField buildDescriptionFormField() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      minLines: 5,
      maxLines: 5,
      onSaved: (newValue) => description = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kDescriptionNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kDescriptionNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Description",
        hintText: "Décrivez l'immobilier en quelques mots",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  // Equipement
  // TextFormField buildEquipementFormField() {
  //   return TextFormField(
  //     keyboardType: TextInputType.multiline,
  //     minLines: 5,
  //     maxLines: 5,
  //     onSaved: (newValue) => equipements = newValue,
  //     // onChanged: (value) {
  //     //   if (value.isNotEmpty) {
  //     //     removeError(error: kDescriptionNullError);
  //     //   }
  //     //   return null;
  //     // },
  //     // validator: (value) {
  //     //   if (value!.isEmpty) {
  //     //     addError(error: kDescriptionNullError);
  //     //     return "";
  //     //   }
  //     //   return null;
  //     // },
  //     decoration: InputDecoration(
  //       labelText: "Equipements",
  //       hintText:
  //           "Ajouter tous les équipements en les séparant par une virgule",
  //       floatingLabelBehavior: FloatingLabelBehavior.always,
  //       //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
  //     ),
  //   );
  // }

  // Equipements selection
  buildEquipementSelectInput() {
    final _items = immobilierController.equipementList
        .map((item) => MultiSelectItem<dynamic>(item, item))
        .toList();

    return Container(
      child: MultiSelectBottomSheetField<dynamic>(
        listType: MultiSelectListType.LIST,
        searchHint: "Rechercher",
        buttonIcon: Icon(
          Icons.arrow_drop_down,
          color: Colors.black45,
        ),
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black54,
          ),
          borderRadius: BorderRadius.circular(23),
          color: Colors.white,
        ),
        key: _multiSelectKey,
        initialChildSize: 0.7,
        maxChildSize: 0.95,
        title: Text("Equipements"),
        buttonText: Text("Choisissez des équipements"),
        confirmText: Text("OK"),
        cancelText: Text("Annuler"),
        items: _items,
        searchable: true,
        // validator: (values) {
        //   if (values == null || values.isEmpty) {
        //     return "Veuillez selectionn";
        //   }
        //   List<String> names = values.map((e) => e.name).toList();
        //   if (names.contains("Frog")) {
        //     return "Frogs are weird!";
        //   }
        //   return null;
        // },
        onConfirm: (values) {
          setState(() {
            equipements = values;
          });
          _multiSelectKey.currentState!.validate();
        },
        chipDisplay: MultiSelectChipDisplay(
          onTap: (item) {
            setState(() {
              equipements!.remove(item);
            });
            _multiSelectKey.currentState!.validate();
          },
        ),
      ),
    );
  }

  // Type d'immobilier
  buildTypeImmobilierSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Type d\'immobilier',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: immobilierType
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return "Type d'immobilier";
        }
      },
      onChanged: (value) {
        selectedType = value.toString();
      },
      onSaved: (value) {
        selectedType = value.toString();
      },
    ));
  }

  void getDateFromUser() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedDate = _pickedDate;
      });
    }
  }

  // Prévisualiser les images sélectionnées
  previewImages() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        child: widget.pictureController!.obx((state) {
          var files = widget.pictureController!.fileList;
          var photo = XFile(widget.pictureController!.selectedImagePath.value);

          return Row(
            children: [
              if (photo.path.isNotEmpty) ListImageCard(image: photo),
              ...List.generate(
                files.length,
                (index) {
                  if (files.length > 0)
                    return ListImageCard(image: files[index]);
                  return SizedBox.shrink();
                  // here by default width and height is 0
                },
              ),
              SizedBox(width: getProportionateScreenWidth(20)),
            ],
          );
        }, onLoading: CircularProgressIndicator(color: kPrimaryColor)),
      ),
    );
  }

  //
  sendDataToServer() async {
    setState(() {
      buttonText = verificationText;
    });
    Immobilier immobilier = Immobilier();
    immobilier.immobilierType = selectedType;
    immobilier.name = name;
    immobilier.pays = pays;
    immobilier.ville = ville;
    immobilier.adresse = adresse;
    immobilier.description = description;
    immobilier.equipements = equipements; //!.split(",")
    immobilier.dateConstruction = formatDate(_selectedDate, "yyyy-MM-dd");
    immobilier.region = region;
    List<XFile> galerie = widget.pictureController!.filesSelected();
    String pictureSelected = widget.pictureController!.pictureSelected();
    PickedFile photo = PickedFile(pictureSelected);
    var result;

    // Traitement des images avant soumission du formulaire
    if (galerie.isNotEmpty && pictureSelected.isNotEmpty) {
      result = await immobilierController.ajouterAcquision(
          immobilier, photo, galerie);
    } else if (galerie.isEmpty) {
      setState(() {
        buttonText = repeatText;
      });
      SoftDialog(btnOkOnPress: "OK").dangerDialog(
          context,
          "Veuillez ajouter des images dans la galerie",
          "Image(s) manquante(s)",
          () {},
          () {});
      return null;
    } else if (pictureSelected.isEmpty) {
      setState(() {
        buttonText = repeatText;
      });
      SoftDialog(btnOkOnPress: "OK").dangerDialog(context,
          "Veuillez ajouter une photo", "Photo manquante", () {}, () {});
      return null;
    }

    if (result['status']) {
      SoftDialog(btnCancelText: "Fermer")
          .successDialog(context, "Enregistrement effectué", "Succès", () {
        Get.off(() => NavigationBarBottom(
              index: 1,
            )); //Get.off(ListImmobilierScreen());
      }, () {
        Get.off(() => NavigationBarBottom(
              index: 1,
            )); //Get.off(ListImmobilierScreen());
      });
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
