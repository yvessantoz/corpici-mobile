import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/screens/editer_bien_immobilier/editer_bien_immobilier_screen.dart';
import 'package:score_immo/size_config.dart';

import 'action_buttons.dart';
import 'bien_immobilier_description.dart';
import 'top_rounded_container.dart';
import 'bien_immobilier_images.dart';

class Body extends StatelessWidget {
  final BienImmobilier bienImmobilier;

  const Body({Key? key, required this.bienImmobilier}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        BienImmobilierImages(bienImmobilier: this.bienImmobilier),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              BienImmobilierDescription(
                bienImmobilier: this.bienImmobilier,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ActionButtons(bienImmobilier: this.bienImmobilier),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.screenWidth * 0.15,
                          right: SizeConfig.screenWidth * 0.15,
                          bottom: getProportionateScreenWidth(40),
                          top: getProportionateScreenWidth(15),
                        ),
                        child: DefaultButton(
                          text: "Editer",
                          press: () {
                            Get.to(EditerBienImmobilierScreen(),
                                arguments: bienImmobilier);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
