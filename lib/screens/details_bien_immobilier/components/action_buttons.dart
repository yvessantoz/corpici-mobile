import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/rounded_icon_btn.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/screens/creer_locataire/locataire_screen.dart';
import 'package:score_immo/screens/creer_location/creer_location_screen.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';

import '../../../size_config.dart';

class ActionButtons extends StatelessWidget {
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  ActionButtons({
    Key? key,
    required this.bienImmobilier,
  }) : super(key: key);

  final BienImmobilier bienImmobilier;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.local_hotel_outlined,
              press: () {
                Get.to(CreerLocationScreen(), arguments: bienImmobilier);
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.list_alt_outlined,
              press: () {
                Get.to(LocationScreen(), arguments: bienImmobilier);
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              icon: Icons.person_add_alt,
              showShadow: true,
              press: () {
                Get.to(CreerLocataireScreen());
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.delete_outline_outlined,
              iconColor: Colors.red,
              press: () {
                SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON")
                    .warningDialog(
                        context, "Voulez-vous supprimer ce bien ?", "Attention",
                        () {
                  bienImmobilierController.delete(bienImmobilier);
                }, () {});
              },
            ),
          ],
        ),
      ),
    );
  }
}
