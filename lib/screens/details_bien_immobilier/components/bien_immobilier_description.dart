import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/screens/list_bien_immobilier/components/bottom_sheet.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class BienImmobilierDescription extends StatelessWidget {
  BienImmobilierDescription({
    Key? key,
    required this.bienImmobilier,
    this.pressOnSeeMore,
  }) : super(key: key);

  final BienImmobilier bienImmobilier;
  final GestureTapCallback? pressOnSeeMore;
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "${bienImmobilier.name}",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.location_on_outlined,
                    size: 18,
                    color: Colors.black45,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Text(
                      "${bienImmobilier.adresse}",
                      maxLines: 3,
                    ),
                  ),
                ],
              ),
            )),
        SizedBox(
          height: 8,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.home_work_outlined,
                  size: 18,
                  color: Colors.black45,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    "${bienImmobilier.bienType}",
                    maxLines: 3,
                  ),
                ),
              ],
            )),
        SizedBox(
          height: 8,
        ),
        Padding(
            padding: EdgeInsets.only(
              left: getProportionateScreenWidth(20),
              right: getProportionateScreenWidth(34),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(
                  Icons.location_city_outlined,
                  size: 18,
                  color: Colors.black45,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    "${bienImmobilier.immobilierItem!.name!}",
                    maxLines: 3,
                  ),
                ),
              ],
            )),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Caractéristiques",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(34),
          ),
          child: Text(
              "${bienImmobilier.nbChambre} Chambre(s), ${bienImmobilier.nbPiece} Pièce(s), ${bienImmobilier.nbSalleBain} Salle(s) de bain"),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Description",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            bienImmobilier.description!,
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Superficie",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            "${bienImmobilier.superficie!} m²",
          ),
        ),
        SizedBox(height: 13),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(20),
            vertical: 10,
          ),
          // child: GestureDetector(
          //   onTap: () {
          //     BottomSheets().settingModalBottomSheet(context);
          //   },
          //   child: Row(
          //     children: [
          //       Text(
          //         "Afficher les statistiques",
          //         style: TextStyle(
          //             fontWeight: FontWeight.w600, color: kPrimaryColor),
          //       ),
          //       SizedBox(width: 5),
          //       Icon(
          //         Icons.arrow_forward_ios,
          //         size: 12,
          //         color: kPrimaryColor,
          //       ),
          //     ],
          //   ),
          // ),
        )
      ],
    );
  }
}

class BottomSheets {
  // Affiche le modal des statistiques
  void settingModalBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20.0),
            topRight: Radius.circular(20.0),
          ),
        ),
        context: context,
        builder: (BuildContext bc) {
          return DetailsModalBottomSheet();
        });
  }
}
