import 'package:flutter/material.dart';
import 'package:score_immo/models/Bien.dart';

import 'components/body.dart';
import 'components/custom_app_bar.dart';

class DetailsScreen extends StatelessWidget {
  static String routeName = "/details";

  @override
  Widget build(BuildContext context) {
    final BienImmobilerDetailsArguments agrs = ModalRoute.of(context)!
        .settings
        .arguments as BienImmobilerDetailsArguments;
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: CustomAppBar(),
      ),
      body: Body(bienImmobilier: agrs.bienImmobilier),
    );
  }
}

class BienImmobilerDetailsArguments {
  final BienImmobilier bienImmobilier;

  BienImmobilerDetailsArguments({required this.bienImmobilier});
}
