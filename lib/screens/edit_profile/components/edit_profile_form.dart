import 'dart:developer';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:intl/intl.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/show_profile/show_profile_screen.dart';
import 'package:score_immo/utils/utils.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class EditProfileForm extends StatefulWidget {
  @override
  _EditProfileFormState createState() => _EditProfileFormState();
}

class _EditProfileFormState extends State<EditProfileForm> {
  User user = User();
  UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? firstName;
  String? lastName;
  String? birthPlace;
  String? typeID;
  String? iD;
  String? expirationID;
  String? fonction;
  String? income;
  String? domainActivity;
  String? qualification;
  String? professionnalStatus;
  String? birthDate;
  String? agency;
  String? taxpayer;
  String? commercialRegistry;
  String? phoneNumber;
  String? agencePhone;
  String? agencyPhoneNumber;
  String selectedStatus = "";
  String? selectedQualification;
  String selectedTypeID = "";
  String selectedTypeUser = "";
  DateTime _selectedBirthDate = new DateTime.now();
  DateTime _selectedExpirationDate = new DateTime.now();
  String buttonText = "Editer";

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    // Récupération des données passées en paramètre à la route
    final args = ModalRoute.of(context)!.settings.arguments as User;
    user = args;

    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildLastNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildFirstNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBirthDateFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBirthPlaceFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildFonctionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildQualificationFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIncomeFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDomainActivityFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIDFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildExpirationIDFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPhoneNumberFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          user.userType == "Agence Immobilier"
              ? buildAgencyNameFormField()
              : Container(),
          SizedBox(
              height: user.userType == "Agence Immobilier"
                  ? getProportionateScreenHeight(30)
                  : 0),
          user.userType == "Agence Immobilier"
              ? buildCommercialRegistryAgencyFormField()
              : Container(),
          SizedBox(
              height: user.userType == "Agence Immobilier"
                  ? getProportionateScreenHeight(30)
                  : 0),
          user.userType == "Agence Immobilier"
              ? buildTaxpayerAccountNumberFormField()
              : Container(),
          SizedBox(
              height: user.userType == "Agence Immobilier"
                  ? getProportionateScreenHeight(30)
                  : 0),
          user.userType == "Agence Immobilier"
              ? buildAgencyPhoneNumberFormField()
              : Container(),
          SizedBox(
              height: user.userType == "Agence Immobilier"
                  ? getProportionateScreenHeight(30)
                  : 0),
          buildTypeIDSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildProfessionnalStatusSelectInput(),
          //SizedBox(height: getProportionateScreenHeight(30)),
          //buildQualificationSelectInput(),
          //SizedBox(height: getProportionateScreenHeight(30)),
          //buildTypeUserSelectInput(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  saveForm();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Lieu de naissance
  buildBirthPlaceFormField() {
    return Container(
        height: 60,
        child: TextFormField(
          initialValue: user.lieuNaissance ?? '',
          onSaved: (newValue) => birthPlace = newValue,
          onChanged: (value) {
            if (value.isNotEmpty) {
              removeError(error: kBirthPlaceNullError);
            }
            return null;
          },
          validator: (value) {
            if (value!.isEmpty) {
              addError(error: kBirthPlaceNullError);
              return "";
            }
            return null;
          },
          decoration: InputDecoration(
              labelText: "Lieu de naissance",
              hintText: "Lieu de naissance",
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: Icon(Icons.my_location_outlined)
              //CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
              ),
        ));
  }

  // BirthDate
  TextFormField buildBirthDateFormField() {
    return TextFormField(
      initialValue: replace("-", "/", user.dateNaissance ?? ''),
      readOnly: true,
      keyboardType: TextInputType.datetime,
      onSaved: (newValue) => birthDate = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kBirthDateNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kBirthDateNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Date de naissance",
          hintText: DateFormat.yMd('fr').format(_selectedBirthDate),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
            icon: Icon(Icons.calendar_today_outlined),
            onPressed: () {
              getBirthDate();
            },
          )
          //CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
          ),
    );
  }

  // Nom de famille
  buildLastNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.lastName ?? '',
        onSaved: (newValue) => lastName = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kNamelNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kNamelNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Nom",
          hintText: "Nom",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_rounded),
        ),
      ),
    );
  }

  // Prenom
  buildFirstNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.firstName ?? '',
        onSaved: (newValue) => firstName = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kFirstNamelNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kFirstNamelNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Prénom(s)",
          hintText: "Prénom(s)",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_rounded),
        ),
      ),
    );
  }

  // Numéro de la pièce CNI/PASSPORT/CARTE DE SEJOUR/...
  buildIDFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.numeroPiece ?? '',
        onSaved: (newValue) => iD = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kIDNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kIDNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Pièce d'identité N°",
            hintText: "Pièce d'identité N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .card_membership_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Date d'expiration de la pièce
  buildExpirationIDFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: replace("-", "/", user.expirationPiece ?? ''),
        keyboardType: TextInputType.datetime,
        readOnly: true,
        onSaved: (newValue) => expirationID = newValue,
        onChanged: (value) {
          // if (value.isNotEmpty) {
          //   removeError(error: kExpirationIDNullError);
          // }
          // return null;
        },
        validator: (value) {
          // if (value!.isEmpty) {
          //   addError(error: kExpirationIDNullError);
          //   return "";
          // }
          // return null;
        },
        decoration: InputDecoration(
            labelText: "Date d'expiration de la pièce",
            hintText: DateFormat.yMd('fr').format(_selectedExpirationDate),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: IconButton(
              icon: Icon(Icons.calendar_today_outlined),
              onPressed: () {
                getExpirationDate();
              },
            )),
      ),
    );
  }

  // Fonction
  buildFonctionFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.fonction ?? '',
        onSaved: (newValue) => fonction = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kFonctionNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kFonctionNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
            labelText: "Fonction Occupée",
            hintText: "Fonction Occupée",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .work_outline_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Revenu mensuel
  buildIncomeFormField() {
    return Container(
        height: 60,
        child: TextFormField(
          initialValue: user.revenuMensuel ?? '',
          keyboardType: TextInputType.number,
          onSaved: (newValue) => income = newValue,
          // onChanged: (value) {
          //   if (value.isNotEmpty) {
          //     removeError(error: kIncomeNullError);
          //   }
          //   return null;
          // },
          // validator: (value) {
          //   if (value!.isEmpty) {
          //     addError(error: kIncomeNullError);
          //     return "";
          //   }
          //   return null;
          // },
          decoration: InputDecoration(
              labelText: "Revenu mensuel",
              hintText: "Revenu mensuel",
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: Icon(
                Icons.money_rounded,
              ) //CustomSurffixIcon(svgIcon: "assets/icons/calendar.svg"),
              ),
        ));
  }

  // Domaine d'activité
  buildDomainActivityFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.domaineActivite ?? '',
        onSaved: (newValue) => domainActivity = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kDomainActivityNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kDomainActivityNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
            labelText: "Domaine d'activité",
            hintText: "Domaine d'activité",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .local_activity_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Statut professionnel
  buildProfessionnalStatusSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
            decoration: InputDecoration(
              isDense: true,
              contentPadding: EdgeInsets.zero,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
              ),
            ),
            isExpanded: true,
            hint: const Text(
              'Statut professionnel',
              style: TextStyle(fontSize: 14),
            ),
            icon: const Icon(
              Icons.arrow_drop_down,
              color: Colors.black45,
            ),
            iconSize: 30,
            buttonHeight: 60,
            buttonPadding: const EdgeInsets.only(left: 20, right: 10),
            dropdownDecoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            items: professionnalStatusItems
                .map((item) => DropdownMenuItem<String>(
                      value: item,
                      child: Text(
                        item,
                        style: const TextStyle(
                          fontSize: 14,
                        ),
                      ),
                    ))
                .toList(),
            validator: (value) {
              if (value == null || value == "Statut professionnel") {
                return 'Veuillez sélectionnez un statut.';
              }
            },
            onChanged: (value) {},
            onSaved: (value) {
              selectedStatus = value.toString();
            },
            value: user.statutProfessionnel ?? "SALARIE"));
  }

  // Qualification
  // buildQualificationSelectInput() {
  //   return Container(
  //       child: DropdownButtonFormField2(
  //     decoration: InputDecoration(
  //       isDense: true,
  //       contentPadding: EdgeInsets.zero,
  //       border: OutlineInputBorder(
  //         borderRadius: BorderRadius.circular(15),
  //       ),
  //     ),
  //     isExpanded: true,
  //     hint: const Text(
  //       'Qualification',
  //       style: TextStyle(fontSize: 14),
  //     ),
  //     icon: const Icon(
  //       Icons.arrow_drop_down,
  //       color: Colors.black45,
  //     ),
  //     iconSize: 30,
  //     buttonHeight: 60,
  //     buttonPadding: const EdgeInsets.only(left: 20, right: 10),
  //     dropdownDecoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(15),
  //     ),
  //     items: qualificationItems
  //         .map((item) => DropdownMenuItem<String>(
  //               value: item,
  //               child: Text(
  //                 item,
  //                 style: const TextStyle(
  //                   fontSize: 14,
  //                 ),
  //               ),
  //             ))
  //         .toList(),
  //     validator: (value) {
  //       if (value == null) {
  //         return 'Veuillez sélectionnez une qualification.';
  //       }
  //     },
  //     onChanged: (value) {
  //       //Do something when changing the item if you want.
  //     },
  //     onSaved: (value) {
  //       selectedQualification = value.toString();
  //     },
  //     value: user.qualification ?? "Item1",
  //   ));
  // }

  // Type de pièce
  buildTypeIDSelectInput() {
    return Container(
      child: DropdownButtonFormField2(
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          isExpanded: true,
          hint: const Text(
            'Type de pièce d\'identité',
            style: TextStyle(fontSize: 14),
          ),
          icon: const Icon(
            Icons.arrow_drop_down,
            color: Colors.black45,
          ),
          iconSize: 30,
          buttonHeight: 60,
          buttonPadding: const EdgeInsets.only(left: 20, right: 10),
          dropdownDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          items: typeIDItems
              .map((item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ))
              .toList(),
          validator: (value) {
            if (value == null || value == "Type de pièce") {
              return 'Veuillez sélectionnez un type de pièce.';
            }
          },
          onChanged: (value) {},
          onSaved: (value) {
            selectedTypeID = value.toString();
          },
          value: user.typePiece ?? "CNI",
          dropdownMaxHeight: 210.0),
    );
  }

  // Type d'utilisateur
  buildTypeUserSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Type d\'utilisateur',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: typeUserItems
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Type d'utilisateur") {
          return 'Veuillez sélectionnez un type d\'utilisateur.';
        }
      },
      onChanged: (value) {
        selectedTypeUser = value.toString();
      },
      onSaved: (value) {
        selectedTypeUser = value.toString();
      },
      value: user.userType ?? "PROPRIETAIRE",
    ));
  }

  // Nom de l'agence
  buildAgencyNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.agenceName ?? '',
        keyboardType: TextInputType.text,
        onSaved: (newValue) => agency = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kAgencyNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kAgencyNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Nom de l'agence",
            hintText: "Nom de l'agence",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons.business_center_outlined)
            //CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
            ),
      ),
    );
  }

  // Numero de téléphone de l'agence
  buildAgencyPhoneNumberFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.agencePhone ?? '',
        keyboardType: TextInputType.phone,
        onSaved: (newValue) => agencyPhoneNumber = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPhoneNumberNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPhoneNumberNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Contact Entreprise",
            hintText: "Contact Entreprise",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .phone_android_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
            ),
      ),
    );
  }

  // Numero de téléphone de l'utilisateur
  buildPhoneNumberFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.mobile ?? '',
        keyboardType: TextInputType.phone,
        onSaved: (newValue) => phoneNumber = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPhoneNumberNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPhoneNumberNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Contact",
          hintText: "Contact",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.phone_android_outlined),
        ),
      ),
    );
  }

  // Numéro de registre de commerce
  buildCommercialRegistryAgencyFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.agenceRegistreCommerce ?? '',
        onSaved: (newValue) => commercialRegistry = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kTaxCommercialRegisterNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kTaxCommercialRegisterNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Registre de commerce N°",
            hintText: "Registre de commerce N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .person_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Numéro de compte contribuable
  buildTaxpayerAccountNumberFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.agenceCompteContribuable ?? '',
        onSaved: (newValue) => taxpayer = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kTaxpayerNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kTaxpayerNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Compte contribuable N°",
            hintText: "Compte contribuable N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .person_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Qualification
  buildQualificationFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        initialValue: user.qualification,
        onSaved: (newValue) => selectedQualification = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kQualificationNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kQualificationNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
          labelText: "Qualification",
          hintText: "Qualification",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_rounded),
        ),
      ),
    );
  }

  // Date d'expiration de la piece
  void getExpirationDate() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedExpirationDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedExpirationDate = _pickedDate;
      });
    }
  }

  // Date d'anniversaire
  void getBirthDate() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedBirthDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedBirthDate = _pickedDate;
      });
    }
  }

  //
  saveForm() async {
    //Activation du loader
    setState(() {
      buttonText = verificationText;
    });

    // Actualisation des informations
    user.lastName = lastName;
    user.firstName = firstName;
    user.lieuNaissance = birthPlace;
    user.dateNaissance = formatDate(_selectedBirthDate, 'yyyy-MM-dd');
    user.typePiece = selectedTypeID;
    user.numeroPiece = iD;
    user.expirationPiece = formatDate(_selectedExpirationDate, 'yyyy-MM-dd');
    user.fonction = fonction;
    user.revenuMensuel = income;
    user.domaineActivite = domainActivity;
    user.qualification = selectedQualification;
    user.statutProfessionnel = selectedStatus;
    user.mobile = phoneNumber;

    if (selectedTypeUser == "Propriétaire") {
      await sendDataToServer(user);
    } else {
      user.agenceName = agency;
      user.agencePhone = agencyPhoneNumber;
      user.agenceCompteContribuable = taxpayer;
      user.agenceRegistreCommerce = commercialRegistry;
      await sendDataToServer(user);
    }
  }

  //
  sendDataToServer(User user) async {
    Map<dynamic, dynamic> response = await userController.updateUser(user);
    // Stoppe le loader
    //log("${response['status']}");
    if (response['status']) {
      SoftDialog(btnOkOnPress: "OK", btnCancelText: "Fermer")
          .successDialog(context, "Modification effectuée.", "Succès", () {
        Get.off(() => ShowProfileScreen());
      }, () {
        Get.off(() => ShowProfileScreen());
      });
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
