import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/coustom_bottom_nav_bar.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/enums.dart';
import 'package:score_immo/screens/creer_bien_immobilier/creer_bien_immobilier_screen.dart';
import 'package:score_immo/screens/creer_immobilier/creer_immobilier_screen.dart';
import 'package:score_immo/screens/creer_locataire/locataire_screen.dart';
import 'package:score_immo/screens/creer_location/creer_location_screen.dart';
import 'package:score_immo/screens/list_tous_locataires/list_tous_locataires_screen.dart';
import 'package:score_immo/size_config.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'components/body.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";

  ImmobilierController immobilierController = Get.put(ImmobilierController());
  BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());
  LocationController locationController = Get.put(LocationController());

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  //
  initMethods() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await immobilierController.initAll();
      await bienImmobilierController.initAll();
      await locationController.initAll();
    } else {
      SoftDialog().dangerDialog(Get.context!,
          "Désolé vous n'avez pas accès à internet", "Erreur", () {}, () {});
    }
  }

  var myMenuItems = <String>[
    'Ajouter une acquisition',
    'Ajouter un bien',
    'Ajouter une location',
    'Ajouter un locataire',
    'Afficher tous les locataires',
  ];

  void onSelect(item) {
    switch (item) {
      case 'Ajouter une acquisition':
        Get.to(() => CreerImmobilierScreen());
        break;
      case 'Ajouter un bien':
        Get.to(() => CreerBienImmobilierScreen());
        break;
      case 'Ajouter une location':
        Get.to(() => CreerLocationScreen());
        break;
      case 'Ajouter un locataire':
        Get.to(() => CreerLocataireScreen());
        break;
      case 'Afficher tous les locataires':
        Get.to(() => ListTousLocataireScreen());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          title: Text(
            "Accueil",
            style: TextStyle(
                color: Colors.black, fontSize: 32, fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            GestureDetector(
              onTap: () {
                initMethods();
              },
              child: Icon(Icons.repeat_rounded),
            ),
            PopupMenuButton<String>(
                onSelected: onSelect,
                itemBuilder: (BuildContext context) {
                  return myMenuItems.map((String choice) {
                    return PopupMenuItem<String>(
                      child: Text(choice),
                      value: choice,
                    );
                  }).toList();
                })
          ]),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: Body(),
      ),
      // bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}
