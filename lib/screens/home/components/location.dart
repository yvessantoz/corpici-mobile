import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/location_card.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';

import '../../../size_config.dart';
import 'section_title.dart';

class LocationsHome extends StatefulWidget {
  @override
  _LocationsState createState() => _LocationsState();
}

class _LocationsState extends State<LocationsHome> {
  final LocationController locationController = Get.put(LocationController());

  @override
  initState() {
    //fetchImmobiliers();
    super.initState();
  }

  fetchImmobiliers() async {
    await locationController.fetchLocationWhere();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Obx(() {
            return SectionTitle(
                title:
                    "Les Locations(${locationController.locationList.length})",
                press: () {
                  Get.off(() => NavigationBarBottom(
                        index: 3,
                      )); //Get.to(LocationScreen());
                });
          }),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            child: locationController.obx(
                (state) => Row(
                      children: [
                        ...List.generate(
                          state!.length,
                          (index) {
                            if (state.length > 0)
                              return LocationCard(location: state[index]);
                            return SizedBox
                                .shrink(); // here by default width and height is 0
                          },
                        ),
                        SizedBox(width: getProportionateScreenWidth(20)),
                      ],
                    ),
                onLoading: Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.5),
                    child: CircularProgressIndicator(
                      color: kPrimaryColor,
                    )),
                onEmpty: Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.15),
                  child: Text("Nous n'avons trouvé aucun élément."),
                ),
                onError: (error) => Padding(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.15),
                      child: Text("Désolé, une erreur est survenue."),
                    )),
          ),
        )
      ],
    );
  }
}
