import 'package:flutter/material.dart';
import 'package:score_immo/screens/home/components/bien_immobilier.dart';
import 'package:score_immo/screens/home/components/location.dart';

import '../../../size_config.dart';
import 'home_header.dart';
import 'immobilier.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            // SizedBox(height: getProportionateScreenHeight(20)),
            // HomeHeader(),
            SizedBox(height: getProportionateScreenWidth(30)),
            ImmobilierListHome(),
            SizedBox(height: getProportionateScreenWidth(30)),
            BienImmobilierHome(),
            SizedBox(height: getProportionateScreenWidth(30)),
            LocationsHome(),
            SizedBox(height: getProportionateScreenWidth(30)),
          ],
        ),
      ),
    );
  }
}
