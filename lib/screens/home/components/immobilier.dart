import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/screens/details_immobilier/details_screen.dart';
import 'package:score_immo/screens/list_immobilier/list_immobilier_screen.dart';

import '../../../size_config.dart';
import 'section_title.dart';

class ImmobilierListHome extends StatefulWidget {
  @override
  _ImmobilierState createState() => _ImmobilierState();
}

class _ImmobilierState extends State<ImmobilierListHome> {
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  @override
  initState() {
    //fetchImmobiliers();
    super.initState();
  }

  fetchImmobiliers() async {
    await immobilierController.fetchImmobiliers();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Obx(() {
            return SectionTitle(
              title:
                  "Les Acquisitions(${immobilierController.immobiliersList.length})",
              press: () {
                Get.off(() => NavigationBarBottom(
                      index: 1,
                    ));
              },
            );
          }),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            child: immobilierController.obx(
                (state) => Row(
                      children: [
                        ...List.generate(state!.length, (index) {
                          //var immoList = immobilierController.immobiliersList;
                          if (state.length > 0) {
                            return ImmobilierCard(
                              image: state[index].photo!,
                              category: state[index].immobilierType!,
                              numOfBrands: state[index].equipements!.length,
                              name: state[index].name!,
                              press: () {
                                Get.to(() => DetailsScreen(),
                                    arguments: ImmobilerDetailsArguments(
                                        immobilier: state[index]));
                              },
                            );
                          }
                          return SizedBox.shrink();
                        }),
                        SizedBox(width: getProportionateScreenWidth(20)),
                      ],
                    ),
                onLoading: Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.5),
                    child: CircularProgressIndicator(
                      color: kPrimaryColor,
                    )),
                onEmpty: Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.15),
                  child: Text("Nous n'avons trouvé aucun élément."),
                ),
                onError: (error) => Padding(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.15),
                      child: Text("Désolé, une erreur est survenue."),
                    )),
          ),
        ),
      ],
    );
  }
}

class ImmobilierCard extends StatelessWidget {
  const ImmobilierCard({
    Key? key,
    required this.category,
    required this.image,
    required this.numOfBrands,
    required this.name,
    required this.press,
  }) : super(key: key);

  final String category, image;
  final int numOfBrands;
  final GestureTapCallback press;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: getProportionateScreenWidth(20)),
      child: GestureDetector(
        onTap: press,
        child: SizedBox(
          width: getProportionateScreenWidth(242), //242 //142
          height: getProportionateScreenWidth(100), //100 //80
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: Stack(
              children: [
                FadeInImage.assetNetwork(
                  alignment: Alignment.center,
                  placeholder: "assets/images/spinner.gif",
                  image: Request.IMAGE_LOAD_BASE + image,
                  fit: BoxFit.cover,
                  width: 300,
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFF343434).withOpacity(0.4),
                        Color(0xFF343434).withOpacity(0.15),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(15.0),
                    vertical: getProportionateScreenWidth(10),
                  ),
                  child: Text.rich(
                    TextSpan(
                      style: TextStyle(color: Colors.white),
                      children: [
                        TextSpan(
                          text: "$name\n",
                          style: TextStyle(
                            fontSize: getProportionateScreenWidth(18),
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        TextSpan(text: "$category")
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
