import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/bien_immobilier_card.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';

import '../../../size_config.dart';
import 'section_title.dart';

class BienImmobilierHome extends StatefulWidget {
  @override
  _BienImmobilierState createState() => _BienImmobilierState();
}

class _BienImmobilierState extends State<BienImmobilierHome> {
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  @override
  initState() {
    //fetchImmobiliers();
    super.initState();
  }

  // fetchImmobiliers() async {
  //   await bienImmobilierController.fetchBienImmobiliers();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Obx(() {
            return SectionTitle(
                title:
                    "Les Biens Immobiliers(${bienImmobilierController.bienImmobiliersList.length})",
                press: () {
                  Get.off(() => NavigationBarBottom(
                        index: 2,
                      )); //Get.to(ListBienImmobilierScreen());
                });
          }),
        ),
        SizedBox(height: getProportionateScreenWidth(20)),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            child: bienImmobilierController.obx(
                (state) => Row(
                      children: [
                        ...List.generate(
                          state!.length,
                          (index) {
                            if (state.length > 0)
                              return BienImmobilierCard(
                                  bienImmobilier: state[index]);

                            return SizedBox
                                .shrink(); // here by default width and height is 0
                          },
                        ),
                        SizedBox(width: getProportionateScreenWidth(20)),
                      ],
                    ),
                onLoading: Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.5),
                    child: CircularProgressIndicator(
                      color: kPrimaryColor,
                    )),
                onEmpty: Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.15),
                  child: Text("Nous n'avons trouvé aucun élément."),
                ),
                onError: (error) => Padding(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.15),
                      child: Text("Désolé, une erreur est survenue."),
                    )),
          ),
        )
      ],
    );
  }
}
