import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/custom_bottom_sheets.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/screens/details_location/details_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ListLocationCard extends StatefulWidget {
  ListLocationCard({
    Key? key,
    required this.location,
  }) : super(key: key);

  final Location location;
  _ListLocationCardState createState() => _ListLocationCardState();
}

class _ListLocationCardState extends State<ListLocationCard> {
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());
  final UserController userController = Get.put(UserController());
  final LocationController locationController = Get.put(LocationController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          // Get.to(DetailsScreen(),
          //     arguments: LocationDetailsArguments(location: widget.location));
          showBottomSheets(
              context,
              widget.location,
              LocationDetailsArguments(location: widget.location),
              DetailsScreen(),
              "Voulez-vous supprimer cette location ?",
              "Suppression de location",
              locationController);
        },
        child: Row(
          children: [
            SizedBox(
              width: 88,
              child: AspectRatio(
                aspectRatio: 0.88,
                child: Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(8.8)),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(3),
                  ),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/spinner.gif",
                    image:
                        Request.IMAGE_LOAD_BASE + widget.location.bien!.photo!,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 140,
                  child: Text(
                    widget.location.name!,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
                SizedBox(height: 10),
                SizedBox(
                  width: 210,
                  child: Text.rich(
                    TextSpan(
                      text:
                          "${widget.location.userLocataire!.lastName} ${widget.location.userLocataire!.firstName}", //"${location.locationType}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                    ),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    maxLines: 1,
                  ),
                )
              ],
            )
          ],
        ));
  }
}
