import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/screens/home/components/search_field.dart';
import 'package:score_immo/screens/list_location/components/list_location_card.dart';

import '../../../size_config.dart';

// class Body extends StatefulWidget {
//   @override
//   _BodyState createState() => _BodyState();
// }

class Body extends StatelessWidget /*State<Body>*/ {
  final LocationController locationController = Get.put(LocationController());

  // @override
  // void initState() {
  //   fetchLocation();
  //   super.initState();
  // }

  // fetchLocation() {
  //   locationController.fetchLocationWhere();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        SearchField(
          searchController: locationController,
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //height: 384.5,
          child: locationController.obx(
            (state) => Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: ListView.builder(
                itemCount: state!.length,
                itemBuilder: (context, index) => Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: ListLocationCard(location: state[index]),
                ),
              ),
            ),
            onLoading: Center(
              child: CircularProgressIndicator(
                color: kPrimaryColor,
              ),
            ),
            onEmpty: Center(child: Text("Nous n'avons trouvé aucun élément.")),
          ),
        )
      ],
    );
  }
}
