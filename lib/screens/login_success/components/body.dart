import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/size_config.dart';

class Body extends StatefulWidget {
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final UserController userController = Get.find();

  @override
  initState() {
    initAllUserData();
    super.initState();
  }

  initAllUserData() async {
    await userController.fetchUserWhere(false);
    await userController.fetchUserWhere(true);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.04),
          Image.asset(
            "assets/images/success.png",
            height: SizeConfig.screenHeight * 0.4, //40%
          ),
          SizedBox(height: SizeConfig.screenHeight * 0.08),
          Text(
            "Connexion réussie !",
            style: TextStyle(
              fontSize: getProportionateScreenWidth(30),
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          Spacer(),
          SizedBox(
            width: SizeConfig.screenWidth * 0.6,
            child: DefaultButton(
              text: "Accédez à l'accueil",
              press: () {
                Get.offAll(
                    NavigationBarBottom()); //Get.offAll(() => HomeScreen());
              },
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
