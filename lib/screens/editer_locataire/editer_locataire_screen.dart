import 'package:flutter/material.dart';

import 'components/body.dart';

class EditerLocataireScreen extends StatelessWidget {
  static String routeName = "/editer_locataire";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edition d\'un locataire'),
      ),
      body: Body(),
    );
  }
}
