import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/screens/home/components/search_field.dart';

import '../../../size_config.dart';
import 'list_immobilier_card.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  // @override
  // void initState() {
  //   fetchImmobilier();
  //   super.initState();
  // }

  // fetchImmobilier() {
  //   immobilierController.fetchImmobiliers();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        SearchField(
          searchController: immobilierController,
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
            //height: 384.5,
            child: immobilierController.obx(
                (state) => Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: getProportionateScreenWidth(20)),
                      child: ListView.builder(
                        itemCount: state!.length,
                        itemBuilder: (context, index) => Padding(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          child: ListImmobilierCard(immobilier: state[index]),
                        ),
                      ),
                    ),
                onLoading: Center(
                  child: CircularProgressIndicator(color: kPrimaryColor),
                ),
                onEmpty: Center(
                  child: Text("Nous n'avons trouvé aucun élément."),
                )))
      ],
    );
  }
}
