import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/custom_bottom_sheets.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/models/Immobilier.dart';
import 'package:score_immo/screens/details_immobilier/details_screen.dart';

import '../../../constants.dart';

class ListImmobilierCard extends StatefulWidget {
  final Immobilier immobilier;
  ListImmobilierCard({
    Key? key,
    required this.immobilier,
  }) : super(key: key);

  _ListImmobilierCardState createState() => _ListImmobilierCardState();
}

class _ListImmobilierCardState extends State<ListImmobilierCard> {
  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          showBottomSheets(
              context,
              widget.immobilier,
              ImmobilerDetailsArguments(immobilier: widget.immobilier),
              DetailsScreen(),
              "Voulez-vous supprimer cette acquisition ?",
              "Suppression d'acquisition",
              immobilierController);
        },
        child: Row(
          children: [
            SizedBox(
              width: 88,
              child: AspectRatio(
                aspectRatio: 0.88,
                child: Container(
                  padding:
                      EdgeInsets.all(8.8), //getProportionateScreenWidth(10)
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(3), //15
                  ),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/spinner.gif",
                    image: Request.IMAGE_LOAD_BASE + widget.immobilier.photo!,
                    fit: BoxFit.cover,
                    width: 320,
                    key: Key(widget.immobilier.photo!),
                  ),
                ),
              ),
            ),
            SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 140,
                  child: Text(
                    "${widget.immobilier.name}",
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
                SizedBox(height: 10),
                SizedBox(
                  width: 210,
                  child: Text.rich(
                    TextSpan(
                      text: "${widget.immobilier.description}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                    ),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    maxLines: 1,
                  ),
                )
              ],
            )
          ],
        ));
  }
}
