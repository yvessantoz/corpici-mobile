import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:score_immo/components/coustom_bottom_nav_bar.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/list_bottom_card.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/enums.dart';
import 'package:score_immo/screens/creer_immobilier/creer_immobilier_screen.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';
import 'package:score_immo/screens/list_immobilier/components/bottom_sheet.dart';

import 'components/body.dart';

class ListImmobilierScreen extends StatelessWidget {
  ImmobilierController immobilierController = Get.put(ImmobilierController());

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000), () {
      initMethods();
    });
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  //
  initMethods() async {
    bool connected = await Request().isConnected();
    if (connected) {
      await immobilierController.initAll();
    } else {
      SoftDialog().dangerDialog(Get.context!,
          "Désolé vous n'avez pas accès à internet", "Erreur", () {}, () {});
    }
  }

  static String routeName = "/list_immobilier";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: SmartRefresher(
        enablePullDown: true,
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: Body(),
      ),
      // bottomNavigationBar:
      //     CustomBottomNavBar(selectedMenu: MenuState.immobilier),
      // bottomNavigationBar: ListBottomCard(
      //     linkTitle: "Afficher les biens",
      //     linkPress: () {
      //       Get.to(ListBienImmobilierScreen());
      //       //_settingModalBottomSheet(context);
      //     },
      //     buttonPress: () {
      //       Get.to(CreerImmobilierScreen());
      //     },
      //     buttonText: "Ajouter",
      //     total: immobilierController.statsData.value.totalImmobilier ?? 0,
      //     icon: Icons.location_city_outlined),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      centerTitle: false,
      automaticallyImplyLeading: false,
      actions: [
        GestureDetector(
          onTap: () {
            Get.to(CreerImmobilierScreen());
          },
          child: Padding(
            padding: EdgeInsets.only(right: 20),
            child: Icon(
              Icons.add_business_outlined,
              size: 30,
            ),
          ),
        )
      ],
      title: Column(
        children: [
          Text(
            "Mes Acquisitions",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "(${immobilierController.statsData.value.totalImmobilier}) éléments",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}

void _settingModalBottomSheet(context) {
  showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
      ),
      context: context,
      builder: (BuildContext bc) {
        return DetailsModalBottomSheet();
      });
}
