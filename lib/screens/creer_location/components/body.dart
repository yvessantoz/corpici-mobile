import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/size_config.dart';

import 'creer_location_form.dart';

class Body extends StatelessWidget {
  Body({this.pictureController, this.location});
  PictureController? pictureController;
  Location? location;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text("Créer une location", style: headingStyle),
                Text(
                  "Veuillez complétez les champs ci-dessous afin d'enregistrer une location.",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                CreerLocationForm(
                    pictureController: pictureController, location: location),
                SizedBox(height: getProportionateScreenHeight(30)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
