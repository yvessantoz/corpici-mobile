import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/models/Location.dart';

import 'components/body.dart';

class CreerLocationScreen extends StatelessWidget {
  final PictureController pictureController = Get.put(PictureController());
  Location? location;
  var isDialOpen = ValueNotifier<bool>(false);

  static String routeName = "/enregistrer_location";
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        }
        return true;
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('Enregistrer une location'),
          ),
          body: Body(pictureController: pictureController, location: location),
          floatingActionButton: SpeedDial(
            icon: Icons.attach_file_outlined,
            openCloseDial: isDialOpen,
            backgroundColor: Colors.green,
            overlayColor: Colors.transparent,
            overlayOpacity: 0.1,
            spacing: 15,
            spaceBetweenChildren: 15,
            closeManually: true,
            children: [
              SpeedDialChild(
                  child: Icon(
                    Icons.picture_as_pdf_outlined,
                    color: Colors.white,
                  ),
                  label: 'Charger un contrat PDF',
                  backgroundColor: Colors.blue,
                  onTap: () {
                    pictureController.downloadPDF();
                    print('contrat PDF');
                  }),
            ],
          )),
    );
  }
}
