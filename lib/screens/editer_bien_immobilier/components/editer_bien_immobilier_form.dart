import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/components/list_image_card.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/immobilierController.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/screens/list_bien_immobilier/list_bien_immobilier_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class EditerBienImmobilierForm extends StatefulWidget {
  EditerBienImmobilierForm({this.pictureController, this.bienImmobilier});
  final PictureController? pictureController;
  final BienImmobilier? bienImmobilier;

  @override
  _EditerBienImmobilierFormState createState() =>
      _EditerBienImmobilierFormState();
}

class _EditerBienImmobilierFormState extends State<EditerBienImmobilierForm> {
  final BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  final ImmobilierController immobilierController =
      Get.put(ImmobilierController());

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? name;
  String? adresse;
  String? superficie;
  String? nbPiece;
  String? nbChambre;
  String? nbSalleBain;
  String? description;
  String selectedImmobilier = "";
  String selectedType = "";
  DateTime _selectedDate = new DateTime.now();
  List<String> bienType = BienImmobilier.typeList;
  String buttonText = "Editer";

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  void initState() {
    //fetchImmobilier();
    super.initState();
  }

  fetchImmobilier() {
    immobilierController.fetchImmobiliers();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildAdresseFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildSuperficieFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildPieceFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildChambreFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildSalleDeBainFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDescriptionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeImmobilierSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildImmobilierSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          previewImages(),
          SizedBox(height: getProportionateScreenHeight(15)),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  sendDataToServer();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Name
  TextFormField buildNameFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.name}",
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNameNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNameNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Libellé",
          hintText: "Libellé",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .location_city_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Adresse
  TextFormField buildAdresseFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.adresse}",
      onSaved: (newValue) => adresse = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kAdresseNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kAdresseNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Adresse",
          hintText: "Adresse",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .location_on_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Superifice
  TextFormField buildSuperficieFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.superficie}",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => superficie = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPaysNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kPaysNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Superfice",
          hintText: "Superfice",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .map_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Nombre de pièce
  TextFormField buildPieceFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.nbPiece}",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => nbPiece = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNbPieceNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNbPieceNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Nombre de pièce",
          hintText: "Nombre de pièce",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .map_sharp) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Nombre de chambre
  TextFormField buildChambreFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.nbChambre}",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => nbChambre = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNbChambreNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNbChambreNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Nombre de chambre",
          hintText: "Nombre de chambre",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .streetview_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Nombre de salle de bain
  TextFormField buildSalleDeBainFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.nbSalleBain}",
      maxLines: 1,
      keyboardType: TextInputType.number,
      onSaved: (newValue) => nbSalleBain = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNbSalleBainNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNbSalleBainNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Nombre de salle de bain",
          hintText: "Nombre de salle de bain",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.wash_outlined)),
    );
  }

  // Description
  TextFormField buildDescriptionFormField() {
    return TextFormField(
      initialValue: "${widget.bienImmobilier!.description}",
      keyboardType: TextInputType.multiline,
      minLines: 5,
      maxLines: 5,
      onSaved: (newValue) => description = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kDescriptionNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kDescriptionNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Description",
        hintText: "Décrivez le bien en quelques mots",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
      ),
    );
  }

  // Type de bien
  buildTypeImmobilierSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Type de bien',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: bienType
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Type de bien") {
          return 'Veuillez sélectionnez un type de bien.';
        }
      },
      onChanged: (value) {
        setState(() {
          selectedType = value.toString();
        });
      },
      onSaved: (value) {
        selectedType = value.toString();
      },
      value: "${widget.bienImmobilier!.bienType}",
    ));
  }

  // Immobilier
  buildImmobilierSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Immobilier',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: immobilierController.immobiliersList
          .map((item) => DropdownMenuItem<String>(
                value: item.name,
                child: Text(
                  "${item.name}",
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return 'Veuillez sélectionnez un immobilier.';
        }
      },
      onChanged: (value) {
        setState(() {
          selectedImmobilier = value.toString();
        });
      },
      onSaved: (value) {
        selectedImmobilier = value.toString();
      },
      value:
          "${immobilierController.fetchNameByID(widget.bienImmobilier!.immobilier!)}",
    ));
  }

  void getDateFromUser() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedDate = _pickedDate;
      });
    }
  }

  // Prévisualiser les images sélectionnées
  previewImages() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        child: widget.pictureController!.obx((state) {
          var files = widget.pictureController!.fileList;
          var photo = XFile(widget.pictureController!.selectedImagePath.value);

          return Row(
            children: [
              if (photo.path.isNotEmpty) ListImageCard(image: photo),
              ...List.generate(
                files.length,
                (index) {
                  if (files.length > 0)
                    return ListImageCard(image: files[index]);
                  return SizedBox.shrink();
                  // here by default width and height is 0
                },
              ),
              SizedBox(width: getProportionateScreenWidth(20)),
            ],
          );
        }, onLoading: CircularProgressIndicator(color: kPrimaryColor)),
      ),
    );
  }

  //
  sendDataToServer() async {
    setState(() {
      buttonText = verificationText;
    });
    var bienImmo = widget.bienImmobilier!;
    // Sauvegarde des anciens fichiers
    var phot = bienImmo.photo;
    var phos = bienImmo.galerie;

    bienImmo.name = name;
    bienImmo.bienType = selectedType;
    bienImmo.adresse = adresse;
    bienImmo.superficie = superficie;
    bienImmo.nbChambre = nbChambre;
    bienImmo.description = description;
    bienImmo.nbPiece = nbPiece;
    bienImmo.nbSalleBain = nbSalleBain;
    bienImmo.immobilier =
        immobilierController.fetchIDByName(selectedImmobilier);

    bienImmo.photo = null;
    bienImmo.galerie = null;

    List<XFile> galerie = widget.pictureController!.filesSelected();
    String pictureSelected = widget.pictureController!.pictureSelected();
    PickedFile photo = PickedFile(pictureSelected);

    var result = await bienImmobilierController.editerBien(
        widget.bienImmobilier!,
        pictureSelected.isNotEmpty ? photo : null,
        galerie.isNotEmpty ? galerie : null);

    if (result['status']) {
      bienImmo.photo = phot;
      bienImmo.galerie = phos;
      SoftDialog().successDialog(context, "Modification effectuée", "Succès",
          () {
        Get.off(() => NavigationBarBottom(
              index: 2,
            )); //Get.off(ListBienImmobilierScreen());
      }, () {
        //Get.off(ListBienImmobilierScreen());
        Get.off(() => NavigationBarBottom(
              index: 2,
            ));
      });
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
