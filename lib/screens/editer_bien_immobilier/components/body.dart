import 'package:flutter/material.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/models/Bien.dart';
import 'package:score_immo/size_config.dart';

import 'editer_bien_immobilier_form.dart';

class Body extends StatelessWidget {
  Body({this.pictureController, this.bienImmobilier});

  final PictureController? pictureController;
  final BienImmobilier? bienImmobilier;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text("Editer un bien", style: headingStyle),
                Text(
                  "Veuillez complétez les champs ci-dessous afin d'éditer votre bien immobilier.",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                EditerBienImmobilierForm(
                    pictureController: pictureController,
                    bienImmobilier: bienImmobilier),
                SizedBox(height: getProportionateScreenHeight(30)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
