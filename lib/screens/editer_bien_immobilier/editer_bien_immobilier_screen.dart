import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/models/Bien.dart';

import 'components/body.dart';

class EditerBienImmobilierScreen extends StatefulWidget {
  @override
  _EditerBienImmobilierScreenState createState() =>
      _EditerBienImmobilierScreenState();
}

class _EditerBienImmobilierScreenState
    extends State<EditerBienImmobilierScreen> {
  final PictureController pictureController = Get.put(PictureController());
  BienImmobilier? bienImmobilier;
  var isDialOpen = ValueNotifier<bool>(false);

  static String routeName = "/editer_bien_immobilier";
  @override
  Widget build(BuildContext context) {
    final args = ModalRoute.of(context)!.settings.arguments as BienImmobilier;
    bienImmobilier = args;

    return WillPopScope(
      onWillPop: () async {
        if (isDialOpen.value) {
          isDialOpen.value = false;
          return false;
        }
        return true;
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text('Editer un bien immobilier'),
          ),
          body: Body(
              pictureController: pictureController,
              bienImmobilier: bienImmobilier),
          floatingActionButton: SpeedDial(
            icon: Icons.attach_file_outlined,
            openCloseDial: isDialOpen,
            backgroundColor: Colors.green,
            overlayColor: Colors.transparent,
            overlayOpacity: 0.1,
            spacing: 15,
            spaceBetweenChildren: 15,
            closeManually: true,
            children: [
              SpeedDialChild(
                  child: Icon(
                    Icons.image_outlined,
                    color: Colors.white,
                  ),
                  label: 'Galerie',
                  backgroundColor: Colors.blue,
                  onTap: () {
                    pictureController.takeMultiplePictures();
                    print('Galerie');
                  }),
              SpeedDialChild(
                  child: Icon(
                    Icons.preview_outlined,
                    color: Colors.white,
                  ),
                  label: 'Photo',
                  backgroundColor: Colors.pinkAccent,
                  onTap: () {
                    pictureController.takePicture(ImageSource.gallery);
                    print('Photo');
                  }),
            ],
          )),
    );
  }
}
