import 'dart:io';

import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:intl/intl.dart';
import 'package:score_immo/components/pdf_card.dart';
import 'package:score_immo/controller/bienImmobilierController.dart';
import 'package:score_immo/controller/locationController.dart';
import 'package:score_immo/controller/pictureController.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/models/Location.dart';
import 'package:score_immo/screens/list_location/list_location_screen.dart';
import 'package:score_immo/utils/utils.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class EditerLocationForm extends StatefulWidget {
  EditerLocationForm({this.pictureController, this.location});
  PictureController? pictureController;
  Location? location;

  @override
  _EditerLocationFormState createState() => _EditerLocationFormState();
}

class _EditerLocationFormState extends State<EditerLocationForm> {
  LocationController locationController = Get.put(LocationController());
  UserController userController = Get.put(UserController());
  BienImmobilierController bienImmobilierController =
      Get.put(BienImmobilierController());

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? name;
  String? dureeBail;
  String? debutBail;
  String? finBail;
  String? isRenouvellable;
  String? recurrencePaiement;
  String? modesPaiement;
  String? montant;
  String? isTva;
  String? tauxTva;

  DateTime _selectedDateDebutBail = new DateTime.now();
  DateTime _selectedDateFinBail = new DateTime.now();

  String selectedRecurrencePaiement = "Recurrence de paiement";
  List<String> recurrencePaiementList = Location.recurrencePaiementList;

  String selectedModesPaiement = "Mode de paiement";
  List<String> modesPaiementList = Location.modesPaiementList;

  String selectedRenouvellable = "Est-ce renouvellable ?";
  List<String> isRenouvellableList = Location.renouvellableList;

  String selectedTva = "Y'a-t-il une Tva ?";
  List<String> isTvaList = Location.tvaList;

  String selectedLocataire = "Choisissez un locataire";
  // List<String> locataireList = Location.locataireList;

  String selectedTypeLocation = "";
  String selectedBien = "";
  String selectedEtatContrat = "";
  String buttonText = "Editer";

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDureeFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildMontantFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTauxTvaFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDateDebutBailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDateFinBailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIsRenouvellableSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildReccurencePaiementSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIsTvaSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildModePaiementSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildLocataireSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBienSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeLocationSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeEtatSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          previewFile(),
          SizedBox(height: getProportionateScreenHeight(15)),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  sendDataToServer();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Name
  TextFormField buildNameFormField() {
    return TextFormField(
      initialValue: "${widget.location!.name}",
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNameNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kNameNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Libellé",
          hintText: "Libellé",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .local_hotel_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Duree du bail
  TextFormField buildDureeFormField() {
    return TextFormField(
      initialValue: "${widget.location!.dureeBail}",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => dureeBail = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kDureeBailNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kDureeBailNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Durée du bail par an",
          hintText: "Durée du bail par an",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.timer_rounded)),
    );
  }

  // Montant
  TextFormField buildMontantFormField() {
    return TextFormField(
      initialValue: "${widget.location!.montant}",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => montant = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kMontantNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kMontantNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Montant",
        hintText: "Montant",
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.money_outlined),
      ),
    );
  }

  // Taux TVA
  TextFormField buildTauxTvaFormField() {
    return TextFormField(
      initialValue: "${widget.location!.tauxTva}",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => tauxTva = newValue,
      // onChanged: (value) {
      //   if (value.isNotEmpty) {
      //     removeError(error: kTauxTvaNullError);
      //   }
      //   return null;
      // },
      // validator: (value) {
      //   if (value!.isEmpty) {
      //     addError(error: kTauxTvaNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
          labelText: "Taux Tva",
          hintText: "Taux Tva",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons
              .rate_review_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          ),
    );
  }

  // Recurrence de paiement
  buildReccurencePaiementSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Recurrence de paiement',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: recurrencePaiementList
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == 'Recurrence de paiement') {
          return "Recurrence de paiement";
        }
      },
      onChanged: (value) {
        selectedRecurrencePaiement = value.toString();
      },
      onSaved: (value) {
        selectedRecurrencePaiement = value.toString();
      },
      value: widget.location!.recurencePaiement,
    ));
  }

  // Mode de paiement
  buildModePaiementSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Modes de paiement',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: modesPaiementList
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == 'Modes de paiement') {
          return "Modes de paiement";
        }
      },
      onChanged: (value) {
        selectedModesPaiement = value.toString();
      },
      onSaved: (value) {
        selectedModesPaiement = value.toString();
      },
      value: widget.location!.modesPaiement,
    ));
  }

  // Locataires
  buildLocataireSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Choississez un locataire',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: userController.userList
          .map((item) => DropdownMenuItem<String>(
                value: item.lastName! + ' ' + item.firstName!,
                child: Text(
                  item.lastName! + ' ' + item.firstName!,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return "Choississez un locataire";
        }
      },
      onChanged: (value) {
        selectedLocataire = value.toString();
      },
      onSaved: (value) {
        selectedLocataire = value.toString();
      },
      value:
          "${widget.location!.userLocataire!.lastName} ${widget.location!.userLocataire!.firstName}", //userController.fetchNameByID(widget.location!.locataire!)
    ));
  }

  // Bien
  buildBienSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Choississez un bien',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: bienImmobilierController.bienImmobiliersList
          .map((item) => DropdownMenuItem<String>(
                value: item.name,
                child: Text(
                  item.name!,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null) {
          return "Choississez un bien";
        }
      },
      onChanged: (value) {
        selectedBien = value.toString();
      },
      onSaved: (value) {
        selectedBien = value.toString();
      },
      value:
          "${widget.location!.bien!.name}", //bienImmobilierController.fetchNameByID(widget.location!.bienId!)
    ));
  }

  // Est-il renouvellable ?
  buildIsRenouvellableSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Est-il renouvellable ?',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: isRenouvellableList
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Est-ce renouvellable ?") {
          return "Est-il renouvellable ?";
        }
      },
      onChanged: (value) {
        selectedRenouvellable = value.toString();
      },
      onSaved: (value) {
        selectedRenouvellable = value.toString();
      },
      value: widget.location!.isRenouvellable == "0" ? "NON" : "OUI",
    ));
  }

  // Formulaire de la Date de début du bail
  TextFormField buildDateDebutBailFormField() {
    return TextFormField(
      initialValue: "${replace("-", "/", widget.location!.debutBail!)}",
      maxLines: 1,
      onSaved: (newValue) => debutBail = newValue,
      // onChanged: (value) {
      //   if (value.isNotEmpty) {
      //     removeError(error: kDateDebutBailNullError);
      //   }
      //   return null;
      // },
      // validator: (value) {
      //   if (value!.isEmpty) {
      //     addError(error: kDateDebutBailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
          labelText: "Début du bail",
          hintText: DateFormat.yMd('fr').format(_selectedDateDebutBail),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
            icon: Icon(Icons.calendar_today_outlined),
            onPressed: () {
              getDateStart();
            },
          )),
    );
  }

  // Formulaire de la Date de fin du bail
  TextFormField buildDateFinBailFormField() {
    return TextFormField(
      initialValue: "${replace("-", "/", widget.location!.finBail!)}",
      maxLines: 1,
      onSaved: (newValue) => finBail = newValue,
      // onChanged: (value) {
      //   if (value.isNotEmpty) {
      //     removeError(error: kDateFinBailNullError);
      //   }
      //   return null;
      // },
      // validator: (value) {
      //   if (value!.isEmpty) {
      //     addError(error: kDateFinBailNullError);
      //     return "";
      //   }
      //   return null;
      // },
      decoration: InputDecoration(
          labelText: "Fin du bail",
          hintText: DateFormat.yMd('fr').format(_selectedDateDebutBail),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
            icon: Icon(Icons.calendar_today_outlined),
            onPressed: () {
              getDateEnd();
            },
          )),
    );
  }

  // Est-ce une TVA ?
  buildIsTvaSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Y\'a-t-il une Tva ?',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: isTvaList
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Y'a-t-il une TVA ?") {
          return "Y'a-t-il une Tva ?";
        }
      },
      onChanged: (value) {
        selectedTva = value.toString();
      },
      onSaved: (value) {
        selectedTva = value.toString();
      },
      value: widget.location!.tauxTva == "0" ? "NON" : "OUI",
    ));
  }

  // Type de location
  buildTypeLocationSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Type de location',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: Location.typeList
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Type de location") {
          return "Type de location";
        }
      },
      onChanged: (value) {
        selectedTypeLocation = value.toString();
      },
      onSaved: (value) {
        selectedTypeLocation = value.toString();
      },
      value: widget.location!.locationType,
    ));
  }

  // Etat du contrat
  buildTypeEtatSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Etat du contrat',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: Location.etatList
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Etat du contrat") {
          return "Etat du contrat";
        }
      },
      onChanged: (value) {
        selectedEtatContrat = value.toString();
      },
      onSaved: (value) {
        selectedEtatContrat = value.toString();
      },
      value: widget.location!.etat,
    ));
  }

  // Debut du bail
  void getDateStart() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedDateDebutBail,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedDateDebutBail = _pickedDate;
      });
    }
  }

  // Fin du bail
  void getDateEnd() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedDateFinBail,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedDateFinBail = _pickedDate;
      });
    }
  }

  //
  previewFile() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Container(
        child: widget.pictureController!.obx((state) {
          bool pdfPath =
              widget.pictureController!.selectedPDFPath.value.isNotEmpty;
          var pdf = pdfPath
              ? widget.pictureController!.selectedPDFPath.value
              : "${widget.location!.contratBail}";

          return Row(
            children: [
              pdf.isNotEmpty
                  ? GestureDetector(
                      onTap: () {
                        widget.location!.contratBail != null
                            ? locationController
                                .download(widget.location!.contratBail!)
                            // ignore: unnecessary_statements
                            : null;
                      },
                      child: PDFCard(image: "assets/images/pdf.png"),
                    )
                  : SizedBox.shrink(),
              // here by default width and height is 0
              SizedBox(width: getProportionateScreenWidth(20)),
            ],
          );
        },
            onLoading: Center(
              child: Center(
                child: Center(
                  child: CircularProgressIndicator(color: kPrimaryColor),
                ),
              ),
            )),
      ),
    );
  }

  //
  sendDataToServer() async {
    setState(() {
      buttonText = verificationText;
    });
    Location location = widget.location!;
    //location.contratBail = ""
    location.debutBail = formatDate(_selectedDateDebutBail, "yyy-MM-dd");
    location.dureeBail = dureeBail;
    location.finBail = formatDate(_selectedDateFinBail, "yyy-MM-dd");
    location.isRenouvellable = selectedRenouvellable == "OUI" ? "1" : "0";
    location.isTva = selectedTva == "OUI" ? "1" : "0";
    location.locataire = userController.fetchIDByName(selectedLocataire);
    location.locationType = selectedTypeLocation;
    location.bienId = bienImmobilierController.fetchIDByName(selectedBien);
    location.montant = montant;
    location.name = name;
    location.etat = selectedEtatContrat;
    location.recurencePaiement = selectedRecurrencePaiement;
    location.modesPaiement = selectedModesPaiement;
    location.tauxTva = selectedTva == "OUI" ? tauxTva : null;

    String contrat = widget.pictureController!.selectedPDFPath.value;

    var result = await locationController.updateLocation(
        location, contrat.isNotEmpty ? File(contrat) : null);
    if (result['status']) {
      SoftDialog().successDialog(context, "Mise à jour effectuée", "Succès",
          () {
        Get.off(() => NavigationBarBottom(
              index: 3,
            )); //Get.off(() => LocationScreen());
      }, () {
        Get.off(() => NavigationBarBottom(
              index: 3,
            )); //Get.off(() => LocationScreen());
      });
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
