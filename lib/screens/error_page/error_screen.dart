import 'package:flutter/material.dart';
import 'package:score_immo/size_config.dart';

import 'components/body.dart';

class ErrorScreen extends StatelessWidget {
  static String routeName = "/error";
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        leading: SizedBox(),
        title: Text("Erreur"),
      ),
      body: Body(),
    );
  }
}
