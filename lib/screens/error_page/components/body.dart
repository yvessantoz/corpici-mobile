import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/controller/request.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/size_config.dart';

class Body extends StatelessWidget {
  checkNextWork() async {
    bool connected = await Request().isConnected();
    if (connected) {
      Get.offAll(() => NavigationBarBottom());
    }
    return;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.04),
          Image.asset(
            "assets/images/network-error.png",
            height: SizeConfig.screenHeight * 0.4, //40%
          ),
          SizedBox(height: SizeConfig.screenHeight * 0.08),
          Center(
            child: Text(
              "Activez votre connexion internet !",
              style: TextStyle(
                fontSize: getProportionateScreenWidth(17),
                fontWeight: FontWeight.normal,
                color: Colors.black,
              ),
            ),
          ),
          Spacer(),
          SizedBox(
            width: SizeConfig.screenWidth * 0.6,
            child: DefaultButton(
              text: "Réessayez",
              press: () {
                checkNextWork(); //Get.offAll(() => HomeScreen());
              },
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
