import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:score_immo/constants.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/screens/home/components/search_field.dart';
import 'package:score_immo/screens/list_tous_locataires/components/list_tous_locataires_card.dart';

import '../../../size_config.dart';

// class Body extends StatefulWidget {
//   @override
//   _BodyState createState() => _BodyState();
// }

class Body extends StatelessWidget /*State<Body>*/ {
  UserController userController = Get.put(UserController());

  // @override
  // void initState() {
  //   fetchUsers();
  //   super.initState();
  // }

  // Lance la recherche des locataires pendant l'initialisation de la vue
  // fetchUsers() {
  //   userController.fetchUserWhere();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 15,
        ),
        SearchField(
          searchController: userController,
        ),
        SizedBox(
          height: 15,
        ),
        Expanded(
          //height: 384.5,
          child: userController.obx(
              (state) => Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(20)),
                    child: ListView.builder(
                      itemCount: userController.usersList.length,
                      itemBuilder: (context, index) => Padding(
                        padding: EdgeInsets.symmetric(vertical: 10),
                        child: ListTousLocataireCard(
                            locataire: userController.usersList[index]),
                      ),
                    ),
                  ),
              onLoading: Center(
                child: CircularProgressIndicator(color: kPrimaryColor),
              ),
              onEmpty:
                  Center(child: Text("Nous n'avons trouvé aucun élément.")),
              onError: (error) => Center(
                    child: Text(error.toString()),
                  )),
        )
      ],
    );
  }
}
