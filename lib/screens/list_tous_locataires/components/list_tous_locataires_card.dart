import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/details_locataire/details_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ListTousLocataireCard extends StatelessWidget {
  const ListTousLocataireCard({
    Key? key,
    required this.locataire,
  }) : super(key: key);

  final User locataire;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Get.to(DetailsScreen(),
              arguments:
                  LocataireDetailsArguments(locataire: locataire, all: true));
        },
        child: Row(
          children: [
            SizedBox(
              width: 88,
              child: AspectRatio(
                aspectRatio: 0.88,
                child: Container(
                  padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                  decoration: BoxDecoration(
                    color: Color(0xFFF5F6F9),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Image.asset(locataire.avatar!),
                ),
              ),
            ),
            SizedBox(width: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  width: 140,
                  child: Text(
                    locataire.lastName! + " " + locataire.firstName!,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                  ),
                ),
                SizedBox(height: 10),
                SizedBox(
                  width: 210,
                  child: Text.rich(
                    TextSpan(
                      text: "${locataire.mobile}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                    ),
                    overflow: TextOverflow.ellipsis,
                    softWrap: false,
                    maxLines: 1,
                  ),
                )
              ],
            )
          ],
        ));
  }
}
