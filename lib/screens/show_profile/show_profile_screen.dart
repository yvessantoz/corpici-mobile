import 'package:flutter/material.dart';
import 'package:score_immo/models/User.dart';

import 'components/body.dart';
import 'components/custom_app_bar.dart';

class ShowProfileScreen extends StatelessWidget {
  static String routeName = "/show_profile";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F6F9),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(AppBar().preferredSize.height),
        child: CustomAppBar(),
      ),
      body: Body(), //user: agrs.user
    );
  }
}

class ShowProfileArguments {
  final User user;

  ShowProfileArguments({required this.user});
}
