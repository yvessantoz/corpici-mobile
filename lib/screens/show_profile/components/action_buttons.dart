import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/rounded_icon_btn.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/screens/reset_password/reset_password_screen.dart';

import '../../../size_config.dart';

class ActionButtons extends StatelessWidget {
  ActionButtons({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;
  final UserController userController = Get.put(UserController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.home_outlined,
              press: () {
                Get.off(() => NavigationBarBottom(
                      index: 0,
                    )); //Get.to(HomeScreen());
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.lock_outline_rounded,
              press: () {
                Get.to(ResetPasswordScreen());
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              iconColor: Colors.orange,
              icon: Icons.logout_outlined,
              press: () {
                userController.logOut();
              },
            ),
            SizedBox(width: getProportionateScreenWidth(20)),
            RoundedIconBtn(
              showShadow: true,
              icon: Icons.delete_outline_outlined,
              iconColor: Colors.red,
              press: () {
                SoftDialog(btnOkOnPress: "OUI", btnCancelText: "NON")
                    .warningDialog(
                        context,
                        "Voulez-vous supprimer votre compte ?",
                        "Attention", () {
                  userController.delete(user);
                }, () {});
              },
            ),
          ],
        ),
      ),
    );
  }
}
