import 'package:flutter/material.dart';
import 'package:score_immo/models/User.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ShowProfileImages extends StatefulWidget {
  const ShowProfileImages({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  _ShowProfileImagesState createState() => _ShowProfileImagesState();
}

class _ShowProfileImagesState extends State<ShowProfileImages> {
  int selectedImage = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(238),
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: widget.user.id.toString(),
              child: Image.asset("${widget.user.avatar}"),
            ),
          ),
        ),
        // SizedBox(height: getProportionateScreenWidth(20)),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   children: [
        //     ...List.generate(widget.user.avatar!.length,
        //         (index) => buildSmallProductPreview(index)),
        //   ],
        // )
      ],
    );
  }

  GestureDetector buildSmallProductPreview(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedImage = index;
        });
      },
      child: AnimatedContainer(
        duration: defaultDuration,
        margin: EdgeInsets.only(right: 15),
        padding: EdgeInsets.all(8),
        height: getProportionateScreenWidth(48),
        width: getProportionateScreenWidth(48),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: kPrimaryColor.withOpacity(selectedImage == index ? 1 : 0)),
        ),
        child: Image.asset("${widget.user.avatar}"),
      ),
    );
  }
}
