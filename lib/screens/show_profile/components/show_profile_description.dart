import 'package:flutter/material.dart';
import 'package:score_immo/models/User.dart';

import '../../../size_config.dart';

class ShowProfileDescription extends StatelessWidget {
  const ShowProfileDescription({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "${user.lastName} ${user.firstName}",
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(34),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.person_outline_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  user.pseudo ?? "Aucun",
                  maxLines: 3,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Téléphone",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.phone_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  user.mobile ?? "Aucun",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Email",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.email_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  user.email ?? "Aucun",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Date et lieu de naissance",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  "${user.dateNaissance} à ${user.lieuNaissance}",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Date d'expiration de la pièce",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(64),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                Icons.calendar_today_outlined,
                size: 18,
                color: Colors.black45,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Text(
                  user.expirationPiece ?? "Aucun",
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Type de pièce",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.typePiece ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Numéro ${user.typePiece}",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.numeroPiece ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Statut professionnel",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.statutProfessionnel ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Fonction",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.fonction ?? "Aucune",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Domaine d'activité",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.domaineActivite ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Qualification",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.qualification ?? "Aucune",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Revenu mensuel (Fcfa)",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.revenuMensuel ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Numéro de compte contribuable",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.numeroCompteContribuable ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Numéro du registre de commerce",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.numeroRegistreCommerce ?? "Aucun",
          ),
        ),
        SizedBox(height: 20),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            "Type de compte",
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
        ),
        SizedBox(height: 3),
        Padding(
          padding: EdgeInsets.only(
            left: getProportionateScreenWidth(20),
            right: getProportionateScreenWidth(30),
          ),
          child: Text(
            user.userType ?? "Aucun",
          ),
        ),
        SizedBox(height: user.userType == "AGENCE" ? 20 : 0),
        user.userType == "AGENCE"
            ? Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Text(
                  "Nom Agence",
                  style: TextStyle(fontSize: 18, color: Colors.black),
                ),
              )
            : Container(),
        SizedBox(height: user.userType == "AGENCE" ? 3 : 0),
        user.userType == "AGENCE"
            ? Padding(
                padding: EdgeInsets.only(
                  left: getProportionateScreenWidth(20),
                  right: getProportionateScreenWidth(30),
                ),
                child: Text(
                  user.agenceName ?? "Aucun",
                ),
              )
            : Container(),
        SizedBox(height: user.userType == "AGENCE" ? 20 : 0),
        user.userType == "AGENCE"
            ? Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Text(
                  "Contact Agence",
                  style: TextStyle(fontSize: 18, color: Colors.black),
                ),
              )
            : Container(),
        SizedBox(height: user.userType == "AGENCE" ? 3 : 0),
        user.userType == "AGENCE"
            ? Padding(
                padding: EdgeInsets.only(
                  left: getProportionateScreenWidth(20),
                  right: getProportionateScreenWidth(30),
                ),
                child: Text(
                  user.agencePhone ?? "Aucun",
                ),
              )
            : Container(),
      ],
    );
  }
}
