import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/edit_profile/edit_profile_screen.dart';
import 'package:score_immo/screens/show_profile/components/show_profile_images.dart';
import 'package:score_immo/size_config.dart';

import 'action_buttons.dart';
import 'show_profile_description.dart';
import 'top_rounded_container.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  User user = User();
  final UserController userController = Get.put(UserController());

  @override
  void initState() {
    fetchUserData();
    super.initState();
  }

  fetchUserData() async {
    var futurUser = await userController.getUser("user");
    setState(() {
      user = futurUser;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ShowProfileImages(user: this.user),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ShowProfileDescription(
                user: this.user,
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ActionButtons(user: this.user),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.screenWidth * 0.15,
                          right: SizeConfig.screenWidth * 0.15,
                          bottom: getProportionateScreenWidth(40),
                          top: getProportionateScreenWidth(15),
                        ),
                        child: DefaultButton(
                          text: "Editer",
                          press: () {
                            Get.to(EditProfileScreen(), arguments: user);
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
