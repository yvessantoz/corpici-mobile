import 'package:flutter/material.dart';

import 'components/body.dart';

class CreerLocataireScreen extends StatelessWidget {
  static String routeName = "/creer_locataire";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Création d\'un locataire'),
      ),
      body: Body(),
    );
  }
}
