import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/dialog.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:intl/intl.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/custom_navigation_bar.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/list_locataire/list_locataire_screen.dart';
import 'package:score_immo/utils/utils.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CreerLocataireForm extends StatefulWidget {
  @override
  _CreerLocataireFormState createState() => _CreerLocataireFormState();
}

class _CreerLocataireFormState extends State<CreerLocataireForm> {
  final UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];
  String? firstName;
  String? lastName;
  String? birthPlace;
  String? typeID;
  String? iD;
  String? expirationID;
  String? birthDate;
  String? mobile;
  String? email;
  String selectedTypeUser = "";
  String selectedTypeID = "";
  String buttonText = "Ajouter";

  DateTime _selectedBirthDate = new DateTime.now();
  DateTime _selectedExpirationDate = new DateTime.now();

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildFirstNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildLastNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBirthDateFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBirthPlaceFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildEmailFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildMobileFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIDFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildExpirationIDFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeIDSelectInput(),
          //SizedBox(height: getProportionateScreenHeight(30)),
          //buildTypeUserSelectInput(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: buttonText,
            press: () {
              if (buttonText != verificationText) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  sendToserver();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Adresse Ligne
  buildBirthPlaceFormField() {
    return Container(
        height: 60,
        child: TextFormField(
          onSaved: (newValue) => birthPlace = newValue,
          onChanged: (value) {
            if (value.isNotEmpty) {
              removeError(error: kBirthPlaceNullError);
            }
            return null;
          },
          validator: (value) {
            if (value!.isEmpty) {
              addError(error: kBirthPlaceNullError);
              return "";
            }
            return null;
          },
          decoration: InputDecoration(
              labelText: "Lieu de naissance",
              hintText: "Lieu de naissance",
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: Icon(Icons.my_location_outlined)
              //CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
              ),
        ));
  }

  // BirthDate
  buildBirthDateFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        readOnly: true,
        keyboardType: TextInputType.datetime,
        onSaved: (newValue) => birthDate = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kBirthDateNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kBirthDateNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
            labelText: "Date de naissance",
            hintText: DateFormat.yMd("fr").format(_selectedBirthDate),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: IconButton(
              icon: Icon(Icons.calendar_today_outlined),
              onPressed: () {
                getBirthDate();
              },
            )),
      ),
    );
  }

  // Nom de famille
  buildLastNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => lastName = newValue,
        decoration: InputDecoration(
            labelText: "Prénom(s)",
            hintText: "Prénom(s)",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons.person_outline_rounded)),
      ),
    );
  }

  // Prenom
  buildFirstNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => firstName = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kNamelNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kNamelNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Nom",
            hintText: "Nom",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .person_outline_rounded) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Numéro de la pièce CNI/PASSPORT/CARTE DE SEJOUR/...
  buildIDFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => iD = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kIDNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kIDNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Pièce d'identité N°",
            hintText: "Pièce d'identité N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .card_membership_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Date d'expiration de la pièce
  buildExpirationIDFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        readOnly: true,
        keyboardType: TextInputType.datetime,
        onSaved: (newValue) => expirationID = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kExpirationIDNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kExpirationIDNullError);
        //     return null;
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
            labelText: "Date d'expiration de la pièce",
            hintText: DateFormat.yMd("fr").format(_selectedExpirationDate),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: IconButton(
              icon: Icon(Icons.calendar_today_outlined),
              onPressed: () {
                getExpirationDate();
              },
            ) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Adresse email
  buildEmailFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        onSaved: (newValue) => email = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kFonctionNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kFonctionNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Adresse email",
            hintText: "Adresse email",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .email_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Contact
  buildMobileFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.number,
        onSaved: (newValue) => mobile = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kPhoneNumberNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kPhoneNumberNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Contact",
            hintText: "Contact",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .phone_android_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/calendar.svg"),
            ),
      ),
    );
  }

  // Type de pièce
  buildTypeIDSelectInput() {
    return Container(
      child: DropdownButtonFormField2(
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          isExpanded: true,
          hint: const Text(
            'Type de pièce d\'identité',
            style: TextStyle(fontSize: 14),
          ),
          icon: const Icon(
            Icons.arrow_drop_down,
            color: Colors.black45,
          ),
          iconSize: 30,
          buttonHeight: 60,
          buttonPadding: const EdgeInsets.only(left: 20, right: 10),
          dropdownDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          items: typeIDItems
              .map((item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ))
              .toList(),
          validator: (value) {
            if (value == null || value == "Type de pièce") {
              return 'Veuillez sélectionnez un type de pièce.';
            }
          },
          onChanged: (value) {},
          onSaved: (value) {
            selectedTypeID = value.toString();
          },
          dropdownMaxHeight: 210.0),
    );
  }

  // Type d'utilisateur
  buildTypeUserSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Type d\'utilisateur',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: typeUserItems
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Type d'utilisateur") {
          return 'Veuillez sélectionnez un type d\'utilisateur.';
        }
      },
      onChanged: (value) {
        setState(() {
          selectedTypeUser = value.toString();
        });
      },
      onSaved: (value) {
        selectedTypeUser = value.toString();
      },
    ));
  }

  // Date d'expiration de la piece
  void getExpirationDate() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedExpirationDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedExpirationDate = _pickedDate;
      });
    }
  }

  // Date d'anniversaire
  void getBirthDate() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedBirthDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedBirthDate = _pickedDate;
      });
    }
  }

  // Envoi des données au serveur
  sendToserver() async {
    setState(() {
      buttonText = verificationText;
    });
    User user = User();
    user.firstName = firstName;
    user.lastName = lastName;
    user.email = email;
    user.dateNaissance = formatDate(_selectedBirthDate, 'yyyy-MM-dd');
    user.lieuNaissance = birthPlace;
    user.expirationPiece = formatDate(_selectedExpirationDate, 'yyyy-MM-dd');
    user.typePiece = selectedTypeID;
    user.userType = selectedTypeUser;
    user.mobile = mobile;
    user.numeroPiece = iD;
    user.password = "confirmed";
    user.passwordConfirmation = "confirmed";

    Map<dynamic, dynamic> result = await userController.simpleSave(user);
    if (result['status']) {
      SoftDialog(btnOkOnPress: "OK").successDialog(
          context,
          "Le locataire: ${user.lastName} ${user.firstName} a été enregistré.",
          "Succès", () {
        Get.off(() => NavigationBarBottom(
              index: 4,
            )); //Get.off(() => ListLocataireScreen());
      }, () {
        Get.off(() => NavigationBarBottom(
              index: 4,
            )); //Get.off(() => ListLocataireScreen());
      });
    } else {
      setState(() {
        buttonText = repeatText;
      });
    }
  }
}
