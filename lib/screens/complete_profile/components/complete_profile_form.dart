import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:score_immo/components/default_button.dart';
import 'package:score_immo/components/form_error.dart';
import 'package:score_immo/controller/userController.dart';
import 'package:score_immo/models/User.dart';
import 'package:score_immo/screens/finish_profile/finish_profile_screen.dart';
import 'package:score_immo/screens/login_success/login_success_screen.dart';
import 'package:intl/intl.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:score_immo/utils/utils.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CompleteProfileForm extends StatefulWidget {
  @override
  _CompleteProfileFormState createState() => _CompleteProfileFormState();
}

class _CompleteProfileFormState extends State<CompleteProfileForm> {
  UserController userController = Get.put(UserController());

  final _formKey = GlobalKey<FormState>();
  final List<String?> errors = [];

  String? firstName;
  String? lastName;
  String? birthPlace;
  String? iD;
  String? expirationID;
  String? fonction;
  String? income;
  String? domainActivity;
  String? birthDate;
  String selectedStatus = "";
  String? selectedQualification;
  String selectedTypeID = "";
  String selectedTypeUser = "";
  DateTime _selectedBirthDate = new DateTime.now();
  DateTime _selectedExpirationDate = new DateTime.now();
  String buttonText = "S'inscrire";

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildLastNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildFirstNameFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBirthDateFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildBirthPlaceFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildFonctionFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIncomeFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildDomainActivityFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildQualificationFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildIDFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildExpirationIDFormField(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeIDSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          buildProfessionnalStatusSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          //buildQualificationSelectInput(),
          //SizedBox(height: getProportionateScreenHeight(30)),
          buildTypeUserSelectInput(),
          SizedBox(height: getProportionateScreenHeight(30)),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(30)),
          DefaultButton(
            text: selectedTypeUser == "PROPRIETAIRE" ? buttonText : "Suivant",
            press: () {
              var stateStart = selectedTypeUser == "PROPRIETAIRE" &&
                  buttonText != verificationText;
              var stateEnd = selectedTypeUser != "PROPRIETAIRE";

              if (stateStart || stateEnd) {
                if (_formKey.currentState!.validate()) {
                  _formKey.currentState!.save();
                  nextForm();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  // Lieu de naissance
  buildBirthPlaceFormField() {
    return Container(
        height: 60,
        child: TextFormField(
          onSaved: (newValue) => birthPlace = newValue,
          onChanged: (value) {
            if (value.isNotEmpty) {
              removeError(error: kBirthPlaceNullError);
            }
            return null;
          },
          validator: (value) {
            if (value!.isEmpty) {
              addError(error: kBirthPlaceNullError);
              return "";
            }
            return null;
          },
          decoration: InputDecoration(
              labelText: "Lieu de naissance",
              hintText: "Lieu de naissance",
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: Icon(Icons.my_location_outlined)
              //CustomSurffixIcon(svgIcon: "assets/icons/Location point.svg"),
              ),
        ));
  }

  // BirthDate
  buildBirthDateFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.datetime,
        decoration: InputDecoration(
          labelText: "Date de naissance",
          hintText: DateFormat.yMd('fr').format(_selectedBirthDate),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: IconButton(
            icon: Icon(Icons.calendar_today_outlined),
            onPressed: () {
              getBirthDate();
            },
          ),
        ),
      ),
    );
  }

  // Nom de famille
  buildLastNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => lastName = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kNamelNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kNamelNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Nom",
          hintText: "Nom",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_rounded),
        ),
      ),
    );
  }

  // Prenom
  buildFirstNameFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => firstName = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kFirstNamelNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kFirstNamelNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
          labelText: "Prénom(s)",
          hintText: "Prénom(s)",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_rounded),
        ),
      ),
    );
  }

  // Qualification
  buildQualificationFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => selectedQualification = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kQualificationNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kQualificationNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
          labelText: "Qualification",
          hintText: "Qualification",
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: Icon(Icons.person_outline_rounded),
        ),
      ),
    );
  }

  // Numéro de la pièce CNI/PASSPORT/CARTE DE SEJOUR/...
  buildIDFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => iD = newValue,
        onChanged: (value) {
          if (value.isNotEmpty) {
            removeError(error: kIDNullError);
          }
          return null;
        },
        validator: (value) {
          if (value!.isEmpty) {
            addError(error: kIDNullError);
            return "";
          }
          return null;
        },
        decoration: InputDecoration(
            labelText: "Pièce d'identité N°",
            hintText: "Pièce d'identité N°",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .card_membership_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Date d'expiration de la pièce
  buildExpirationIDFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        keyboardType: TextInputType.datetime,
        readOnly: true,
        onSaved: (newValue) => expirationID = newValue,
        onChanged: (value) {
          // if (value.isNotEmpty) {
          //   removeError(error: kExpirationIDNullError);
          // }
          // return null;
        },
        validator: (value) {
          // if (value!.isEmpty) {
          //   addError(error: kExpirationIDNullError);
          //   return "";
          // }
          // return null;
        },
        decoration: InputDecoration(
            labelText: "Date d'expiration de la pièce",
            hintText: DateFormat.yMd('fr').format(_selectedExpirationDate),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: IconButton(
              icon: Icon(Icons.calendar_today_outlined),
              onPressed: () {
                getExpirationDate();
              },
            )),
      ),
    );
  }

  // Fonction
  buildFonctionFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => fonction = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kFonctionNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kFonctionNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
            labelText: "Fonction Occupée",
            hintText: "Fonction Occupée",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .work_outline_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Revenu mensuel
  buildIncomeFormField() {
    return Container(
        height: 60,
        child: TextFormField(
          keyboardType: TextInputType.number,
          onSaved: (newValue) => income = newValue,
          // onChanged: (value) {
          //   if (value.isNotEmpty) {
          //     removeError(error: kIncomeNullError);
          //   }
          //   return null;
          // },
          // validator: (value) {
          //   if (value!.isEmpty) {
          //     addError(error: kIncomeNullError);
          //     return "";
          //   }
          //   return null;
          // },
          decoration: InputDecoration(
              labelText: "Revenu mensuel",
              hintText: "Revenu mensuel",
              floatingLabelBehavior: FloatingLabelBehavior.always,
              suffixIcon: Icon(
                Icons.money_rounded,
              ) //CustomSurffixIcon(svgIcon: "assets/icons/calendar.svg"),
              ),
        ));
  }

  // Domaine d'activité
  buildDomainActivityFormField() {
    return Container(
      height: 60,
      child: TextFormField(
        onSaved: (newValue) => domainActivity = newValue,
        // onChanged: (value) {
        //   if (value.isNotEmpty) {
        //     removeError(error: kDomainActivityNullError);
        //   }
        //   return null;
        // },
        // validator: (value) {
        //   if (value!.isEmpty) {
        //     addError(error: kDomainActivityNullError);
        //     return "";
        //   }
        //   return null;
        // },
        decoration: InputDecoration(
            labelText: "Domaine d'activité",
            hintText: "Domaine d'activité",
            floatingLabelBehavior: FloatingLabelBehavior.always,
            suffixIcon: Icon(Icons
                .local_activity_outlined) //CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
            ),
      ),
    );
  }

  // Statut professionnel
  buildProfessionnalStatusSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Statut professionnel',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: professionnalStatusItems
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Statut professionnel") {
          return 'Veuillez sélectionnez un statut.';
        }
      },
      onChanged: (value) {},
      onSaved: (value) {
        selectedStatus = value.toString();
      },
    ));
  }

  // // Qualification
  // buildQualificationSelectInput() {
  //   return Container(
  //       child: DropdownButtonFormField2(
  //     decoration: InputDecoration(
  //       isDense: true,
  //       contentPadding: EdgeInsets.zero,
  //       border: OutlineInputBorder(
  //         borderRadius: BorderRadius.circular(15),
  //       ),
  //     ),
  //     isExpanded: true,
  //     hint: const Text(
  //       'Qualification',
  //       style: TextStyle(fontSize: 14),
  //     ),
  //     icon: const Icon(
  //       Icons.arrow_drop_down,
  //       color: Colors.black45,
  //     ),
  //     iconSize: 30,
  //     buttonHeight: 60,
  //     buttonPadding: const EdgeInsets.only(left: 20, right: 10),
  //     dropdownDecoration: BoxDecoration(
  //       borderRadius: BorderRadius.circular(15),
  //     ),
  //     items: qualificationItems
  //         .map((item) => DropdownMenuItem<String>(
  //               value: item,
  //               child: Text(
  //                 item,
  //                 style: const TextStyle(
  //                   fontSize: 14,
  //                 ),
  //               ),
  //             ))
  //         .toList(),
  //     validator: (value) {
  //       if (value == null) {
  //         return 'Veuillez sélectionnez une qualification.';
  //       }
  //     },
  //     onChanged: (value) {
  //       //Do something when changing the item if you want.
  //     },
  //     onSaved: (value) {
  //       selectedQualification = value.toString();
  //     },
  //   ));
  // }

  // Type de pièce
  buildTypeIDSelectInput() {
    return Container(
      child: DropdownButtonFormField2(
          decoration: InputDecoration(
            isDense: true,
            contentPadding: EdgeInsets.zero,
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
          isExpanded: true,
          hint: const Text(
            'Type de pièce d\'identité',
            style: TextStyle(fontSize: 14),
          ),
          icon: const Icon(
            Icons.arrow_drop_down,
            color: Colors.black45,
          ),
          iconSize: 30,
          buttonHeight: 60,
          buttonPadding: const EdgeInsets.only(left: 20, right: 10),
          dropdownDecoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          items: typeIDItems
              .map((item) => DropdownMenuItem<String>(
                    value: item,
                    child: Text(
                      item,
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ))
              .toList(),
          validator: (value) {
            if (value == null || value == "Type de pièce") {
              return 'Veuillez sélectionnez un type de pièce.';
            }
          },
          onChanged: (value) {},
          onSaved: (value) {
            selectedTypeID = value.toString();
          },
          dropdownMaxHeight: 210.0),
    );
  }

  // Type d'utilisateur
  buildTypeUserSelectInput() {
    return Container(
        child: DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      isExpanded: true,
      hint: const Text(
        'Type d\'utilisateur',
        style: TextStyle(fontSize: 14),
      ),
      icon: const Icon(
        Icons.arrow_drop_down,
        color: Colors.black45,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: typeUserItems
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 14,
                  ),
                ),
              ))
          .toList(),
      validator: (value) {
        if (value == null || value == "Type d'utilisateur") {
          return 'Veuillez sélectionnez un type d\'utilisateur.';
        }
      },
      onChanged: (value) {
        setState(() {
          selectedTypeUser = value.toString();
        });
      },
      onSaved: (value) {
        selectedTypeUser = value.toString();
      },
    ));
  }

  // Date d'expiration de la piece
  void getExpirationDate() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedExpirationDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedExpirationDate = _pickedDate;
      });
    }
  }

  // Date d'anniversaire
  void getBirthDate() async {
    final DateTime? _pickedDate = await showDatePicker(
        context: context,
        initialDate: _selectedBirthDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(1900),
        lastDate: DateTime(2101),
        cancelText: "Fermer",
        confirmText: "OK");
    if (_pickedDate != null) {
      setState(() {
        _selectedBirthDate = _pickedDate;
      });
    }
  }

  nextForm() async {
    // Récuperation des informations en session
    User user = await userController.getUser("register");
    user.lastName = lastName;
    user.firstName = firstName;
    user.lieuNaissance = birthPlace;
    user.dateNaissance = formatDate(_selectedBirthDate, 'yyyy-MM-dd');
    user.typePiece = selectedTypeID;
    user.numeroPiece = iD;
    user.expirationPiece = formatDate(_selectedExpirationDate, 'yyyy-MM-dd');
    user.fonction = fonction;
    user.revenuMensuel = "$income";
    user.domaineActivite = domainActivity;
    user.qualification = selectedQualification;
    user.statutProfessionnel = selectedStatus;
    user.userType = selectedTypeUser;

    // Redirection vers la prochaine page
    if (selectedTypeUser == "PROPRIETAIRE") {
      Map<dynamic, dynamic> response = await userController.register(user);
      // Stoppe le loader
      setState(() {
        buttonText = verificationText;
      });

      if (response['status']) {
        Get.offAll(() => LoginSuccessScreen());
      } else {
        setState(() {
          buttonText = repeatText;
        });
      }
    } else {
      // Stocke à nouveau les informations en session
      userController.setPreferences(false, user, "register");

      // Redirection vers l'ecran suivant pour les informations d'une agence
      Get.to(() => FinishProfileScreen());
    }
  }
}
