import 'package:intl/intl.dart';

String formatDate(DateTime now, String format) {
  return DateFormat(format).format(now);
}

String replace(String from, String to, String value) {
  return value.replaceAll(from, to).split(" ")[0];
}
