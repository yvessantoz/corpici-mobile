import 'package:flutter/widgets.dart';
import 'package:score_immo/screens/complete_profile/complete_profile_screen.dart';
import 'package:score_immo/screens/details_bien_immobilier/details_screen.dart';
import 'package:score_immo/screens/finish_profile/finish_profile_screen.dart';
import 'package:score_immo/screens/forgot_password/forgot_password_screen.dart';
import 'package:score_immo/screens/home/home_screen.dart';
import 'package:score_immo/screens/list_immobilier/list_immobilier_screen.dart';
import 'package:score_immo/screens/login_success/login_success_screen.dart';
import 'package:score_immo/screens/otp/otp_screen.dart';
import 'package:score_immo/screens/profile/profile_screen.dart';
import 'package:score_immo/screens/reset_password/reset_password_screen.dart';
import 'package:score_immo/screens/sign_in/sign_in_screen.dart';
import 'package:score_immo/screens/splash/splash_screen.dart';

import 'screens/sign_up/sign_up_screen.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(),
  ResetPasswordScreen.routeName: (context) => ResetPasswordScreen(),
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(),
  SignUpScreen.routeName: (context) => SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(),
  FinishProfileScreen.routeName: (context) => FinishProfileScreen(),
  OtpScreen.routeName: (context) => OtpScreen(),
  HomeScreen.routeName: (context) => HomeScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(),
  ListImmobilierScreen.routeName: (context) => ListImmobilierScreen(),
  ProfileScreen.routeName: (context) => ProfileScreen(),
};
